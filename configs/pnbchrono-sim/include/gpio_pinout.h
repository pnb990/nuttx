/****************************************************************************
 * configs/pnbchrono-v2/include/gpio_pinout.h
 *
 *   Copyright (C) 2015 Bouteville Pierre-Noel. All rights reserved.
 *   Author: Bouteville Pierre-Noel <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __CONFIGS_STM32F4_PNBCHRONO_INCLUDE_GPIO_PINOUT_H
#define __CONFIGS_STM32F4_PNBCHRONO_INCLUDE_GPIO_PINOUT_H


/* SDIO: SDIO
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PC12       CLK               Configured by driver
 * PD2        CMD               "        " "" "    "
 * PC8        DAT0              "        " "" "    "
 * PC9        DAT1              "        " "" "    "
 * PC10       DAT2              "        " "" "    "
 * PC11       CD/DAT3           "        " "" "    "
 * PA15       NCD               Pulled up externally
 * ---------- ----------------- ------------------------------
 */
#define GPIO_SDIO_NCD           (GPIO_INPUT|GPIO_FLOAT|GPIO_EXTI|\
                                 GPIO_SPEED_100MHz|\
                                 GPIO_PORTA|GPIO_PIN15)


/* FMSC: LCD
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PE1        RESET             Configured by driver
 * PE2        FSMC_A23          A0                
 * PD7        FSMC_NE1          #CS                
 * PD5        FSMC_NWE          #WR                
 * PD4        FSMC_NOE          #RD                
 * PD14       FSMC_D0           DATA0
 * PD15       FSMC_D1           DATA1
 * PD0        FSMC_D2           DATA2
 * PD1        FSMC_D3           DATA3
 * PE7        FSMC_D4           DATA4
 * PE8        FSMC_D5           DATA5
 * PE9        FSMC_D6           DATA6
 * PE10       FSMC_D7           DATA7
 * PB7        TIM4_CH2          BackLight
 * ---------- ----------------- ------------------------------
 */

#define GPIO_LCD_RESET          (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_OUTPUT_SET|\
                                 GPIO_SPEED_2MHz|\
                                 GPIO_PORTE|GPIO_PIN1)

#ifdef CONFIG_PWM
#   define GPIO_LCD_PWM_DEV        "/dev/pwm_backlight"
#   define GPIO_LCD_PWM_TIMER      4
#   define GPIO_LCD_PWM_CHANNEL    2
#   define GPIO_TIM4_CH2OUT        GPIO_TIM4_CH2OUT_1
#else
#   define GPIO_LCD_PWM_PIN        (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_OUTPUT_SET|\
                                    GPIO_SPEED_2MHz|\
                                    GPIO_PORTB|GPIO_PIN7)
#endif

#define LCD_ADDRESS_IDX         23
#define GPIO_LCD_A0             GPIO_FSMC_A23


/* CAN1
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PB8        CAN_TX  
 * PB9        CAN_RX
 * ---------- ----------------- ------------------------------
 */

#define GPIO_CAN1_TX  GPIO_CAN1_TX_2
#define GPIO_CAN1_RX  GPIO_CAN1_RX_2

/* CAN2
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PB6        CAN_TX  
 * PB5        CAN_RX
 * ---------- ----------------- ------------------------------
 */

#define GPIO_CAN2_TX  GPIO_CAN2_TX_2
#define GPIO_CAN2_RX  GPIO_CAN2_RX_2


/* USB OTG FS
 * 
 * ---------- ----------------- ------------------------------
 * PIO        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PA9        OTG_FS_VBUS       VBUS sensing
 * PA10       OTG_FS_ID         USB ID
 * PA11       OTG_FS_DM         USB D-
 * PA12       OTG_FS_DP         USB D+
 * ---------- ----------------- ------------------------------
 */

#define GPIO_OTGFS_VBUS         (GPIO_INPUT|GPIO_FLOAT|\
                                 GPIO_SPEED_2MHz|\
                                 GPIO_PORTA|GPIO_PIN9)




/* USART6 : GPS
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PC6        USART6_TX         GPS_MTX           
 * PC7        USART6_TX         GPS_MRX                                          
 * PB2        GPIO Input        GPS_RST (Unused for moment)
 * ---------- ----------------- ------------------------------
 */

#define GPIO_USART6_RX          GPIO_USART6_RX_1
#define GPIO_USART6_TX          GPIO_USART6_TX_1
#define GPIO_GPS_RESET          (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_OUTPUT_SET|\
                                 GPIO_SPEED_2MHz|\
                                 GPIO_PORTB|GPIO_PIN2)

/* UART4 : KLine
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PA0        UART4_RX          KLINE_MTX
 * PA1        UART4_TX          KLINE_MRX
 * PA4        GPIO Output       KLINE_NSLP
 * ---------- ----------------- ------------------------------
 */

#define GPIO_UART4_RX           GPIO_UART4_RX_1
#define GPIO_UART4_TX           GPIO_UART4_TX_1
#define GPIO_KLINE_NSLP         (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_OUTPUT_CLEAR|\
                                 GPIO_SPEED_2MHz|\
                                 GPIO_PORTA|GPIO_PIN4)

/* USART3 : WIFI ESP2866
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PD8        TX    / MOSI      WIFI_MOSI_MTX  
 * PD9        RX    / MISO      WIFI_MISO_MRX  
 * PD10       GPIO0 / CLK       WIFI_CLK       
 * PD11       GPIO2 / INT       WIFI_INT_MCTS  
 * PD12       RESET / CS        WIFI_CS_MRTS   
 * PD13       CH_PD / PWR_ON    WIFI_EN_POWER  
 * ---------- ----------------- ------------------------------
 */

#define GPIO_USART3_RX  GPIO_USART3_RX_3
#define GPIO_USART3_TX  GPIO_USART3_TX_3

#define GPIO_WIFI_GPIO0 (GPIO_OUTPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET|GPIO_PULLUP|\
                         GPIO_SPEED_2MHz|\
                         GPIO_PORTD|GPIO_PIN10)
#define GPIO_WIFI_GPIO2 (GPIO_INPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET|GPIO_PULLUP|\
                         GPIO_SPEED_2MHz|\
                         GPIO_PORTD|GPIO_PIN11)
#define GPIO_WIFI_RST   (GPIO_OUTPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET|GPIO_PULLUP|\
                         GPIO_SPEED_2MHz|\
                         GPIO_PORTD|GPIO_PIN12)
#define GPIO_WIFI_CHPD  (GPIO_OUTPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET|GPIO_PULLUP|\
                         GPIO_SPEED_2MHz|\
                         GPIO_PORTD|GPIO_PIN13)



/* 
 * ---------- ----------------- ------------------------------
 * PIO        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PE12       GPIO Input        BACK(BP1)
 * PB4        GPIO Input        UP  (BP2)
 * PE13       GPIO Input        DOWN(BP3)
 * PE0        GPIO Input        OK  (BP4)
 * ---------- ----------------- ----------------------------------
 */

#define GPIO_KEYBOARD_MAPPING \
{\
    { /* BACK */ \
        (GPIO_INPUT|GPIO_PULLUP|GPIO_SPEED_2MHz|\
         GPIO_PORTE|GPIO_PIN12 ),\
    },\
    { /* UP   */ \
        (GPIO_INPUT|GPIO_PULLUP|GPIO_SPEED_2MHz|\
         GPIO_PORTB|GPIO_PIN4  )\
    },\
    { /* DOWN */ \
        (GPIO_INPUT|GPIO_PULLUP|GPIO_SPEED_2MHz|\
         GPIO_PORTE|GPIO_PIN13 )\
    },\
    { /* OK   */ \
        (GPIO_INPUT|GPIO_PULLUP|GPIO_SPEED_2MHz|\
         GPIO_PORTE|GPIO_PIN0  )\
    }\
}

#define GPIO_BTLDR_PIN_SET()                            \
{                                                       \
    uint32_t regval;                                    \
    /* Enable GPIO Clock for PORT E */                  \
    regval = getreg32(STM32_RCC_AHB1ENR);               \
    regval |= RCC_AHB1ENR_GPIOEEN;                      \
    putreg32(regval,STM32_RCC_AHB1ENR);                 \
    /* Set PE12 in input mode. To be sure */            \
    regval = getreg32(STM32_GPIOE_MODER);               \
    regval &= ~GPIO_MODER_MASK(12); /* input is 0 */    \
    regval |= GPIO_INPUT<<GPIO_MODER_SHIFT(12);         \
    putreg32(regval,STM32_GPIOE_MODER);                 \
    /* Enable GPIO Pull-up */                           \
    regval = getreg32(STM32_GPIOE_PUPDR);               \
    regval &= ~GPIO_PUPDR_MASK(12);                     \
    regval |= GPIO_PUPDR_PULLUP<<GPIO_PUPDR_SHIFT(12);  \
    putreg32(regval,STM32_GPIOE_PUPDR);                 \
}

#define GPIO_BTLDR_PIN_RESET() \
{                                                       \
    uint32_t regval;                                    \
                                                        \
    /* input already the reset value */                 \
                                                        \
    /* Reset GPIO Pull-up */                            \
    regval = getreg32(STM32_GPIOE_PUPDR);               \
    regval &= ~GPIO_PUPDR_MASK(12);                     \
    putreg32(regval,STM32_GPIOE_PUPDR);                 \
    /* Reset GPIO Clock for PORT E */                   \
    regval = getreg32(STM32_RCC_AHB1ENR);               \
    regval &= ~(RCC_AHB1ENR_GPIOEEN);                   \
    putreg32(regval,STM32_RCC_AHB1ENR);                 \
}

#define GPIO_IS_BTLDR_MODE_FORCED() \
    ( ( getreg32(STM32_GPIOE_IDR) & (1<<12)) == 0 )

/* 
 * ---------- ----------------- ------------------------------
 * PIO        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PA6        TIM3_CH1          RED               
 * PA7        TIM3_CH2          YELLOW            
 * PB0        TIM3_CH3          GREEN2            
 * PB1        TIM3_CH4          GREEN1            
 * ---------- ----------------- ------------------------------
 */

#define GPIO_LED_PWM_DEV        "/dev/pwm_led"
#define GPIO_LED_PWM_TIMER      3

#define GPIO_LED_MODE   (GPIO_OUTPUT|GPIO_PUSHPULL|\
                         GPIO_OUTPUT_CLEAR|GPIO_SPEED_100MHz)
#ifdef CONFIG_PWM
#   define GPIO_TIM3_CH1OUT     GPIO_TIM3_CH1OUT_1
#   define GPIO_TIM3_CH2OUT     GPIO_TIM3_CH2OUT_1
#   define GPIO_TIM3_CH3OUT     GPIO_TIM3_CH3OUT_1
#   define GPIO_TIM3_CH4OUT     GPIO_TIM3_CH4OUT_1
#else
#   define GPIO_LED_RED         (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_OUTPUT_CLEAR|\
                                 GPIO_SPEED_2MHz|GPIO_PORTA|GPIO_PIN6)
#   define GPIO_LED_YELLOW      (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_OUTPUT_CLEAR|\
                                 GPIO_SPEED_2MHz|GPIO_PORTA|GPIO_PIN7)
#   define GPIO_LED_GREEN2      (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_OUTPUT_CLEAR|\
                                 GPIO_SPEED_2MHz|GPIO_PORTB|GPIO_PIN0)
#   define GPIO_LED_GREEN1      (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_OUTPUT_CLEAR|\
                                 GPIO_SPEED_2MHz|GPIO_PORTB|GPIO_PIN1)
#endif


/* SPI2 : SPI Extension
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PB13       CLK               SPI_CLK       
 * PB12       CS                SPI_CS
 * PB14       MISO              SPI_MISO
 * PB15       MOSI              SPI_MOSI
 * PE14       INT               SPI_INT
 * PE15       PWR_ON            SPI_POWER_ON  
 * ---------- ----------------- ------------------------------
 */

#if CONFIG_ARCH_LEDS
/* LED definitions **********************************************************/
/* The Amber Web Server has a reset switch and four LEDs. The LEDs indicate
 * the status of power, programming state, Ethernet link status and reset
 * status (Active).  None are available for software use.
 */

typedef enum
{
 LED_STARTED=0,
 LED_HEAPALLOCATE,
 LED_IRQSENABLED,
 LED_STACKCREATED,
 LED_INIRQ,
 LED_SIGNAL,
 LED_ASSERTION,
 LED_PANIC,
 LED_USER1,
 LED_IDLE,
 LED_NBR
}board_led_t;

#define GPIO_LED_MODE   (GPIO_OUTPUT|GPIO_PUSHPULL|\
                         GPIO_OUTPUT_CLEAR|GPIO_SPEED_100MHz)

//#   define GPIO_LED_STARTED         (GPIO_LED_MODE|GPIO_PORTB|GPIO_PIN12)
//#   define GPIO_LED_HEAPALLOCATE    (GPIO_LED_MODE|GPIO_PORTB|GPIO_PIN12)
//#   define GPIO_LED_IRQSENABLED     (GPIO_LED_MODE|GPIO_PORTB|GPIO_PIN12)
//#   define GPIO_LED_STACKCREATED    (GPIO_LED_MODE|GPIO_PORTB|GPIO_PIN12)
#   define GPIO_LED_INIRQ           (GPIO_LED_MODE|GPIO_PORTB|GPIO_PIN15)
//#   define GPIO_LED_SIGNAL          (GPIO_LED_MODE|GPIO_PORTB|GPIO_PIN12)
//#   define GPIO_LED_ASSERTION       (GPIO_LED_MODE|GPIO_PORTB|GPIO_PIN12)
//#   define GPIO_LED_PANIC           (GPIO_LED_MODE|GPIO_PORTB|GPIO_PIN12)
#   define GPIO_LED_USER1           (GPIO_LED_MODE|GPIO_PORTB|GPIO_PIN12)
//#   define GPIO_LED_IDLE            (GPIO_LED_MODE|GPIO_PORTB|GPIO_PIN12)

#else
#   define GPIO_SPI2_MOSI          GPIO_SPI2_MOSI_1
#   define GPIO_SPI2_MISO          GPIO_SPI2_MISO_1
#   define GPIO_SPI2_SCK           GPIO_SPI2_SCK_1
#   define GPIO_SPI2_CS            GPIO_SPI2_NSS_1
#   define GPIO_SPI2_INT           (GPIO_INPUT|GPIO_PULLUP|GPIO_SPEED_100MHz\
                                    GPIO_PORTE|GPIO_PIN14)
#   define GPIO_SPI2_PWR_ON        (GPIO_INPUT|GPIO_PULLUP|GPIO_SPEED_100MHz\
                                    GPIO_PORTE|GPIO_PIN15)
#endif


/* TIM2 : Timer Sensors 
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PA5        TIM2_CH1_ETR      Precise clock from GPS or TCXO
 * PA3        TIM2_CH4          Loop
 * PA2        TIM2_CH3          Break
 *
 * PA8        TIM1_CH1          Speed
 * PE5        TIM9_CH1          RPM
 * ---------- ----------------- ------------------------------
 */

#define GPIO_CHRONO_CAPTURE     2
#define GPIO_TIM2_ETR           GPIO_TIM2_ETR_3

/* LOOP */
#define GPIO_CHRONO_LOOP_CH     4
#define GPIO_CHRONO_LOOP_CFG    (STM32_CAP_MAPPED_TI1| STM32_CAP_INPSC_NO    |\
                                 STM32_CAP_FILTER_NO | STM32_CAP_EDGE_FALLING)
#define GPIO_TIM2_CH4IN         GPIO_TIM2_CH4IN_1

/* BREAK */
#if 0
#define GPIO_CHRONO_BREAK_CH    3
#define GPIO_CHRONO_BREAK_CFG   (STM32_CAP_MAPPED_TI1| STM32_CAP_INPSC_NO    |\
                                 STM32_CAP_FILTER_NO | STM32_CAP_EDGE_BOTH   )
#endif
#define GPIO_BREAKING           (GPIO_INPUT|GPIO_PULLUP|GPIO_SPEED_2MHz|\
                                 GPIO_PORTA|GPIO_PIN2)
#define GPIO_BREAK_PRESSED()    (stm32_gpioread(GPIO_BREAKING)!=0)


/* SPEED */
#define GPIO_SPEED_PPM_ID       1
#define GPIO_TIM1_CH1IN         GPIO_TIM1_CH1IN_1
#define GPIO_PPM1_CH1CFG        (STM32_CAP_MAPPED_TI1| STM32_CAP_INPSC_NO    |\
                                 STM32_CAP_FILTER_NO | STM32_CAP_EDGE_FALLING )

/* RPM */
#define GPIO_RPM_PPM_ID         9
#define GPIO_TIM9_CH1IN         GPIO_TIM9_CH1IN_2
#define GPIO_PPM9_CH1CFG        (STM32_CAP_MAPPED_TI1| STM32_CAP_INPSC_NO    |\
                                 STM32_CAP_FILTER_NO | STM32_CAP_EDGE_FALLING )

/* ADC : Analog Sensors 
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PC4        ADC12_IN14        Light temperature
 * PC5        ADC12_IN15        Battery Voltage
 * PC0        ADC123_IN10       ADC_EXT1
 * PC1        ADC123_IN11       ADC_EXT2
 * PC2        ADC123_IN12       Suspension Front
 * PC3        ADC123_IN13       Suspension Rear
 * ---------- ----------------- ------------------------------
 */


#define GPIO_ADC_VREF_MV        3300
#define GPIO_VBAT_MAX           3300
#define GPIO_VBAT_MIN           2400
#define GPIO_VPWR_CHR_MV        6000

/* Vbat --- /2 (STM32) --- ADC */
#define GPIO_ADC2VBAT_MV(adc)   ((adc)*GPIO_ADC_VREF_MV*2/(4096))

/* (STM32) --- ADC */
#define GPIO_ADC2MV(adc)   ((adc)*GPIO_ADC_VREF_MV/(4096))

/* Vpower --- 10K --- ADC --- 1K5 --- GND */
/* (4096)*GPIO_ADC_VREF_MV*(100+15) => 0x5C A6 C0 00 fit a int */
#define GPIO_ADC2VPWR_MV(adc)   ((adc)*GPIO_ADC_VREF_MV*(100+15)\
                                 / (4096*15))

#define GPIO_ADC_LIGHT_TEMP     GPIO_ADC1_IN14
#define GPIO_ADC_VPWR           GPIO_ADC1_IN15
#define GPIO_ADC_VBAT           ((uint32_t)-1) /* no GPIO */
#define GPIO_ADC_EXT1           GPIO_ADC1_IN10
#define GPIO_ADC_EXT2           GPIO_ADC1_IN11
#define GPIO_ADC_FRONT          GPIO_ADC1_IN12
#define GPIO_ADC_REAR           GPIO_ADC1_IN13

#define GPIO_ADC_CH_LIGHT_TEMP  14
#define GPIO_ADC_CH_VPWR        15
#define GPIO_ADC_CH_VBAT        18
#define GPIO_ADC_CH_EXT1        10
#define GPIO_ADC_CH_EXT2        11
#define GPIO_ADC_CH_FRONT       12
#define GPIO_ADC_CH_REAR        13

/* POWER ON
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PE3       SDA               
 * ---------- ----------------- ------------------------------
 */

#define GPIO_POWER_ON           (GPIO_OUTPUT|GPIO_OUTPUT_SET|GPIO_PUSHPULL|\
                                 GPIO_SPEED_2MHz|\
                                 GPIO_PORTE|GPIO_PIN3)

/* I2C  
 *
 * ---------- ----------------- ------------------------------
 * PIN        SIGNAL            Comments
 * ---------- ----------------- ------------------------------
 * PB10       SCL               
 * PB11       SDA               
 * ---------- ----------------- ------------------------------
 */

#define GPIO_MPU_I2C        2
#define GPIO_MLX90614_I2C   2
#define GPIO_VL53L0X_I2C    2

#define GPIO_I2C2_SCL       GPIO_I2C2_SCL_1
#define GPIO_I2C2_SDA       GPIO_I2C2_SDA_1



/* DMA
 *
 * ---------- ----- -------- --------- ------------------------------
 * DEVICE      DMA   STREAM   CHANNEL   Comments
 * ---------- ----- -------- --------- ------------------------------
 * SDIO       DMA2  STREAM3   CHAN4     STREAM 3 or 6
 * USART3     DMA1  STREAM1   CHAN4     Fix
 * UART4      DMA1  STREAM2   CHAN4     Fix
 * USART6     DMA2  STREAM1   CHAN5     STREAM 1 or 2
 * ---------- ----------------- ------------------------------
 */
#define DMAMAP_SDIO             DMAMAP_SDIO_1
//#define DMAMAP_USART6_RX        DMAMAP_USART6_RX_1




#endif
