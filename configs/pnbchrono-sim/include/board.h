/************************************************************************************
 * configs/pnbchrono-sim/include/board.h
 *
 *   Copyright (C) 2017 Pierre-Noel Bouteville. All rights reserved.
 *   Author: Pierre-Noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __CONFIG_SIM_PNBCHRONO_INCLUDE_BOARD_H
#define __CONFIG_SIM_PNBCHRONO_INCLUDE_BOARD_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include "gpio_pinout.h"

#ifndef __ASSEMBLY__
# include <stdint.h>
# include "stdbool.h"
#endif

#include <sys/boardctl.h>
#include <nuttx/board.h>


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/


/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

#define BOARD_SDHC_BLOCK_DEV_PATH   "/dev/mmcsd0"
#define BOARD_SDHC_MOUNT_PATH       "/mnt"
#define BOARD_SDHC_FSTYPE           "vfat"

#define BOARD_TTYS_WIFI_DEV         "/dev/ttyS0"
#define BOARD_TTYS_KLINE_DEV        "/dev/ttyS1"
#define BOARD_TTYS_GPS_DEV          "/dev/ttyS2"
#define BOARD_FAST_SNS_DEV          "/dev/fast_sns0"


#define BOARD_MLX90614_PATH         "/dev/temps_ir"
#define BOARD_MLX90614_I2C_ADDR(x)  (0x10+(x))
#define BOARD_MLX90614_NBR          6

#define BOARD_VL52L0X_I2C_ADDR_DEF      (0x52/2) /* default Address */
#define BOARD_VL52L0X_I2C_ADDR_FRONT    (BOARD_VL52L0X_I2C_ADDR_DEF+1)
#define BOARD_VL52L0X_I2C_ADDR_REAR     (BOARD_VL52L0X_I2C_ADDR_DEF)
#define BOARD_PCF9536_I2C_ADDR          (0x41)

#define BOARD_KEY_PRESSED  0x80
#define BOARD_KEY_BACK      0
#define BOARD_KEY_UP        1
#define BOARD_KEY_DOWN      2
#define BOARD_KEY_OK        3


/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/


/************************************************************************************
 * Name:  Low level non posix function
 *
 * Description:
 *   If CONFIG_ARCH_LEDS is defined, then NuttX will control the on-board LEDs.  If
 *   CONFIG_ARCH_LEDS is not defined, then the following interfaces are available to
 *   control the LEDs from user applications.
 *
 ************************************************************************************/

#ifdef CONFIG_BOARDCTL_IOCTL

enum
{
    /**
      @brief Reboot in bootloader mode.
      @note board_ioctl cmd
     */
    BIOC_REBOOT_BTLDR      =  BOARDIOC_USER+1,
    /**
      @brief Format SDCard
      @note board_ioctl cmd
     */
    BIOC_SDCARD_FORMAT,

    /**
      @brief Mount ProcFs
      @note board_ioctl cmd
     */
    BIOC_PROCFS_MOUNT,

    /**
      @brief Mount SDCard
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is Mounted, can be NULL.
     */
    BIOC_SDCARD_MOUNT,

    /**
      @brief Mount SDCard
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is Mounted.
     */
    BIOC_SDCARD_IS_MOUNTED,

    /**
      @brief Check SDCARD is inserted
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is inserted.
     */
    BIOC_SDCARD_IS_PRESENT,

    /**
      @brief Umount SDCard
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is Mounted, can be NULL.
     */
    BIOC_SDCARD_UNMOUNT,

    /**
      @brief Enable USB Mass Storage Class
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is Connected.
     */
    BIOC_USB_IS_CONNECTED,

    /**
      @brief Enable USB Mass Storage Class
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is Enable.
     */
    BIOC_USB_IS_ENABLED,

    /**
      @brief Enable USB Mass Storage Class
      @note board_ioctl cmd
     */
    BIOC_USB_MSC_ENABLE,

    /**
      @brief Disable USB Mass Storage Class
      @note board_ioctl cmd
     */
    BIOC_USB_MSC_DISABLE,

    /**
      @brief is Charging
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is charging.
     */
    BIOC_IS_CHARGING,

    /**
      @brief read battery
      @note board_ioctl cmd
      @param[out]   arg (int*) Battery level in percent.
     */
    BIOC_BAT_LEVEL,

    /**
      @brief read battery voltage in mv.
      @note board_ioctl cmd
      @param[out]   arg (int*) Battery level in mv.
     */
    BIOC_VBAT,

    /**
      @brief read power supply in mv.
      @note board_ioctl cmd
      @param[out]   arg (int*) Power Voltage in mV.
      @return 0 (OK) ons success, -1 in case of error.
     */
    BIOC_VPWR,

    /**
      @brief read ambient temperature in m°C.
      @note board_ioctl cmd
      @param[out]   arg (int*) ambient temperature in mV.
      @return 0 (OK) ons success, -1 in case of error.
     */
    BIOC_TAMB,

    /**
      @brief Power OFF.
      if arg is true, we unmount SD card 
      otherwise it failed SD card is mounted.
      @note board_ioctl cmd
      @param[in]   arg bool : force
      @return 0 (OK) ons success, -1 in case of error.
     */
    BIOC_POWER_OFF,
};
#endif


#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif  /* __CONFIG_SIM_PNBCHRONO_INCLUDE_BOARD_H */
