/****************************************************************************
 * configs/stm32gg-pnbchrono/src/stm32_fast_sns.c
 *
 *   Copyright (C) 2015 Pierre-noel Bouteville. All rights reserved.
 *   Author: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>

#include <nuttx/arch.h>
#include <nuttx/irq.h>
#include <nuttx/wqueue.h>
#include <nuttx/clock.h>

#include <arch/irq.h>
#include <arch/board/board.h>
#include <arch/board/fast_sns.h>

#include <string.h>
#include <poll.h>
#include <errno.h>
#include <nuttx/kmalloc.h>

#include "up_arch.h"
#include "stm32_gpio.h"
#include "stm32_rcc.h"
#include "stm32_tim.h"
#include "pnbchrono-v2.h"

#ifdef CONFIG_STM32_SENSORS_DEBUG 
#   define fast_snsinfo(fmt,...)   _info("SNSinf "fmt, ##__VA_ARGS__)
#else
#   define fast_snsinfo(fmt,...)
#endif

#ifdef CONFIG_STM32_SENSORS_ERROR 
#   define fast_snserr(fmt,...)    _err("SNSerr "fmt, ##__VA_ARGS__)
#else
#   define fast_snserr(fmt,...)
#endif


/****************************************************************************
 * SENSORS config
 ****************************************************************************/
#define FIFO_SIZE           128
#define TIMER_CLK_FREQ      10000 /* 10 kHz */
#define VL53L0X_TIMEOUT_S   30 /* secondd */

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Fileops Prototypes and Structures
 ****************************************************************************/

typedef FAR struct file file_t;

static int stm32_fast_sns_open(      file_t * filep);
static int stm32_fast_sns_close(     file_t * filep);
static ssize_t stm32_fast_sns_read(  file_t * filep, FAR char *buf, 
                                      size_t buflen);
static int stm32_fast_sns_ioctl(     file_t * filep, int cmd, 
                                      unsigned long arg );
#ifndef CONFIG_DISABLE_POLL
static int stm32_fast_sns_poll(file_t * filep, 
                                FAR struct pollfd *fds, 
                                bool setup
                               );
#endif

static const struct file_operations fast_sns_ops =
{
    .open   = stm32_fast_sns_open,
    .close  = stm32_fast_sns_close,
    .read   = stm32_fast_sns_read,
    .write  = NULL,                
    .seek   = NULL,
    .ioctl  = stm32_fast_sns_ioctl, 
#ifndef CONFIG_DISABLE_POLL
    .poll   = stm32_fast_sns_poll, 
#endif
};

/****************************************************************************
 * Name: stm32_fast_sns_dev_t
 * Description:
 *  variable of keypad
 ****************************************************************************/
typedef struct 
{
    int     open_count;     /* open counter */
    int     missing_nbr;    /* Number of sample missed */

    FAR struct stm32_tim_dev_s  *tim; /* timer used */

    /* fifo */
    sem_t   *poll_sem;      /* Poll event semaphore */
    sem_t   mutex;          /* mutex protection */
    sem_t   rd_sem;         /* fifo semaphore */
    int     inndx;         /* fifo read index */
    int     outndx;         /* fifo write index */
    fast_sns_t buf[FIFO_SIZE]; /* fifo data */

    struct stm32_ppm_s* rpm;
    struct stm32_ppm_s* speed;

    int  rpm_rate;
    int  speed_rate;

    uint8_t break_type;
    uint8_t suspension_front_type;
    uint8_t suspension_rear_type;

    int32_t suspension_front;
    int32_t suspension_rear;


    bool    hpworker_started;
    struct  work_s work;

}stm32_fast_sns_dev_t;


/****************************************************************************
 * Name: stm32_fast_sns
 * Description:
 *  variable of keypad instance.
 ****************************************************************************/
stm32_fast_sns_dev_t* stm32_fast_sns;

/************************************************************************************
 * Name: stm32_fast_sns_takesem
 ************************************************************************************/

static int stm32_fast_sns_takesem(FAR sem_t *sem, bool errout)
{
  /* Loop, ignoring interrupts, until we have successfully acquired the semaphore */

  while (sem_wait(sem) != OK)
    {
      /* The only case that an error should occur here is if the wait was awakened
       * by a signal.
       */

      ASSERT(get_errno() == EINTR);

      /* When the signal is received, should we errout? Or should we just continue
       * waiting until we have the semaphore?
       */

      if (errout)
        {
          return -EINTR;
        }
    }

  return OK;
}

/*******************************************************************************
 * Name: stm32_fast_sns_givesem
 ******************************************************************************/

#define stm32_fast_sns_givesem(sem) (void)sem_post(sem)


/****************************************************************************
 * Name: stm32_chrono_level
 ****************************************************************************/

static void stm32_fast_sns_hpworker(FAR void *arg)
{
    int front = 0;
    int rear = 0;
    systime_t last_good = 0;
    irqstate_t irqstate;
    stm32_fast_sns_dev_t *dev = (stm32_fast_sns_dev_t*)arg;

    if ( dev->suspension_front_type == SENSORS_TYPE_VL53L0X )
    {
        front = stm32_vl53l0x_get_range(vl53l0x_eID_FONT);
    }

    if ( dev->suspension_rear_type == SENSORS_TYPE_VL53L0X )
    {
        rear = stm32_vl53l0x_get_range(vl53l0x_eID_REAR);
    }

    irqstate = up_irq_save(); /* disable IRQ */
    dev->suspension_front   = front;
    dev->suspension_rear    = rear;
    up_irq_restore(irqstate); /* enabled IRQ */


    {
        systime_t tick = clock_systimer();
        if ( ( last_good != 0 ) && \
             ( ( front < 0 ) || ( rear < 0 ) ) && \
             ( ( last_good < tick ) )
           )
        {
            stm32_vl53l0x_lpwork_reset();
        }

        last_good = tick + SEC2TICK(VL53L0X_TIMEOUT_S);
    }

    dev->hpworker_started = false;
}

/****************************************************************************
 * Name: stm32_timer_handler
 *
 * Description:
 *   timer interrupt handler
 *
 * Input Parameters:
 *
 * Returned Values:
 *
 ****************************************************************************/

int stm32_fast_sns_worker(int irq, FAR void* context)
{
    UNUSED(irq);
    UNUSED(context);
    stm32_fast_sns_dev_t *dev = stm32_fast_sns;

    int i;
    int next;
    fast_sns_t *ptr;
    bool need_hpwork = false;

    ASSERT(dev != NULL);

    next = dev->inndx + 1;
    if (next >= FIFO_SIZE)
    {
        next -= FIFO_SIZE;
    }

    if (next == dev->inndx)
    {
        dev->missing_nbr++;
        return -1; 
    }

    ptr = &dev->buf[dev->inndx];
    memset(ptr,0,sizeof(*ptr));

    /*************************************************************************/

    //fast_snsinfo("\f\n");

    /* 
     * Time 
     */
    if ( stm32_chrono_get_time(&ptr->tp) < 0 )
    {
        fast_snserr("Cannot get chrono time\n");
        return -1;
    }
    fast_snsinfo("    Time   %6d.%06d  \n",ptr->tp.tv_sec,ptr->tp.tv_nsec/1000);

    /* 
     * Breaking 
     */
    switch(dev->break_type)
    {
#ifdef GPIO_BREAKING
        case SENSORS_BREAK_GPIO_NORM:
            if ( GPIO_BREAK_PRESSED() )
                ptr->breaking = 1000; /* 1 bar */
            break;
        case SENSORS_BREAK_GPIO_INV:
            if ( ! GPIO_BREAK_PRESSED() )
                ptr->breaking = 1000; /* 1 bar */
            break;
#endif
        case SENSORS_BREAK_GPIO_EXT1:
                ptr->breaking = GPIO_ADC2MV(ana_values[ANA_EXT1]);
            break;
        case SENSORS_BREAK_GPIO_EXT2:
                ptr->breaking = GPIO_ADC2MV(ana_values[ANA_EXT2]);
            break;
        default:
            break;
    }
    fast_snsinfo("    BREAK   %6d  \n",ptr->breaking);
    /* 
     * Motor RPM
     */
    if (dev->rpm != NULL)
    {
        float val = stm32_ppm_get_last(dev->rpm);
        ptr->rpm = val*dev->rpm_rate;
        fast_snsinfo("    RPM     %6d RPM (ppm %3d val)\n",ptr->rpm,(int)val);
    }
    /* 
     * Vehicle SPEED
     */
    if (dev->speed != NULL)
    {
        float val = stm32_ppm_get_last(dev->speed);
        ptr->speed = (uint16_t)((val*dev->speed_rate)/1000000.);
        fast_snsinfo("    Speed   %6d Km/h (ppm %3d val)\n",ptr->speed,(int)val);
    }

    /* 
     * Front suspension
     */
    switch ( dev->suspension_front_type )
    {
        case SENSORS_TYPE_ADC:
            i = ana_values[ANA_FRONT];
            ptr->suspension_front = GPIO_ADC2MV(i);
            fast_snsinfo("    SF      %6d mV (ADC %6d)\n",
                         ptr->suspension_front,i);
            break;
        case SENSORS_TYPE_VL53L0X:
            if ( ! dev->hpworker_started )
            {
                i = dev->suspension_front;
                ptr->suspension_front = i; /* no compensation for now */
                fast_snsinfo("    SF      %6d mm \n", ptr->suspension_front);
                need_hpwork = true;
            }
            break;
    }

    /* 
     * Rear suspension
     */
    switch ( dev->suspension_rear_type )
    {
        case SENSORS_TYPE_ADC:
            i = ana_values[ANA_REAR];
            ptr->suspension_rear = GPIO_ADC2MV(i);
            fast_snsinfo("    SR      %6d mV (ADC %6d)\n", 
                         ptr->suspension_rear,i);
            break;
        case SENSORS_TYPE_VL53L0X:
            if ( ! dev->hpworker_started )
            {
                i = dev->suspension_rear;
                ptr->suspension_rear = i; /* no compensation for now */
                fast_snsinfo("    SR      %6d mm \n", ptr->suspension_rear);
                need_hpwork = true;
            }
            break;
    }

    /* 
     * Analog Extension 1
     */
    i = ana_values[ANA_EXT1];
    ptr->ext1 = GPIO_ADC2MV(i);
    fast_snsinfo("    EXT1    %6d mV (ADC %6d)\n", ptr->ext1,i);

    /* 
     * Analog Extension 2
     */
    i = ana_values[ANA_EXT2];
    ptr->ext2 = i;
    fast_snsinfo("    EXT2    %6d mV (ADC %6d)\n", ptr->ext2,i);



    /*************************************************************************/

    dev->inndx = next;
    sem_post(&dev->rd_sem);

    /* add event to waiting semaphore */
    if ( dev->poll_sem )
    {
        sem_post( dev->poll_sem );
    }

    STM32_TIM_ACKINT(dev->tim, 0);

    if ( dev->hpworker_started )
    {
        fast_snsinfo("FAST Sns HPWorker not finised!\n");
    }
    else if ( need_hpwork )
    {
        dev->hpworker_started = true;
        if ( work_queue(HPWORK, &(dev->work), stm32_fast_sns_hpworker, dev, 0
                       ) != OK )
        {
            dev->hpworker_started = false;
            fast_snsinfo("Cannot register worker !\n");
        }
    }

    return OK;
}


/****************************************************************************
 * Name: stm32_fast_sns_init
 *
 * Description:
 *  Initialize All GPIO for key pad.
 * Input parameters:
 *  _key_map    - first key mapping of mapping GPIO<=>Key list.
 *                    to Finish list set Pin with negative value.
 * Returned Value:
 *   None (User allocated instance initialized).
 ****************************************************************************/
int stm32_fast_sns_init( void )
{
    irqstate_t irqstate;
    stm32_fast_sns_dev_t *dev;

#ifdef GPIO_BREAKING
    stm32_configgpio(GPIO_BREAKING);
#endif

    /* Disable interrupts until we are done.  This guarantees that the
     * following operations are atomic.
     */

    ASSERT(stm32_fast_sns == NULL);

    dev = (stm32_fast_sns_dev_t*)kmm_malloc(sizeof(stm32_fast_sns_dev_t));
    if ( dev == NULL )
    {
        fast_snserr("Cannot allocate it!\n");
        return -ENODEV;
    }

    memset(dev,0,sizeof(*dev));

    dev->tim = stm32_tim_init(BOARD_SENSORS_TIMER);
    if (dev->tim == NULL)
    {
        free(dev);
        return -EINVAL;
    }

    irqstate = up_irq_save(); /* Disable IRQ ================================= */

    //dev->open_count = 0; already done */
    //dev->poll_sem = NULL; already done */
    sem_init(&dev->mutex,  0, 1);

    STM32_TIM_SETISR(dev->tim, stm32_fast_sns_worker, 0);

    ASSERT(stm32_fast_sns == NULL);

    stm32_fast_sns = dev;

    up_irq_restore(irqstate); /* Enable IRQ ================================= */

    return register_driver(BOARD_FAST_SNS_DEV, &fast_sns_ops, 0444, dev);

}


/****************************************************************************
 * Name: stm32_fast_sns_open
 ****************************************************************************/

static int stm32_fast_sns_open(file_t * filep)
{
    int ret;
    FAR struct inode *inode = filep->f_inode;
    FAR stm32_fast_sns_dev_t *dev    = inode->i_private;

    ASSERT( dev != NULL );

    ret = stm32_fast_sns_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    dev->open_count++;

    if ( dev->open_count == 1 )
    {
        STM32_TIM_SETMODE(dev->tim, STM32_TIM_MODE_UP);

        /* SETCLOCK should be after SET PERIOD do be sure that current counter 
         * value is not already after period value
         */
        STM32_TIM_SETPERIOD(dev->tim, TIMER_CLK_FREQ); /* 1 Hz by default */
        STM32_TIM_SETCLOCK(dev->tim, TIMER_CLK_FREQ); /* base time 1ms */

        STM32_TIM_ENABLEINT(dev->tim, 0);
        dev->missing_nbr = 0;
    }

    stm32_fast_sns_givesem( &dev->mutex );

    return OK;
}

/****************************************************************************
 * Name: stm32_fast_sns_close
 ****************************************************************************/

static int stm32_fast_sns_close(file_t * filep)
{
    int ret;
    FAR struct inode *inode = filep->f_inode;
    FAR stm32_fast_sns_dev_t *dev    = inode->i_private;

    ret = stm32_fast_sns_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    dev->open_count--;

    DEBUGASSERT(dev->open_count >= 0);

    if ( dev->open_count == 0 )
    {
        STM32_TIM_DISABLEINT(dev->tim, 0);
    }

    stm32_fast_sns_givesem( &dev->mutex );

    return OK;
}


/****************************************************************************
 * Name: stm32_fast_sns_ioctl
 ****************************************************************************/

static int stm32_fast_sns_ioctl(file_t *filep, int cmd, unsigned long arg)
{
    int ret;
    FAR struct inode *inode         = filep->f_inode;
    FAR stm32_fast_sns_dev_t *dev    = inode->i_private;

    irqstate_t irqstate;

    ASSERT( dev != NULL );

    ret = stm32_fast_sns_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    switch(cmd)
    {
        case SENSORS_FREQUENCY: 
            /* saturate to 100Hz second */
            if ( arg > 0 )
            {
                if ( arg > 100 )
                    arg = 100;
                arg = TIMER_CLK_FREQ/arg;
            }
            else 
                arg = 0;

            STM32_TIM_SETPERIOD(dev->tim, arg);
            break;

        case SENSORS_RESET_FIFO: 
            irqstate = up_irq_save(); /* disable IRQ */
            dev->inndx = 0;
            dev->outndx = 0;
            up_irq_restore(irqstate); /* enabled IRQ */

        case SENSORS_RPM_RATE: 
            if ( arg != 0 )
            {
                if (dev->rpm == NULL)
                    dev->rpm = stm32_ppm_init(GPIO_RPM_PPM_ID,BOARD_RPM_PPM_FREQ);
                if (dev->rpm == NULL)
                {
                    fast_snserr("Cannot initialize RPM \n");
                }
                dev->rpm_rate = arg;
            }
            break;

        case SENSORS_SPEED_RATE: 
            if (dev->speed == NULL)
                dev->speed = stm32_ppm_init(GPIO_SPEED_PPM_ID,BOARD_RPM_PPM_FREQ);
            if (dev->speed == NULL)
            {
                fast_snserr("Cannot initialize Speed \n");
            }
            dev->speed_rate = arg*3600; /* Pulse/second*3600 = Pulse/hour */
            break;

        case SENSORS_SUSPENSION_FRONT:
            dev->suspension_front_type = arg;
            break;

        case SENSORS_SUSPENSION_REAR:
            dev->suspension_rear_type = arg;
            break;
        case SENSORS_BREAK_TYPE:
            dev->break_type = arg;
            break;

        default:
            ret = -EINVAL;
    }

    stm32_fast_sns_givesem( &dev->mutex );

    return ret;
}

/****************************************************************************
 * Name: stm32_fast_sns_poll
 ****************************************************************************/

#ifndef CONFIG_DISABLE_POLL
static int stm32_fast_sns_poll(file_t * filep, FAR struct pollfd *fds, bool setup)
{
    int ret;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_fast_sns_dev_t *dev    = inode->i_private;
    irqstate_t irqstate;

    ret = stm32_fast_sns_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    if (setup)
    {

        fds->revents = 0;
        /* This is a request to set up the poll.  Find an available
         * slot for the poll structure reference
         */

        if ( dev->poll_sem != NULL)
        {
            ret = -EINVAL;
            goto errout;
        }

        irqstate = up_irq_save();
        if ( dev->inndx != dev->outndx )
        {
            fds->revents |= (fds->events & POLLIN);
        }
        up_irq_restore(irqstate);

        if ( fds->revents == 0 )
        {
            dev->poll_sem = fds->sem ;
        }
        else
        {
            sem_post(fds->sem);
            ret = 1;
        }
    }
    else if ( dev->poll_sem == fds->sem )
    {
        irqstate = up_irq_save();
        if ( dev->inndx != dev->outndx )
        {
            fds->revents |= (fds->events & POLLIN);
        }
        up_irq_restore(irqstate);
        dev->poll_sem = NULL;
    }
errout:
    stm32_fast_sns_givesem(&dev->mutex);
    return ret;
}
#endif

/****************************************************************************
 * Name: stm32_fast_sns_read
 ****************************************************************************/

static ssize_t stm32_fast_sns_read(file_t * filep, FAR char *buf, size_t buflen)
{
    int ret;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_fast_sns_dev_t *dev    = inode->i_private;

    fast_sns_t* ptr = (fast_sns_t*)buf;
    ssize_t size = 0;

    ASSERT( dev == NULL );

    ret = stm32_fast_sns_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    while ( buflen >= sizeof(fast_sns_t) )
    {
        irqstate_t irqstate;
        int len = buflen;
        UNUSED(len);

        /* first lock it then only try lock */
        irqstate = up_irq_save(); /* disable IRQ */

        if ( dev->inndx == dev->outndx )
        {
            up_irq_restore(irqstate); /* enabled IRQ */

            if ( size == 0 )
            {
                sem_wait( &dev->rd_sem);
            }
            else if ( sem_trywait( &dev->rd_sem ) < 0 )
            {
                break;
            }

            irqstate = up_irq_save(); /* disable IRQ */
        }
        else
        {
            int ndx = dev->outndx;

            *ptr = dev->buf[ndx];

            ndx++;
            if (ndx >= FIFO_SIZE )
                ndx -= FIFO_SIZE;

            dev->outndx = ndx;
        }

        up_irq_restore(irqstate); /* enabled IRQ */


        fast_snsinfo("Read %d bytes of ev %d timespec %8d.%3d\n",
                      len,
                      ptr->tp.tv_sec,
                      ptr->tp.tv_nsec/1000000
                     );

        ptr++;
        buflen  -= sizeof(fast_sns_t);
        size    += sizeof(fast_sns_t); 
    }

    stm32_fast_sns_givesem( &dev->mutex );

    return size;
}




