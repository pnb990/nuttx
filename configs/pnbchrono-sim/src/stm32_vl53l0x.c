/************************************************************************************
 * configs/pnbchrono-v2/src/stm32_vl53l0x.c
 *
 *   Copyright (C) 2015 Pierre-noel Bouteville. All rights reserved.
 *   Author: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <debug.h>
#include <nuttx/wqueue.h>
#include <nuttx/clock.h>

#include <arch/board/board.h>

#include "pnbchrono-v2.h"

#include "stm32.h"
#include "stm32_i2c.h"

#include <nuttx/i2c/i2c_master.h>
#include "nuttx/sensors/vl53l0x_api.h"
#include "nuttx/sensors/vl53l0x_platform.h"

#if defined(CONFIG_I2C) && defined(CONFIG_VL53L0X)

#define _sninfo sninfo
#define _snerr snerr

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/************************************************************************************
 * Private Data
 ************************************************************************************/

static VL53L0X_Dev_t    stm32_vl53l0x_devs[vl53l0x_eNBR];
static bool             stm32_vl53l0x_lpwork_running = false;
static struct work_s    stm32_vl53l0x_reset_work;

/************************************************************************************
 * Private Functions
 ************************************************************************************/

static void stm32_print_pal_error(VL53L0X_Error Status)
{
    if ( Status == VL53L0X_ERROR_NONE )
    {
        _snerr("API Status: 0 : OK\n");
    }
    else
    {
        char buf[VL53L0X_MAX_STRING_LENGTH];
        VL53L0X_GetPalErrorString(Status, buf);
        _snerr("API Status: %i : %s\n", Status, buf);
    }
} 

static int stm32_vl53l0x_init(VL53L0X_Dev_t * Dev)
{
    VL53L0X_DeviceInfo_t DeviceInfo;
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;

    /* if it is not default address need to change it at each reset or power off
     */
    if ( Dev->I2cDevAddr != BOARD_VL52L0X_I2C_ADDR_DEF )
    {
        uint8_t addr = Dev->I2cDevAddr;
        Dev->I2cDevAddr = BOARD_VL52L0X_I2C_ADDR_DEF;
        VL53L0X_SetDeviceAddress(Dev,addr*2); /* error is not significant */
        Dev->I2cDevAddr = addr;
    }

    /* Set 2V8 Mode and Check presence of Sensor */
    Status = VL53L0X_Set2V8Mode(Dev);
    if(Status != VL53L0X_ERROR_NONE)
        return -1;

    /*
     *  Get the version of the VL53L0X API running in the firmware
     */
    VL53L0X_Version_t Version;
    if ( VL53L0X_GetVersion(&Version) < 0 )
        return -1;

    _sninfo("VL53L0X API Version : %d.%d.%d (revision %d).\n",
            Version.major, Version.minor, Version.build, Version.revision);

    _sninfo ("Call of VL53L0X_DataInit\n");
    Status = VL53L0X_DataInit(Dev,true); // Data initialization
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }


    _sninfo ("Get VL53L0X info\n");
    Status = VL53L0X_GetDeviceInfo(Dev, &DeviceInfo);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    _sninfo("VL53L0X_GetDeviceInfo:\n");
    _sninfo("Device Name             : %s\n", DeviceInfo.Name);
    _sninfo("Device Type             : %s\n", DeviceInfo.Type);
    _sninfo("Device ID               : %s\n", DeviceInfo.ProductId);
    _sninfo("ProductRevisionMajor    : %d\n", DeviceInfo.ProductRevisionMajor);
    _sninfo("ProductRevisionMinor    : %d\n", DeviceInfo.ProductRevisionMinor);

    if ( (DeviceInfo.ProductRevisionMinor != 1) && \
         (DeviceInfo.ProductRevisionMinor != 1)
       ) 
    {
        _sninfo("Error expected cut 1.1 but found cut %d.%d\n",
                DeviceInfo.ProductRevisionMajor, 
                DeviceInfo.ProductRevisionMinor);
        return -1;
    }
    return 0;
}

int stm32_vl53l0x_cfg(VL53L0X_Dev_t * Dev)
{
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;
    uint16_t id;
    uint32_t refSpadCount;
    uint8_t isApertureSpads;
    uint8_t VhvSettings;
    uint8_t PhaseCal;

    _sninfo("Call of VL53L0X_StaticInit\n");
    Status = VL53L0X_StaticInit(Dev);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    _sninfo("Call of VL53L0X_PerformRefCalibration\n");
    Status = VL53L0X_PerformRefCalibration(Dev, &VhvSettings, &PhaseCal);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }


    _sninfo("Call of VL53L0X_PerformRefSpadManagement\n");
    Status = VL53L0X_PerformRefSpadManagement(Dev, &refSpadCount, 
                                              &isApertureSpads);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    _sninfo("refSpadCount = %d, isApertureSpads = %d\n", refSpadCount, 
            isApertureSpads);

    _sninfo("Call of VL53L0X_SetDeviceMode\n");
//    Status = VL53L0X_SetDeviceMode(Dev, VL53L0X_DEVICEMODE_CONTINUOUS_RANGING);
    Status = VL53L0X_SetDeviceMode(Dev, VL53L0X_DEVICEMODE_SINGLE_RANGING);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    id = VL53L0X_CHECKENABLE_SIGMA_FINAL_RANGE;
    Status = VL53L0X_SetLimitCheckEnable(Dev, id, 1);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    id = VL53L0X_CHECKENABLE_SIGNAL_RATE_FINAL_RANGE;
    Status = VL53L0X_SetLimitCheckEnable(Dev, id, 1);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    id = VL53L0X_CHECKENABLE_SIGNAL_RATE_FINAL_RANGE;
    Status = VL53L0X_SetLimitCheckValue(Dev, id, (FixPoint1616_t)(0.25*65536));
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    id = VL53L0X_CHECKENABLE_SIGMA_FINAL_RANGE;
    Status = VL53L0X_SetLimitCheckValue(Dev, id, (FixPoint1616_t)(32*65536));			
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    Status = VL53L0X_SetMeasurementTimingBudgetMicroSeconds(Dev, 30000);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    _sninfo("Call of VL53L0X_SetGpioConfig\n");
    Status = VL53L0X_SetGpioConfig(Dev, 0, VL53L0X_GPIOFUNCTIONALITY_OFF,
                                   VL53L0X_GPIOFUNCTIONALITY_OFF,
                                   VL53L0X_INTERRUPTPOLARITY_LOW);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    _sninfo("Call of VL53L0X_StartMeasurement\n");
    Status = VL53L0X_StartMeasurement(Dev);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }

    return 0;
}



int stm32_vl53l0x_read( VL53L0X_Dev_t * Dev, int* val )
{
    int ret = 0;
    uint8_t ready = 0; 
    uint32_t mask;

    VL53L0X_RangingMeasurementData_t    data;
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;

    Status = VL53L0X_GetMeasurementDataReady(Dev,&ready);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
        return -1;
    }
    if ( ! ready )
        return -1;

    Status = VL53L0X_GetRangingMeasurementData(Dev, &data);

    // Clear the interrupt
    mask = VL53L0X_REG_SYSTEM_INTERRUPT_GPIO_NEW_SAMPLE_READY;
    VL53L0X_ClearInterruptMask(Dev, mask);

    _sninfo("Measured distance: %i\n\n", data.RangeMilliMeter);

    *val = data.RangeMilliMeter;

    return ret;
}


int stm32_vl53l0x_pcf_reset( FAR struct i2c_master_s *i2c, bool active )
{
    int ret;
    struct i2c_msg_s msg;
    uint8_t buf[2];

    /* Setup 8-bit MPU address write message */

    msg.frequency  = CONFIG_VL53L0X_I2C_FREQ;  /* I2C frequency             */
    msg.addr       = BOARD_PCF9536_I2C_ADDR;   /* 7-bit address             */
    msg.flags      = 0;                        /* Write transaction         */
    msg.buffer     = buf;                      /* Transfer from this address*/
    msg.length     = 2;                        /* Send one byte address     */

    /* set P0 as Output */
    buf[0] = 0x03; 
    buf[1] = 0xFE;

    ret = I2C_TRANSFER(i2c, &msg, 1);
    if (ret < 0)
        return -1;

    /* set P0 value */
    buf[0] = 0x01; 
    buf[1] = (active)?0:1;

    ret = I2C_TRANSFER(i2c, &msg, 1);
    if (ret < 0)
        return -1;

    syslog(LOG_INFO,"PCF9638: Set output reset : %s\n", (active)?"On":"Off");

    return 0;
}

static int stm32_vl53l0x_reset(vl53l0x_id_t id)
{
    int ret = 0;
    VL53L0X_Dev_t* dev = &stm32_vl53l0x_devs[id];

    dev->ready = false;

    /* if we configure front we need to reset rear sensors */
    if (dev->I2cDevAddr != BOARD_VL52L0X_I2C_ADDR_DEF)
        stm32_vl53l0x_pcf_reset(dev->i2c, true); 
    else
        stm32_vl53l0x_pcf_reset(dev->i2c, false); 

    if ( stm32_vl53l0x_init(dev) < 0 )
    {
        syslog(LOG_ERR,"vl53l0x %s not found\n",
               stm32_vl53l0x_id_str(id));
        ret = -1;
    }
    else if ( stm32_vl53l0x_cfg(dev) < 0 )
    {
        syslog(LOG_ERR,"vl53l0x %s Init Error\n",
               stm32_vl53l0x_id_str(id));
        ret = -1;
    }
    else
    {
        syslog(LOG_NOTICE,"vl53l0x %s found\n",
               stm32_vl53l0x_id_str(id));
    }
    if ( ret >= 0 )
    {
        dev->ready = true;
    }
    return ret;
}

static void stm32_vl53l0x_reset_worker(FAR void *arg)
{
    int i;
    for (i = 0; i < vl53l0x_eNBR; i++)
    {
        stm32_vl53l0x_reset(i);
    }
}

/************************************************************************************
 * Public Functions
 ************************************************************************************/

const char *stm32_vl53l0x_id_str(vl53l0x_id_t id)
{
    switch(id)
    {
        case vl53l0x_eID_FONT: return "Front";
        case vl53l0x_eID_REAR: return "Rear";
        default: return "Unknown";
    }
}


int stm32_vl53l0x_lpwork_reset(void)
{
    int i;
    if ( stm32_vl53l0x_lpwork_running )
    {
        _sninfo("FAST Sns LPWorker not finised!\n");
        return -1;
    }

    for (i = 0; i < vl53l0x_eNBR; i++)
    {
        stm32_vl53l0x_devs[i].ready = false;
    }

    stm32_vl53l0x_lpwork_running = true;

    if ( work_queue(LPWORK, &(stm32_vl53l0x_reset_work), 
                    stm32_vl53l0x_reset_worker, NULL, 0 ) != OK )
    {
        stm32_vl53l0x_lpwork_running = false;
        _sninfo("Cannot register worker !\n");
        return -1;
    }

    return 0;
}

int stm32_vl53l0x_initialize(void)
{
    FAR struct i2c_master_s *i2c;
    int ret = 0;
    int i;

    /* Initialize I2C */

    memset(stm32_vl53l0x_devs,0,sizeof(stm32_vl53l0x_devs));

    i2c = stm32_i2cbus_initialize(GPIO_VL53L0X_I2C);

    if (!i2c)
    {
        return -ENODEV;
    }

    // Set I2C address I2C

    stm32_vl53l0x_devs[vl53l0x_eID_FONT].I2cDevAddr \
        = BOARD_VL52L0X_I2C_ADDR_FRONT;
    stm32_vl53l0x_devs[vl53l0x_eID_REAR].I2cDevAddr \
        = BOARD_VL52L0X_I2C_ADDR_REAR;

    for (i = 0; i < vl53l0x_eNBR; i++)
    {
        stm32_vl53l0x_devs[i].i2c = i2c;
    }

    stm32_vl53l0x_lpwork_reset();

    return ret;
}

int stm32_vl53l0x_get_range(vl53l0x_id_t id)
{
    int val;
    VL53L0X_Error Status;

    ASSERT( ((unsigned)id) < vl53l0x_eNBR );

    if ( stm32_vl53l0x_devs[id].i2c == NULL )
        return -1;

    if( stm32_vl53l0x_read( &stm32_vl53l0x_devs[id], &val ) < 0 )
    {
        val = -1;
    }

    Status = VL53L0X_StartMeasurement(&stm32_vl53l0x_devs[id]);
    if(Status != VL53L0X_ERROR_NONE)
    {
        stm32_print_pal_error(Status);
    }

    return val;
}


#endif /* CONFIG_I2C && CONFIG_MPL115A && CONFIG_STM32_I2C1 */
