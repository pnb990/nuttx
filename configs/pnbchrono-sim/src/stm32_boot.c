/************************************************************************************
 * configs/pnbchrono-v2/src/stm32_boot.c
 *
 *   Copyright (C) 2015 Pierre-Noel Bouteville. All rights reserved.
 *   Author: Pierre-Noel Bouteville <pnb990@gmail.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <debug.h>

#include <nuttx/arch.h>
#include <nuttx/board.h>
#include <arch/board/board.h>
#include <arch/board/btldr.h>
#include "chip.h"
#include "up_arch.h"
#include "up_internal.h"
#include "stm32.h"
#include "stm32_rcc.h"
#include "pnbchrono-v2.h"


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

#define ARMV7M_NVIC_BASE                0xe000e000
#define NVIC_VECTAB_TBLOFF_MASK         (0xffffff80)
#define NVIC_VECTAB_OFFSET              0x0d08 /* Vector table offset register */
#define NVIC_VECTAB                     (ARMV7M_NVIC_BASE + NVIC_VECTAB_OFFSET)


/************************************************************************************
 * Private Functions
 ************************************************************************************/

uint8_t *firm_get_boot_address(void);

#ifdef CONFIG_PM
static void led_pm_notify(struct pm_callback_s *cb, enum pm_state_e pmstate);
static int led_pm_prepare(struct pm_callback_s *cb, enum pm_state_e pmstate);
#endif

/****************************************************************************
 * Private Data
 ****************************************************************************/

#ifdef CONFIG_BTLDR_MODE
static uint32_t stm32_reset_cause = 0;
#endif
#ifdef CONFIG_PM
static struct pm_callback_s g_bootscb =
{
  .notify  = led_pm_notify,
  .prepare = led_pm_prepare,
};
#endif

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: led_pm_notify
 *
 * Description:
 *   Notify the driver of new power state. This callback is called after
 *   all drivers have had the opportunity to prepare for the new power state.
 *
 ****************************************************************************/

#ifdef CONFIG_PM
static void led_pm_notify(struct pm_callback_s *cb , enum pm_state_e pmstate)
{
  switch (pmstate)
    {
      case(PM_NORMAL):
        {
          /* Restore normal LEDs operation */

        }
        break;

      case(PM_IDLE):
        {
          /* Entering IDLE mode - Turn leds off */

        }
        break;

      case(PM_STANDBY):
        {
          /* Entering STANDBY mode - Logic for PM_STANDBY goes here */

        }
        break;

      case(PM_SLEEP):
        {
          /* Entering SLEEP mode - Logic for PM_SLEEP goes here */

        }
        break;

      default:
        {
          /* Should not get here */

        }
        break;
    }
}
#endif

/****************************************************************************
 * Name: led_pm_prepare
 *
 * Description:
 *   Request the driver to prepare for a new power state. This is a warning
 *   that the system is about to enter into a new power state. The driver
 *   should begin whatever operations that may be required to enter power
 *   state. The driver may abort the state change mode by returning a
 *   non-zero value from the callback function.
 *
 ****************************************************************************/

#ifdef CONFIG_PM
static int led_pm_prepare(struct pm_callback_s *cb , enum pm_state_e pmstate)
{
  /* No preparation to change power modes is required by the LEDs driver.
   * We always accept the state change by returning OK.
   */

  return OK;
}
#endif


#ifdef CONFIG_BTLDR_MODE
static void stm32_boot_jump(uint32_t sp,uint32_t pc)
{
    //__set_PSP(sp);
    //__asm__ volatile ("msr psp,r0"); doesn't work with full optimisation
    __asm__ volatile ("MSR psp, %0\n" : : "r" (sp) );

    //__set_MSP(sp);
    //__asm__ volatile ("msr msp,r0"); doesn't work with full optimisation
    __asm__ volatile ("MSR msp, %0\n" : : "r" (sp) );

    /* Jump to PC (r1)*/
    (void)(pc);
    //__asm__ volatile ("blx r1"); doesn't work with full optimisation
    __asm__ volatile ("blx %0\n" : : "r" (pc) );
}
#endif

#ifdef CONFIG_BTLDR_MODE
static void stm32_start_fw(void)
{
    uint32_t *addr;

    addr = (uint32_t*)firm_get_boot_address();
    if ( addr != NULL )
    {
        btldr_mode = btldrMODE_TEST_FIRM;

/* normally Nuttx set it at start */
#if 0
        /* The vector table must be aligned */

        DEBUGASSERT((((uint32_t)addr) & ~NVIC_VECTAB_TBLOFF_MASK) == 0);

        /* Now configure the NVIC to use the new vector table. */

        putreg32((uint32_t)addr, NVIC_VECTAB);

        /* The number bits required to align the RAM vector table seem to vary
         * from part-to-part.  The following assertion will catch the case where
         * the table alignment is insufficient.
         */

        irqinfo("NVIC_VECTAB=%08x\n", getreg32(NVIC_VECTAB));
        DEBUGASSERT(getreg32(NVIC_VECTAB) == (uint32_t)addr);
#endif

        stm32_boot_jump(*addr,*(addr+1));
    }
}
#endif


#define ansiBACK(color)     "\033[4"ansi##color"m"
#define ansiTEXT(color)     "\033[3"ansi##color"m"
#define ansi(text,back)     ansiTEXT(text)ansiBACK(back)

#define ansiBLACK      "0"
#define ansiRED        "1"
#define ansiGREEN      "2"
#define ansiYELLOW     "3"
#define ansiBLUE       "4"
#define ansiMAGENTA    "5"
#define ansiCYAN       "6"
#define ansiWHITE      "7"
#define ansiDEFAULT    "9"

static const char *prefix[] =
{
    ansi(BLACK ,RED   )"EME",  /* System is unusable */
    ansi(BLACK ,YELLOW)"ALE",  /* Action must be taken immediately */
    ansi(BLACK ,WHITE )"CRI",  /* Critical conditions */
    ansi(RED   ,BLACK )"ERR",  /* Error conditions */
    ansi(YELLOW,BLACK )"WAR",  /* Warning conditions */
    ansi(CYAN  ,BLACK )"NOT",  /* Normal, but significant, condition */
    ansi(GREEN ,BLACK )"INF",  /* Informational message */
    ansi(WHITE ,BLACK )"DEB"   /* Debug-level message */
};

#include <nuttx/streams.h>
void up_syslog_custom_prefix(FAR struct lib_outstream_s* stream,int priority)
{

    DEBUGASSERT((unsigned)priority <= LOG_DEBUG);
    (void)lib_sprintf(stream,prefix[priority]);
}


/************************************************************************************
 * Public Functions
 ************************************************************************************/

#if defined(CONFIG_ARCH_BOARD_STM32_EARLYINIT)
void stm32_early_init(void)
{
#ifdef CONFIG_BTLDR_MODE

    stm32_reset_cause = getreg32(STM32_RCC_CSR);
    /* clear reset cause */
    stm32_reset_cause |= RCC_CSR_RMVF;
    putreg32(stm32_reset_cause,STM32_RCC_CSR);

    /* by default run firmware */
    if ( stm32_reset_cause & RCC_CSR_PORRSTF )
    {
        btldr_mode = btldrRUN_FIRM;
    }

    GPIO_BTLDR_PIN_SET();

    up_mdelay(100);

    /* if btldr force key is pressed start btldr */
    if ( GPIO_IS_BTLDR_MODE_FORCED() )
    {
        btldr_mode = btldrMODE_BTLDR;
    }

    GPIO_BTLDR_PIN_RESET();

    switch (btldr_mode)
    {
        case btldrMODE_BTLDR:     
            //Explicit start bootloader
            break;

        case btldrMODE_TEST_FIRM: 
            //FW not started, then go back to the bootloader
            break;

        case btldrMODE_FIRM_WAS_OK:
        case btldrRUN_FIRM:       
            stm32_start_fw();
            break;
    }
#endif
}
#endif

#if 0
static void stm32_i2c_detect(int port , int frequency)
{
    FAR struct i2c_master_s *i2c;
    struct i2c_msg_s msg[1];
    int i;
    uint8_t buf[2];

    /* Initialize I2C */

    i2c = stm32_i2cbus_initialize(port);

    ASSERT(i2c != NULL);

    for (i = 0x03; i <= 0x77; i++)
    {
        int ret;
        msg[0].frequency = frequency;
        msg[0].addr      = i;
        msg[0].flags     = I2C_M_READ;
        msg[0].buffer    = buf;        
        msg[0].length    = 2;           
                                       
        /* Perform the transfer */

        ret = I2C_TRANSFER(i2c, msg, 1);
        syslog(LOG_INFO,"I2C addr 0x%02X : %s (%d)\n", i,(ret>=0)?"OK":"ERR",
               ret);

        /* NOTE : I2C_TRANSFER return EAGAIN if buffer length is equal to 1 */

        up_mdelay(1);
    }
}
#else
#   define stm32_i2c_detect(port,frequency)
#endif

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void)
{

#ifdef CONFIG_ARCH_LEDS
    board_autoled_initialize();
#endif

#ifdef CONFIG_STM32_OTGFS
    stm32_usbdev_init();
#endif

    /* test log Message */

    /* reset color */
    syslog(LOG_EMERG,ansi(DEFAULT ,DEFAULT));

    syslog(LOG_DEBUG,"STM32 Board initialization.\n");

    syslog(LOG_DEBUG    ,   "LOG_DEBUG    \n");
    syslog(LOG_INFO     ,   "LOG_INFO     \n");
    syslog(LOG_NOTICE   ,   "LOG_NOTICE   \n");
    syslog(LOG_WARNING  ,   "LOG_WARNING  \n");
    syslog(LOG_ERR      ,   "LOG_ERR      \n");
    syslog(LOG_CRIT     ,   "LOG_CRIT     \n");
    syslog(LOG_ALERT    ,   "LOG_ALERT    \n");
    syslog(LOG_EMERG    ,   "LOG_EMERG    \n");

}


/****************************************************************************
 * Name: board_initialize
 *
 * Description:
 *   If CONFIG_BOARD_INITIALIZE is selected, then an additional
 *   initialization call will be performed in the boot-up sequence to a
 *   function called board_initialize().  board_initialize() will be
 *   called immediately after up_initialize() is called and just before the
 *   initial application is started.  This additional initialization phase
 *   may be used, for example, to initialize board-specific device drivers.
 *
 ****************************************************************************/

#ifdef CONFIG_BOARD_INITIALIZE
void board_initialize(void)
{

#ifdef GPIO_POWER_ON
    stm32_configgpio(GPIO_POWER_ON);
#endif
#ifdef GPIO_GPS_RESET
    stm32_configgpio(GPIO_GPS_RESET);
#endif
#ifdef GPIO_WIFI_GPIO0 
    stm32_configgpio(GPIO_WIFI_GPIO0);
#endif
#ifdef GPIO_WIFI_GPIO2 
    stm32_configgpio(GPIO_WIFI_GPIO2);
#endif
#ifdef GPIO_WIFI_RST   
    stm32_configgpio(GPIO_WIFI_RST);
#endif
#ifdef GPIO_WIFI_CHPD  
    stm32_configgpio(GPIO_WIFI_CHPD);
#endif

  stm32_i2c_detect(GPIO_MLX90614_I2C, 100000);

#ifdef CONFIG_PWM
    syslog(LOG_NOTICE,"initialize PWM \n");
    if ( stm32_pwm_setup() < 0 )
    {
        syslog(LOG_ERR,"Cannot initialize PWMs\n");
    }
#else
    stm32_configgpio(GPIO_LED_RED);
    stm32_configgpio(GPIO_LED_YELLOW);
    stm32_configgpio(GPIO_LED_GREEN2);
    stm32_configgpio(GPIO_LED_GREEN1);
#endif

    syslog(LOG_NOTICE,"initialize LCD \n");
    if ( board_lcd_initialize() != OK )
    {
        syslog(LOG_ERR,"Cannot initialize LCD \n");
    }

#ifdef CONFIG_ARCH_KEYBOARD
    /* initialise Key pad */
    if ( stm32_gpio_kbd_init() != OK )
    {
        syslog(LOG_ERR,"Cannot initialize keypad \n");
    }
#endif

    syslog(LOG_NOTICE,"Board Ready \n");

#ifndef CONFIG_BTLDR_MODE
    if ( btldr_mode == btldrMODE_TEST_FIRM )
        btldr_mode = btldrMODE_FIRM_WAS_OK;
#endif

}
#endif
