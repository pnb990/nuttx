/************************************************************************************
 * configs/pnbchrono-v2/src/stm32_board.c
 *
 *   Copyright (C) 2015 Pierre-Noel Bouteville. All rights reserved.
 *   Author: Pierre-Noel Bouteville <pnb990@gmail.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <debug.h>

#include <nuttx/arch.h>
#include <nuttx/board.h>
#include <arch/board/board.h>
#include <arch/board/btldr.h>
#include <nuttx/fs/mkfatfs.h>

#include "chip.h"
#include "up_arch.h"
#include "up_internal.h"
#include "stm32.h"
#include "pnbchrono-v2.h"

#include <string.h>
#include <sys/mount.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

//#define LOG(...) syslog(__VA_ARGS__)
#define LOG(...)

/************************************************************************************
 * Private Functions
 ************************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

static unsigned int  sdcard_mounted_cnt;
static sem_t       mutex;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static int board_ioctl_protected(unsigned int cmd, uintptr_t arg);

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/


/**
  @brief initialise board for specific application.
    Perform application specific initialization.  This function is never
    called directly from application code, but only indirectly via the
    (non-standard) boardctl() interface using the command BOARDIOC_INIT.
 
  @param[in] arg : The boardctl() argument is passed to the 
   board_app_initialize() implementation without modification.  
   The argument has no meaning to NuttX; the meaning of the argument is a 
   contract between the board-specific initalization logic and the the
   matching application logic.  The value cold be such things as a
   mode enumeration value, a set of DIP switch switch settings, a
   pointer to configuration data read from a file or serial FLASH,
   or whatever you would like to do with it.  Every implementation
   should accept zero/NULL as a default configuration.
 
   @return Zero (OK) is returned on success; a negated errno value is 
   returned on any failure to indicate the nature of the failure.
 */
int board_app_initialize(uintptr_t arg)
{
    UNUSED(arg);
    sdcard_mounted_cnt = 0;
    ASSERT(sem_init(&mutex, 0, 1) == 0);

#if defined(CONFIG_TRACKING_SENSORS_DRIVER)
    if ( stm32_analog_init() != OK )
    {
        syslog(LOG_ERR,"Cannot initialize Analog \n");
    }

    syslog(LOG_NOTICE,"initialize CHRONO \n");
    if ( stm32_chrono_init() < 0 )
    {
        syslog(LOG_ERR,"Cannot initialize Chrono\n");
    }

#if defined(CONFIG_I2C) && defined(CONFIG_MLX90614)
    syslog(LOG_NOTICE,"initialize ranging sensors (VL53L0X) \n");
    if ( stm32_vl53l0x_initialize() < 0 )
    {
        syslog(LOG_ERR,"Cannot initialize ranging sensors (VL53L0X)\n");
    }
#endif

    syslog(LOG_NOTICE,"initialize Fast sensors \n");
    if ( stm32_fast_sns_init() < 0 )
    {
        syslog(LOG_ERR,"Cannot initialize fast sensors\n");
    }

#if defined(CONFIG_I2C)
    syslog(LOG_NOTICE,"initialize MPU \n");
    if ( stm32_initialize_mpu() < 0 )
    {
        syslog(LOG_ERR,"Cannot initialize MPU\n");
    }
#endif

#if defined(CONFIG_I2C) && defined(CONFIG_MLX90614)
    syslog(LOG_NOTICE,"initialize IR temperature \n");
    if ( stm32_initialize_mlx90614() < 0 )
    {
        syslog(LOG_ERR,"Cannot initialize IR temperature\n");
    }
#endif
#endif

#ifdef CONFIG_BMP180
    /* initialize BMP180 */
    //if ( stm32_bmp180initialize("/dev/bmp180") )
    {
        syslog(LOG_ERR,"Cannot initialize BMP180\n");
    }
#endif

    /* Mount the SDIO-based MMC/SD block driver */

    syslog(LOG_NOTICE,"initialize SDIO.\n");
#ifndef CONFIG_MMCSD_MULTIBLOCK_DISABLE
    syslog(LOG_NOTICE,"Warning  SDIO MULTIBLOCK Not diesabled !!!!!!!!!!!!!\n");
#endif
    if ( stm32_sdio_initialize() < 0 )
    {
        syslog(LOG_ERR,"Cannot initialize some spi devices\n");
    }

#ifdef CONFIG_PM
    syslog(LOG_NOTICE,"register PM \n");
    if ( pm_register(&g_bootscb) )
    {
        syslog(LOG_ERR,"Cannot Register PM\n");
    }
#endif

#if (defined(CONFIG_FS_PROCFS) && defined(CONFIG_STM32_CCM_PROCFS))
    /* Register the CCM procfs entry.  This must be done before the procfs is
     * mounted.
     */
    (void)ccm_procfs_register();
#endif

    return OK;
}


/**
  @brief Give board_ioctl \b cmd corresponding string.
  @param[in] cmd board_ioctl cmd.
  @return Corresponding string of \b cmd.
 */
static inline const char *board_ioctl_str(int cmd)
{
    switch (cmd)
    {
        case BIOC_REBOOT_BTLDR:         return "BIOC_REBOOT_BTLDR";
        case BIOC_SDCARD_FORMAT:        return "BIOC_SDCARD_FORMAT";
        case BIOC_SDCARD_MOUNT:         return "BIOC_SDCARD_MOUNT";
        case BIOC_SDCARD_IS_MOUNTED:    return "BIOC_SDCARD_IS_MOUNTED";
        case BIOC_SDCARD_UNMOUNT:       return "BIOC_SDCARD_UNMOUNT";
        case BIOC_POWER_OFF:            return "BIOC_POWER_OFF";
        case BIOC_USB_IS_CONNECTED:     return "BIOC_USB_IS_CONNECTED";
        case BIOC_USB_IS_ENABLED:       return "BIOC_USB_IS_ENABLED";
        case BIOC_USB_MSC_ENABLE:       return "BIOC_USB_MSC_ENABLE";
        case BIOC_USB_MSC_DISABLE:      return "BIOC_USB_MSC_DISABLE";
        case BIOC_IS_CHARGING:          return "BIOC_IS_CHARGING";
        case BIOC_BAT_LEVEL:            return "BIOC_BAT_LEVEL";
        case BIOC_VBAT:                 return "BIOC_VBAT";
        case BIOC_VPWR:                 return "BIOC_VPWR";
        default:                        return "Unkown";
    }
}

/**
  @brief part of ioctl that should be protected by mutex.
  @param[in] cmd ioctl command value
  @param[in] arg ioctl argument value
  @return ioctl return code.
 */
#ifdef CONFIG_BOARDCTL_IOCTL
static int board_ioctl_protected(unsigned int cmd, uintptr_t arg)
{
    int ret = 0;
    switch(cmd)
    {
        case BIOC_SDCARD_FORMAT:
            if ( sdcard_mounted_cnt == 0 )
            {
                struct fat_format_s fmt = FAT_FORMAT_INITIALIZER;
                fmt.ff_fattype = 0; /* leave select the right choice */
                strncpy((char*)fmt.ff_volumelabel,"PNBChrono",
                        sizeof(fmt.ff_volumelabel)/sizeof(fmt.ff_volumelabel[0])
                       ); /* Volume label */
                ret = mkfatfs(BOARD_SDHC_BLOCK_DEV_PATH,&fmt);
            }
            else
            {
                ret = -EBUSY;
            }
            break;

        case BIOC_PROCFS_MOUNT:
            if ( mount(NULL, "/proc", "procfs", 0, NULL) < 0 )
                ret = -errno;
            break;

        case BIOC_SDCARD_MOUNT:
            if ( sdcard_mounted_cnt == 0 )
            {
#ifdef CONFIG_USBMSC
                if ( stm32_usbdev_is_enable() )
                {
                    ret = -EBUSY;
                }
                else 
#endif
                    if ( mount(BOARD_SDHC_BLOCK_DEV_PATH,BOARD_SDHC_MOUNT_PATH,
                               BOARD_SDHC_FSTYPE,0,NULL) < 0 )
                {
                    ret = -errno;
                }
            }
            if ( ret >= 0 )
                sdcard_mounted_cnt++;
            break;

        case BIOC_SDCARD_UNMOUNT:
            if ( sdcard_mounted_cnt > 0 )
            {
                sdcard_mounted_cnt--;

                if ( ( sdcard_mounted_cnt == 0 ) && 
                     ( umount(BOARD_SDHC_MOUNT_PATH) < 0 )
                   )
                {
                    ret = -errno;
                    sdcard_mounted_cnt++;
                }
            }
            break;

#ifdef GPIO_POWER_ON
        case BIOC_POWER_OFF:
            if ( arg ) /* if true => force */ 
            {
                while( sdcard_mounted_cnt )
                    board_ioctl_protected(BIOC_SDCARD_UNMOUNT, 0);
            }
            if ( sdcard_mounted_cnt == 0 )
            {
                sched_lock();
                stm32_gpiowrite(GPIO_POWER_ON,false);
                sleep(10);
                up_systemreset();
            }
            ret = -EBUSY;
            break;
#endif
        case BIOC_REBOOT_BTLDR:         
            btldr_mode = btldrMODE_BTLDR;
            if ( arg ) /* if true => force */ 
            {
                while( sdcard_mounted_cnt )
                    board_ioctl_protected(BIOC_SDCARD_UNMOUNT, 0);
            }
            if ( sdcard_mounted_cnt == 0 )
            {
                up_systemreset();
            }
            ret = -EBUSY;
            break;

#ifdef CONFIG_USBMSC
            /* TODO pnb use BOARDIOC_USBDEV_MSC instead */
        case BIOC_USB_MSC_ENABLE:
            if ( sdcard_mounted_cnt > 0 )
                ret = -EBUSY;
            else
                ret = stm32_usbdev_enable_usbmsc();
            break;
#endif

        default:
            ret = -EINVAL;
            break;
    }
    return ret;
}
#endif

/**
  @brief part of ioctl that should be protected by mutex.
  @param[in] cmd ioctl command value
  @param[in] arg ioctl argument value
  @return ioctl return code.
 */
#ifdef CONFIG_BOARDCTL_IOCTL
int board_ioctl(unsigned int cmd, uintptr_t arg)
{
    int val;
    int ret = OK;

    LOG(LOG_NOTICE,"Board iotcl: %s.\n",board_ioctl_str(cmd));

    switch(cmd)
    {
        case BIOC_SDCARD_IS_PRESENT:
            *((bool*)arg) = stm32_cardinserted();
            break;

        case BIOC_SDCARD_IS_MOUNTED:
            *((bool*)arg) = (sdcard_mounted_cnt > 0);
            break;

#ifdef CONFIG_USBMSC
        case BIOC_USB_IS_CONNECTED:
            ret = stm32_usbdev_is_connected();
            *((bool*)arg) = (ret>0)?true:false;
            break;
        case BIOC_USB_IS_ENABLED:
            ret = stm32_usbdev_is_enable();
            *((bool*)arg) = (ret>0)?true:false;
            break;
        case BIOC_USB_MSC_DISABLE:
            ret = stm32_usbdev_disable_usbmsc();
            break;
#endif

        case BIOC_IS_CHARGING:
            ret = board_ioctl(BIOC_VPWR,(uintptr_t)&val);
            if ( ret == 0 )
                *((bool*)arg) = (val>GPIO_VPWR_CHR_MV)?true:false;
            LOG(LOG_INFO,"VPWR %d mV => %s\n",val,
                (*((int*)arg))?"Charging":"Not charging");
            break;

#if defined(CONFIG_TRACKING_SENSORS_DRIVER)
        case BIOC_BAT_LEVEL:
            ret = board_ioctl(BIOC_VBAT,(uintptr_t)&val);

            if ( ( ret == 0 ) && ( val > GPIO_VBAT_MIN ) )
            {
                val -= GPIO_VBAT_MIN;
                val *= 100;
                val /= GPIO_VBAT_MAX-GPIO_VBAT_MIN;
                if (val > 100)
                    val = 100;
            }
            else
                val = 0;

            *((int*)arg) = val;
            LOG(LOG_INFO,"BAT LVL %d mV => %d %%\n",val,*((int*)arg));
            break;
        case BIOC_VBAT:
            *((int*)arg) = GPIO_ADC2VBAT_MV(ana_values[ANA_VBAT]);
            LOG(LOG_INFO,"VBAT %d => %d mV\n",ana_values[ANA_VBAT],
                *((int*)arg));
            break;
        case BIOC_VPWR:
            *((int*)arg) = GPIO_ADC2VPWR_MV(ana_values[ANA_VPWR]);
            LOG(LOG_INFO,"VPWR %d => %d mV\n",ana_values[ANA_VPWR],
                *((int*)arg));
            break;
#endif

        default:
            ret = sem_wait(&mutex);
            if ( ret == 0 )
            {
                ret = board_ioctl_protected(cmd,arg);
                ASSERT(sem_post(&mutex) >= 0);
            }
            break;
    }

    if ( ret < 0 )
    {
        LOG(LOG_ERR,"Cmd %s failed!\n",board_ioctl_str(cmd));
    }

    return ret;
}
#endif




