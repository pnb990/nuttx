/****************************************************************************
 * configs/stm32f4discovery/src/stm32_autoleds.c
 *
 *   Copyright (C) 2016 Bouteville Pierre-Noel. All rights reserved.
 *   Author: Bouteville Pierre-Noel <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <stdbool.h>
#include <debug.h>

#include <nuttx/board.h>
#include <arch/board/board.h>

#include "chip.h"
#include "up_arch.h"
#include "up_internal.h"
#include "stm32.h"
//#include "stm32f4discovery.h"

#ifdef CONFIG_ARCH_LEDS

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/


/****************************************************************************
 * Private Data
 ****************************************************************************/


/****************************************************************************
 * Private Functions
 ****************************************************************************/


static uint32_t board_led_gpio(unsigned int led)
{
    uint32_t gpio = (uint32_t)-1;
    switch(led)
    {
#ifdef GPIO_LED_STARTED
        case LED_STARTED:
            gpio = GPIO_LED_STARTED;
            break;
#endif

#ifdef GPIO_LED_HEAPALLOCATE
        case LED_HEAPALLOCATE:
            gpio = GPIO_LED_HEAPALLOCATE;
            break;
#endif

#ifdef GPIO_LED_IRQSENABLED
        case LED_IRQSENABLED:
            gpio = GPIO_LED_IRQSENABLED;
            break;
#endif

#ifdef GPIO_LED_STACKCREATED
        case LED_STACKCREATED:
            gpio = GPIO_LED_STACKCREATED;
            break;
#endif

#ifdef GPIO_LED_INIRQ
        case LED_INIRQ:
            gpio = GPIO_LED_INIRQ;
            break;
#endif

#ifdef GPIO_LED_SIGNAL
        case LED_SIGNAL:
            gpio = GPIO_LED_SIGNAL;
            break;
#endif

#ifdef GPIO_LED_ASSERTION
        case LED_ASSERTION:
            gpio = GPIO_LED_ASSERTION;
            break;
#endif

#ifdef GPIO_LED_PANIC
        case LED_PANIC:
            gpio = GPIO_LED_STARTED;
            break;
#endif

#ifdef GPIO_LED_IDLE
        case LED_IDLE:
            gpio = GPIO_LED_IDLE;
            break;
#endif

#ifdef GPIO_LED_USER1
        case LED_USER1:
            gpio = GPIO_LED_USER1;
            break;
#endif
    }
    return gpio;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: board_autoled_initialize
 ****************************************************************************/

void board_autoled_initialize(void)
{
   /* Configure LED1-4 GPIOs for output */
    int i = LED_NBR;
    while (i--)
    {
        int gpio = board_led_gpio(i);
        if ( gpio != (uint32_t)-1 )
            stm32_configgpio(gpio);
    }
}

/****************************************************************************
 * Name: board_autoled_on
 ****************************************************************************/

void board_autoled_on(int led)
{
    int gpio = board_led_gpio(led);
    if ( gpio != (uint32_t)-1 )
        stm32_gpiowrite(gpio, true);
}

/****************************************************************************
 * Name: board_autoled_off
 ****************************************************************************/

void board_autoled_off(int led)
{
    int gpio = board_led_gpio(led);
    if ( gpio != (uint32_t)-1 )
        stm32_gpiowrite(gpio, false);
}

#endif /* CONFIG_ARCH_LEDS */
