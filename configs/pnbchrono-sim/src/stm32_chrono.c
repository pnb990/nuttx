/****************************************************************************
 * configs/pnbchrono-v2/src/stm32_chrono.c
 *
 *   Copyright (C) 2015 Pierre-noel Bouteville. All rights reserved.
 *   Author: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>

#include <nuttx/arch.h>
#include <nuttx/irq.h>

#include <arch/irq.h>
#include <arch/board/board.h>
#include <pnbchrono/chrono.h>

#include <string.h>
#include <poll.h>
#include <errno.h>
#include <nuttx/kmalloc.h>

#include "up_arch.h"
#include "stm32_gpio.h"
#include "stm32_capture.h"
#include "pnbchrono-v2.h"

#define STM32_CHRONO_LOG(...)
//#define STM32_CHRONO_LOG(lvl,...)    lldbg("CHRONO:"__VA_ARGS__)
//#define STM32_CHRONO_LOG(...)        syslog("CHRONO:"__VA_ARGS__)

#ifndef CONFIG_STM32_CHRONO_BUFSIZE
#  define CONFIG_STM32_CHRONO_BUFSIZE 64
#endif

#ifndef CONFIG_STM32_CHRONO_FILTER_DELAYS_S
#  define CONFIG_STM32_CHRONO_FILTER_DELAYS_S 1
#endif

#define CHRONO_COUNTER_SIZE (1ULL<<32)
#define CHRONO_COUNTER_PSC  (1)

#if ( defined( CONFIG_CHRONO_EXT_CLK_FREQ ) && \
      ( CONFIG_CHRONO_EXT_CLK_FREQ > 0 ) )
#   define CHRONO_CLK_SRC       STM32_CAP_CLK_EXT
#   define CHRONO_COUNTER_FREQ CONFIG_CHRONO_EXT_CLK_FREQ
#else
#   define CHRONO_CLK_SRC       STM32_CAP_CLK_INT
#   define CHRONO_COUNTER_FREQ  BOARD_CHRONO_CLK_INT_FREQ
#endif


/****************************************************************************
 * Fileops Prototypes and Structures
 ****************************************************************************/

typedef FAR struct file file_t;

static int stm32_chrono_open(       file_t * filep);
static int stm32_chrono_close(      file_t * filep);
static ssize_t stm32_chrono_read(   file_t * filep, FAR char *buf, 
                                      size_t buflen);
static int stm32_chrono_ioctl(      file_t *filep, int cmd, 
                                    unsigned long arg);
#ifndef CONFIG_DISABLE_POLL
static int stm32_chrono_poll(file_t * filep, FAR struct pollfd *fds, 
                                  bool setup);
#endif

static const struct file_operations chrono_ops =
{
    .open   = stm32_chrono_open,
    .close  = stm32_chrono_close,
    .read   = stm32_chrono_read,                 
    .write  = NULL,                
    .seek   = NULL,
    .ioctl  = stm32_chrono_ioctl, 
#ifndef CONFIG_DISABLE_POLL
    .poll   = stm32_chrono_poll, 
#endif
};

/****************************************************************************
 * Name: stm32_chrono_t
 * Description:
 *  variable of keypad
 ****************************************************************************/
typedef struct 
{
    int     freq;           /* current clock frequency */
    uint64_t cnt_offset;    /* number of overflow done by timer */
    int missing_event_nbr;  /* number of event missed due to buffer or capture 
                             * overflow 
                             */
    FAR struct stm32_cap_dev_s *capture;    /* timer used */

    int     open_count;     /* open counter */
    time_t  last_loop_trig;  /* time of last loop trigger */

    /* fifo */

    sem_t   *poll_sem;  /* Poll event semaphore */
    sem_t   mutex;      /* mutex protection */
    sem_t   rd_sem;     /* fifo semaphore */
    int     rd_idx;     /* fifo read index */
    int     wr_idx;     /* fifo write index */
    chrono_t buf[CONFIG_STM32_CHRONO_BUFSIZE]; /* fifo data */
}stm32_chrono_t;

/****************************************************************************
 * Name: stm32_chrono
 * Description:
 *  variable of keypad instance.
 ****************************************************************************/
static stm32_chrono_t* stm32_chrono;

/************************************************************************************
 * Name: stm32_chrono_takesem
 ************************************************************************************/

static int stm32_chrono_takesem(FAR sem_t *sem, bool errout)
{
  /* Loop, ignoring interrupts, until we have successfully acquired the semaphore */

  while (sem_wait(sem) != OK)
    {
      /* The only case that an error should occur here is if the wait was awakened
       * by a signal.
       */

      ASSERT(get_errno() == EINTR);

      /* When the signal is received, should we errout? Or should we just continue
       * waiting until we have the semaphore?
       */

      if (errout)
        {
          return -EINTR;
        }
    }

  return OK;
}

/************************************************************************************
 * Name: stm32_chrono_givesem
 ************************************************************************************/

#define stm32_chrono_givesem(sem) (void)sem_post(sem)


/****************************************************************************
 * Name: stm32_chrono_level
 ****************************************************************************/

static inline int stm32_chrono_level(FAR stm32_chrono_t *dev)
{
    int level = dev->wr_idx - dev->rd_idx;

    if ( level < 0 )
        level += CONFIG_STM32_CHRONO_BUFSIZE;

    return level;
}

/****************************************************************************
 * Name: stm32_chrono_new_event
 ****************************************************************************/

static void stm32_chrono_new_event(chrono_event_t ev, uint64_t time_raw)
{
    struct timespec tp;
    chrono_t *ptr;
    stm32_chrono_t *dev = stm32_chrono;

    ASSERT(dev != NULL);

    time_raw += dev->cnt_offset;

    STM32_CHRONO_LOG(LOG_NOTICE, "ev %d time_raw %16llu\n", ev, time_raw);

    tp.tv_sec  = time_raw/dev->freq;
    tp.tv_nsec = ((time_raw*1000000000)/dev->freq)%1000000000;

    if ( ( ev == chronoEVENT_LOOP ) && ( dev->last_loop_trig >= tp.tv_sec ) )
    {
        STM32_CHRONO_LOG(LOG_NOTICE,"Event LOOP filtered\n");
        return; 
    }

    dev->last_loop_trig = tp.tv_sec+CONFIG_STM32_CHRONO_FILTER_DELAYS_S;

    if (stm32_chrono_level(dev) >= CONFIG_STM32_CHRONO_BUFSIZE)
    {
        dev->missing_event_nbr++;
        return; 
    }

    STM32_CHRONO_LOG(LOG_NOTICE,
                          "timespec %8d.%3d\n",
                          tp.tv_sec,
                          tp.tv_nsec/1000000
                         );

    ptr = &dev->buf[dev->wr_idx];

    ptr->tp     = tp;
    ptr->event  = ev;

    dev->wr_idx++;
    if ( dev->wr_idx >= CONFIG_STM32_CHRONO_BUFSIZE )
        dev->wr_idx = 0;

    sem_post(&dev->rd_sem);

    /* add event to waiting semaphore */
    if ( dev->poll_sem )
    {
        sem_post( dev->poll_sem );
    }
}




/****************************************************************************
 * irq handler
 ****************************************************************************/

static int stm32_chrono_tim_isr_handler (int irq, void *context)
{
    uint64_t val;
    uint32_t cnt;
    stm32_chrono_t *dev = stm32_chrono;
    stm32_cap_flags_t flags;
    chrono_event_t ev;

    ASSERT(dev);

    flags = STM32_CAP_GETFLAGS(dev->capture);
    cnt   = STM32_CAP_GETCAPTURE(dev->capture,STM32_CAP_CHANNEL_COUNTER);
    STM32_CHRONO_LOG(LOG_NOTICE,"Capture flags 0x%08X\n",flags);

#ifdef GPIO_CHRONO_BREAK_CH
    if (flags & STM32_CAP_FLAG_IRQ_CH(GPIO_CHRONO_BREAK_CH))
    {
        ev = chronoEVENT_BREAK_RELEASE;
        if (GPIO_BREAK_PRESSED())
            ev = chronoEVENT_BREAK_PRESSED;

        if (flags & STM32_CAP_FLAG_OF_CH(GPIO_CHRONO_BREAK_CH))
            dev->missing_event_nbr++;

        val = STM32_CAP_GETCAPTURE(dev->capture,GPIO_CHRONO_BREAK_CH);

        if ( (flags & STM32_CAP_FLAG_IRQ_COUNTER) && ( val <= cnt ) )
            val += CHRONO_COUNTER_SIZE;

        stm32_chrono_new_event(ev,val);

    }
#endif

    if (flags & STM32_CAP_FLAG_IRQ_CH(GPIO_CHRONO_LOOP_CH))
    {
        ev = chronoEVENT_LOOP;

        if (flags & STM32_CAP_FLAG_OF_CH(GPIO_CHRONO_LOOP_CH))
            dev->missing_event_nbr++;

        val = STM32_CAP_GETCAPTURE(dev->capture,GPIO_CHRONO_LOOP_CH);

        if ( (flags & STM32_CAP_FLAG_IRQ_COUNTER) && ( val <= cnt ) )
            val += CHRONO_COUNTER_SIZE;

        stm32_chrono_new_event(ev,val);
    }

    if (flags & STM32_CAP_FLAG_IRQ_COUNTER)
    {
        dev->cnt_offset += CHRONO_COUNTER_SIZE;
    }

    STM32_CAP_ACKFLAGS(dev->capture,flags);

    return 0;
}



/****************************************************************************
 * Name: stm32_chrono_init
 *
 * Description:
 *  Initialize All GPIO for key pad.
 * Input parameters:
 *  _key_map    - first key mapping of mapping GPIO<=>Key list.
 *                    to Finish list set Pin with negative value.
 * Returned Value:
 *   None (User allocated instance initialized).
 ****************************************************************************/
int stm32_chrono_init( void )
{
    irqstate_t irqstate;
    stm32_chrono_t *dev;

    /* Disable interrupts until we are done.  This guarantees that the
     * following operations are atomic.
     */

    dev = (stm32_chrono_t*)kmm_malloc(sizeof(stm32_chrono_t));
    if ( dev == NULL )
    {
        STM32_CHRONO_LOG(LOG_ERR,"Cannot allocate it!\n");
        return -ENODEV;
    }

    irqstate = up_irq_save();

    memset(dev,0,sizeof(*dev));

    //dev->open_count = 0; already done */
    //dev->poll_sem = NULL; already done */
    sem_init(&dev->rd_sem, 0, 0);
    sem_init(&dev->mutex,  0, 1);

    dev->capture = stm32_cap_init(GPIO_CHRONO_CAPTURE);

    //stm32_gpioirq(CHRONO);
    //(void)irq_attach(IRQ_CHRONO, stm32_chrono_irq);

    ASSERT(stm32_chrono == NULL);

    stm32_chrono = dev;

    up_irq_restore(irqstate);


    if (!dev->capture)
        return -EINVAL;

    return register_driver("/dev/chrono0", 
                           &chrono_ops, 
                           0444, 
                           dev
                          );
}

/*
 *
 * Name: stm32_chrono_val
 * 
 * return current time spec value of chrono meter.
 */
int stm32_chrono_get_time(struct timespec* tp)
{
    irqstate_t irqstate;
    uint64_t time_raw;
    int     freq;
    stm32_chrono_t *dev = stm32_chrono;

    /* get all needed value */
    irqstate = up_irq_save();

    time_raw = STM32_CAP_GETCAPTURE(dev->capture,STM32_CAP_CHANNEL_COUNTER);
    time_raw += dev->cnt_offset;
    freq = dev->freq;

    up_irq_restore(irqstate);

    tp->tv_sec  = time_raw/freq;
    tp->tv_nsec = ((time_raw*1000000000)/freq)%1000000000;

    return OK;
}

/****************************************************************************
 * Name: stm32_chrono_open
 ****************************************************************************/

static int stm32_chrono_open(file_t * filep)
{
    int ret;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_chrono_t *dev    = inode->i_private;

    ASSERT( dev != NULL );

    ret = stm32_chrono_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    dev->open_count++;

    if ( dev->open_count == 1 )
    {
        stm32_cap_flags_t flags;

        dev->freq = CHRONO_COUNTER_FREQ;
        dev->missing_event_nbr = 0;

        STM32_CAP_SETCLOCK(dev->capture,  
                           CHRONO_CLK_SRC,
                           CHRONO_COUNTER_PSC,
                           CHRONO_COUNTER_SIZE-1
                          );

        STM32_CAP_SETCHANNEL(dev->capture,
                             GPIO_CHRONO_LOOP_CH, 
                             GPIO_CHRONO_LOOP_CFG
                            );
#ifdef GPIO_CHRONO_BREAK_CH
        STM32_CAP_SETCHANNEL(dev->capture,
                             GPIO_CHRONO_BREAK_CH,
                             GPIO_CHRONO_BREAK_CFG
                            );
#endif

        STM32_CAP_SETISR(dev->capture,      stm32_chrono_tim_isr_handler);

        flags = STM32_CAP_FLAG_IRQ_COUNTER;
        flags |=STM32_CAP_FLAG_IRQ_CH(GPIO_CHRONO_LOOP_CH);
#ifdef GPIO_CHRONO_BREAK_CH
        flags |=STM32_CAP_FLAG_IRQ_CH(GPIO_CHRONO_BREAK_CH);
#endif

        STM32_CAP_ACKFLAGS(dev->capture,flags);
        STM32_CAP_ENABLEINT(dev->capture,flags,true);

    }
    stm32_chrono_givesem( &dev->mutex );


    return OK;
}

/****************************************************************************
 * Name: stm32_chrono_close
 ****************************************************************************/

static int stm32_chrono_close(file_t * filep)
{
    int ret;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_chrono_t *dev    = inode->i_private;

    ret = stm32_chrono_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    dev->open_count--;

    DEBUGASSERT(dev->open_count >= 0);

    if ( dev->open_count == 0 )
    {
        STM32_CAP_SETCHANNEL(dev->capture,GPIO_CHRONO_LOOP_CH, STM32_CAP_EDGE_DISABLED);
#ifdef GPIO_CHRONO_BREAK_CH
        STM32_CAP_SETCHANNEL(dev->capture,GPIO_CHRONO_BREAK_CH,STM32_CAP_EDGE_DISABLED);
#endif
    }

    stm32_chrono_givesem( &dev->mutex );

    return OK;
}


/****************************************************************************
 * Name: stm32_chrono_ioctl
 ****************************************************************************/

static int stm32_chrono_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
    int ret;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_chrono_t *dev    = inode->i_private;

    irqstate_t irqstate;

    ASSERT( dev != NULL );

    ret = stm32_chrono_takesem(&dev->mutex, true);

    if (ret < 0)
    {
        /* A signal received while waiting for access to the poll data
         * will abort the operation.
         */

        return ret;
    }


    switch(cmd)
    {
        case CHRONO_RESET_FIFO:
            irqstate = up_irq_save();
            dev->rd_idx = 0;
            dev->wr_idx = 0;
            up_irq_restore(irqstate);
            break;
        case CHRONO_GET_TIME:
            ret = stm32_chrono_get_time((struct timespec*) arg);
            break;
        default:
            ret = -EINVAL;
    }

    stm32_chrono_givesem( &dev->mutex );

    return ret;
}

/****************************************************************************
 * Name: stm32_chrono_poll
 ****************************************************************************/

#ifndef CONFIG_DISABLE_POLL
static int stm32_chrono_poll(file_t * filep, FAR struct pollfd *fds, bool setup)
{
    int ret;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_chrono_t *dev    = inode->i_private;

    ret = stm32_chrono_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    if (setup)
    {
        fds->revents = 0;
        /* This is a request to set up the poll.  Find an available
         * slot for the poll structure reference
         */

        if ( dev->poll_sem != NULL)
        {
            ret = -EINVAL;
            goto errout;
        }

        if ( stm32_chrono_level(dev) > 0 )
        {
            fds->revents |= (fds->events & POLLIN);
        }

        if ( fds->revents == 0 )
        {
            dev->poll_sem = fds->sem ;
        }
        else
        {
            sem_post(fds->sem);
            ret = 1;
        }
    }
    else if ( dev->poll_sem == fds->sem )
    {
        if ( stm32_chrono_level(dev) > 0 )
        {
            fds->revents |= (fds->events & POLLIN);
        }
        dev->poll_sem = NULL;
    }
errout:
    stm32_chrono_givesem(&dev->mutex);
    return ret;
}
#endif


/****************************************************************************
 * Name: stm32_chrono_read
 ****************************************************************************/

static ssize_t stm32_chrono_read(file_t * filep, FAR char *buf, size_t buflen)
{
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_chrono_t *dev    = inode->i_private;

    chrono_t* ptr = (chrono_t*)buf;
    ssize_t size = 0;

    ASSERT( dev != NULL );

    while ( buflen >= sizeof(chrono_t) )
    {
        irqstate_t irqstate;

        /* first lock it then only try lock */

        if ( size == 0 )
            sem_wait( &dev->rd_sem);
        else if ( sem_trywait( &dev->rd_sem ) < 0 )
            break;

        irqstate = up_irq_save(); /* disable IRQ */
        *ptr = dev->buf[dev->rd_idx++];
        if (dev->rd_idx >= CONFIG_STM32_CHRONO_BUFSIZE )
            dev->rd_idx=0;
        up_irq_restore(irqstate); /* enabled IRQ */

        STM32_CHRONO_LOG(LOG_NOTICE,
                              "read ev %d timespec %8d.%3d\n",
                              ptr->event,
                              ptr->tp.tv_sec,
                              ptr->tp.tv_nsec/1000000
                             );

        ptr++;
        buflen  -= sizeof(chrono_t);
        size    += sizeof(chrono_t); 

    }


    return size;
}


