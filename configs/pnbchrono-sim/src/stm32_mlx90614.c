/************************************************************************************
 * configs/pnbchrono-v2/src/stm32_mlx90614.c
 *
 *   Copyright (C) 2015 Pierre-noel Bouteville. All rights reserved.
 *   Author: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <debug.h>

#include <arch/board/board.h>

#include "pnbchrono-v2.h"

#include "stm32.h"
#include "stm32_i2c.h"

#include <nuttx/i2c/i2c_master.h>
#include <nuttx/sensors/mlx90614.h>

#if defined(CONFIG_I2C) && defined(CONFIG_MLX90614)


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

static const struct mlx90614_config_s cfg[BOARD_MLX90614_NBR] =
{
    { 
        .flags      = MLX90614_FLAGS_TOBJ1,
        .i2c_addr   = BOARD_MLX90614_I2C_ADDR(0),
        .i2c_freq   = MLX90614_FREQ_MAX,
    },
    { 
        .flags      = MLX90614_FLAGS_TOBJ1,
        .i2c_addr   = BOARD_MLX90614_I2C_ADDR(1),
        .i2c_freq   = MLX90614_FREQ_MAX,
    },
    { 
        .flags      = MLX90614_FLAGS_TOBJ1,
        .i2c_addr   = BOARD_MLX90614_I2C_ADDR(2),
        .i2c_freq   = MLX90614_FREQ_MAX,
    },
    { 
        .flags      = MLX90614_FLAGS_TOBJ1,
        .i2c_addr   = BOARD_MLX90614_I2C_ADDR(3),
        .i2c_freq   = MLX90614_FREQ_MAX,
    },
    { 
        .flags      = MLX90614_FLAGS_TOBJ1,
        .i2c_addr   = BOARD_MLX90614_I2C_ADDR(4),
        .i2c_freq   = MLX90614_FREQ_MAX,
    },
    { 
        .flags      = MLX90614_FLAGS_TOBJ1,
        .i2c_addr   = BOARD_MLX90614_I2C_ADDR(5),
        .i2c_freq   = MLX90614_FREQ_MAX,
    }
};


/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_bmp180initialize
 *
 * Description:
 *   Initialize and register the MPL115A Pressure Sensor driver.
 *
 * Input parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/press0"
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ************************************************************************************/

int stm32_initialize_mlx90614(void)
{
    FAR struct i2c_master_s *i2c;
    int ret = 0;
    int i;
    char *path;

    /* Initialize I2C */

    i2c = stm32_i2cbus_initialize(GPIO_MLX90614_I2C);

    if (!i2c)
    {
        return -ENODEV;
    }

    path = malloc(PATH_MAX);
    if ( path == NULL )
    {
        snerr("ERROR: Malloc error registering MLX90614\n");
    }

    /* Then register the barometer sensor */

    i = BOARD_MLX90614_NBR;
    while(i--)
    {
        snprintf(path,PATH_MAX,BOARD_MLX90614_PATH"%d",i);
        path[PATH_MAX-1] = '\0'; /* to be sure */
        ret = mlx90614_register(path, i2c, &cfg[i]);
        if (ret < 0)
        {
            snerr("ERROR: Error registering MLX90614\n");
        }
    }

    free(path);

    return ret;
}

#endif /* CONFIG_I2C && CONFIG_MPL115A && CONFIG_STM32_I2C1 */
