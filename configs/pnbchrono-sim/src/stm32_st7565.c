/**************************************************************************************
 * configs/pnbchrono-v2/src/stm32_st7565.c
 *
 * This logic supports the connection of an ST7565-based LCD to the pnbchrono-v2
 * board.
 *
 *   Copyright (C) 2012 Gregory Nutt. All rights reserved.
 *   Authors: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 **************************************************************************************/

/**************************************************************************************
 * Included Files
 **************************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <debug.h>
#include <fcntl.h>

#include <nuttx/arch.h>
#include <nuttx/board.h>
#include <nuttx/spi/spi.h>
#include <nuttx/drivers/pwm.h>
#include <nuttx/lcd/lcd.h>
#include <nuttx/lcd/st7565.h>

#include <arch/board/board.h>

#include "up_arch.h"
#include "stm32.h"
#include "pnbchrono-v2.h"

/**************************************************************************************
 * Pre-processor Definitions
 **************************************************************************************/
/* Configuration **********************************************************************/

#ifndef CONFIG_STM32_FSMC
#  error "CONFIG_STM32_FSMC is required to use the LCD"
#endif

/* Define CONFIG_DEBUG_LCD to enable detailed LCD debug output. Verbose debug must
 * also be enabled.
 */

#ifndef CONFIG_DEBUG
#  undef CONFIG_DEBUG_VERBOSE
#  undef CONFIG_DEBUG_GRAPHICS
#  undef CONFIG_DEBUG_LCD
#endif

#ifndef CONFIG_DEBUG_VERBOSE
#  undef CONFIG_DEBUG_LCD
#endif

/* pnbchrono-v2 LCD Hardware Definitions ******************************************/
/* LCD /CS is CE1 ==  NOR/SRAM Bank 1
 *
 * Bank 1 = 0x60000000 | 0x00000000
 * Bank 2 = 0x60000000 | 0x04000000
 * Bank 3 = 0x60000000 | 0x08000000
 * Bank 4 = 0x60000000 | 0x0c000000
 *
 * FSMC address bit 8 is used to distinguish command and data.  FSMC address bits
 * 0-24 correspond to ARM address bits 1-25.
 */

#ifndef LCD_ADDRESS_IDX
#   error "LCD_ADDRESS_IDX not defined ! (if you use A23 set LCD_ADDRESS_IDX = 23)"
#endif
#ifndef GPIO_LCD_A0
#   error "GPIO_LCD_A0 not defined ! (if you use A23 set GPIO_FSMC_A23)"
#endif

#define STM32_LCDBASE       ((uintptr_t)(0x60000000 | 0x00000000))
#define LCD_CMD_ADDR        (STM32_LCDBASE)
#define LCD_DATA_ADDR       (STM32_LCDBASE + (1<<(LCD_ADDRESS_IDX))) 
#define IDX                 #LCD_ADDRESS_IDX

/* SRAM pin definitions */

#define LCD_NDATALINES   8  /* D0-7 */

#if defined(GPIO_LCD_PWM_DEV) && (!defined(CONFIG_PWM))
#   error "GPIO_LCD_PWM_DEV need CONFIG_PWM"
#endif


/* Debug ******************************************************************************/


/**************************************************************************************
 * Private Type Definition
 **************************************************************************************/

/**************************************************************************************
 * Private Function Protototypes
 **************************************************************************************/
/* Low Level LCD access */

static void stm32_reset     (FAR struct st7565_lcd_s *lcd, bool on);
static void stm32_select    (FAR struct st7565_lcd_s *lcd);
static void stm32_deselect  (FAR struct st7565_lcd_s *lcd);
static void stm32_cmddata   (FAR struct st7565_lcd_s *lcd, const uint8_t cmd);
static int  stm32_senddata  (FAR struct st7565_lcd_s *lcd, const uint8_t *data, int size);
static int  stm32_backlight (FAR struct st7565_lcd_s *lcd, int level);

/**************************************************************************************
 * Private Data
 **************************************************************************************/
/* LCD pin mapping (see configs/stm324discovery/README.txt
 * MAPPING TO STM32 F4:
 *
 *  ---------------- ------------- ----------------------------------
 *   STM32 FUNCTION  LCD PIN       pnbchrono-v2 PIN
 *  ---------------- ------------- ----------------------------------
 *   FSMC_NE1         ~CS   pin 1   PD7  P2 pin 27 Free I/O
 *   PD3              RESET pin 2   PD3  P2 pin 47 Free I/O
 *   FSMC_A23         A0    pin 3   PE2  P1 pin 27 Free I/O
 *   FSMC_NWE         ~WR   pin 4   PD5  P2 pin 29 Conflict (Note 3)
 *   FSMC_NOE         ~RD   pin 5   PD4  P2 pin 32 Conflict (Note 4)
 *   FSMC_D0          D0    pin 6   PD14 P1 pin 46 Conflict (Note 1)
 *   FSMC_D1          D1    pin 7   PD15 P1 pin 47 Conflict (Note 2)
 *   FSMC_D2          D2    pin 8   PD0  P2 pin 36 Free I/O
 *   FSMC_D3          D3    pin 9   PD1  P2 pin 33 Free I/O
 *   FSMC_D4          D4    pin 10  PE7  P1 pin 25 Free I/O
 *   FSMC_D5          D5    pin 11  PE8  P1 pin 26 Free I/O
 *   FSMC_D6          D6    pin 12  PE9  P1 pin 27 Free I/O
 *   FSMC_D7          D7    pin 13  PE10 P1 pin 28 Free I/O
 *  ---------------- ------------- ----------------------------------
 *
 *   1 Used for the RED LED
 *   2 Used for the BLUE LED
 *   3 Used for the RED LED and for OTG FS Overcurrent.  It may be okay to use
 *     for the parallel interface if PC0 is held high (or floating).  PC0 enables
 *     the STMPS2141STR IC power switch that drives the OTG FS host VBUS.
 *   4 Also the reset pin for the CS43L22 audio Codec.
 */

/* GPIO configurations unique to the LCD  */

static const uint32_t g_lcdconfig[] =
{

  GPIO_LCD_RESET,   /* LCD_RESET */
  GPIO_LCD_A0,      /* LCD_A0 (FSMC_Axx) */
  GPIO_FSMC_NOE,    /* FSMC_NOE */
  GPIO_FSMC_NWE,    /* FSMC_NWE */
  GPIO_FSMC_NE1     /* FSMC_NE1 */
};
#define NLCD_CONFIG (sizeof(g_lcdconfig)/sizeof(uint32_t))

/* This is the driver state structure (there is no retained state information) */

struct st7565_lcd_s stm32_st7565_lcd =
{
    .reset      = &stm32_reset,
    .select     = &stm32_select,
    .deselect   = &stm32_deselect, 
    .cmddata    = &stm32_cmddata,  
    .senddata   = &stm32_senddata, 
    .backlight  = &stm32_backlight
};

/* The saved instance of the LCD driver */

static FAR struct lcd_dev_s *g_lcd;
static uint8_t *write_address = NULL;
#ifdef GPIO_LCD_PWM_DEV
static int lcd_pwm_dev = -1;
#endif

/**************************************************************************************
 * Private Functions
 **************************************************************************************/

/**************************************************************************************
 * Name:  stm32_reset
 *
 * Description:
 *   Enable/Disable reset pinn of LCD the device.
 *
 *   lcd - A reference to the lcd interface specific structure
 *
 **************************************************************************************/

static void stm32_reset (FAR struct st7565_lcd_s *lcd, bool on)
{
    UNUSED(lcd);
    stm32_gpiowrite(GPIO_LCD_RESET,!on);
}

/**************************************************************************************
 * Name: stm32_select
 *
 * Description:
 *   Select the LCD device
 *
 **************************************************************************************/

static void stm32_select(FAR struct st7565_lcd_s *dev)
{
  /* Does not apply to this hardware */
}

/**************************************************************************************
 * Name: stm32_deselect
 *
 * Description:
 *   De-select the LCD device
 *
 **************************************************************************************/

static void stm32_deselect(FAR struct st7565_lcd_s *dev)
{
  /* Does not apply to this hardware */
}


/**************************************************************************************
 * Name:  st7565_cmddata
 *
 * Description:
 *   Select command (A0 = 0) or data (A0 = 1) mode.
 *
 *   lcd - A reference to the lcd interface specific structure
 *   cmd    - If true command mode will be seleted.
 *
 **************************************************************************************/

static void stm32_cmddata   (FAR struct st7565_lcd_s *lcd, const uint8_t cmd)
{
    //lcdinfo("set command %d\n",cmd);
    if (cmd)
        write_address = (uint8_t*)LCD_CMD_ADDR;
    else
        write_address = (uint8_t*)LCD_DATA_ADDR;
}

/**************************************************************************************
 * Name:  st7565_send_data_buf
 *
 * Description:
 *   Send a data buffer to the LCD driver (A0 = 1).
 *
 *   lcd - A reference to the lcd interface specific structure
 *   buf    - buffer sent as data to LCD driver.
 *   size   - size of buffer in bytes.
 *
 **************************************************************************************/

static int  stm32_senddata  (FAR struct st7565_lcd_s *lcd, const uint8_t *data, int size)
{

    UNUSED(lcd);

    while(size--)
    {
        //lcdinfo("write 0x%02x\n",*data);
        *write_address = (*data++);
    }

    return OK;

}

/**************************************************************************************
 * Name:  stm32_backlight
 *
 * Description:
 *   Change backlight level of display.
 *
 *   lcd - A reference to the lcd interface specific structure
 *   level  - set backlight pwm from 0 CONFIG_LCD_MAXPOWER-1.
 *
 **************************************************************************************/

static int  stm32_backlight (FAR struct st7565_lcd_s *lcd, int level)
{
    int ret = OK;
    UNUSED(lcd);

#ifdef GPIO_LCD_PWM_PIN
    stm32_gpiowrite(GPIO_LCD_PWM_PIN,level);
#endif

#ifdef GPIO_LCD_PWM_DEV
    if ( lcd_pwm_dev < 0 )
    {
        lcd_pwm_dev = open(GPIO_LCD_PWM_DEV, O_RDONLY);
        if (lcd_pwm_dev < 0)
        {
            lcderr("LCD pwm open failed errno: %d\n");
        }
    }
    if ( lcd_pwm_dev < 0 )
    {
        ret = -1;
    }
    else
    {
        struct pwm_info_s info;

        memset(&info,0,sizeof(struct pwm_info_s));

        /* Configure the characteristics of the pulse train */

        /* Frequency of the pulse train */
        info.frequency = 100; /* 100 Hz */
        /* Duty of the pulse train, "1"-to-"0" duration.
         * Maximum: 65535/65536 (0x0000ffff)
         * Minimum:     1/65536 (0x00000001) */
#ifdef CONFIG_PWM_MULTICHAN
        info.channels[0].duty       = (((uint32_t)level)*65535) / 100;
        info.channels[0].channel    = GPIO_LCD_PWM_CHANNEL;
#else
        info.duty      = (((uint32_t)level)*65535) / 100;

        /* The number of pulse to generate.  0 means to
         * generate an indefinite number of pulses  
         * So done by memset before 
         */
#endif

        ret = ioctl(lcd_pwm_dev, 
                    PWMIOC_SETCHARACTERISTICS, 
                    (unsigned long)&info
                   );
        if (ret < 0)
        {
            lcderr("pwm_main: ioctl(PWMIOC_SETCHARACTERISTICS)"
                 " failed errno: %d\n",
                 errno
                );
        }

        /* Then start the pulse train.  Since the driver was opened in blocking
         * mode, this call will block if the count value is greater than zero.
         */

        if ( ret >= 0 )
        {
            ret = ioctl(lcd_pwm_dev, PWMIOC_START, 0);
            if (ret < 0)
            {
                lcderr("pwm_main: ioctl(PWMIOC_START) failed: %d\n", errno);
            }
        }
    }

#endif

    return ret;

}

/************************************************************************************
 * Name: stm32_selectlcd
 *
 * Description:
 *   Initialize to the LCD
 *
 ************************************************************************************/

void stm32_selectlcd(void)
{
  /* Configure GPIO pins */

  stm32_extmemdata(LCD_NDATALINES);             /* Common data lines: D0-D15 */
  stm32_extmemgpios(g_lcdconfig, NLCD_CONFIG);  /* LCD-specific control lines */

  /* Enable AHB clocking to the FSMC */

  stm32_enablefsmc();

  /* Color LCD configuration (LCD configured as follow):
   *
   *   - Data/Address MUX  = Disable   "FSMC_BCR_MUXEN" just not enable it.
   *   - Extended Mode     = Disable   "FSMC_BCR_EXTMOD"
   *   - Memory Type       = SRAM      "FSMC_BCR_SRAM"
   *   - Data Width        = 8bit      "FSMC_BCR_MWID8"
   *   - Write Operation   = Enable    "FSMC_BCR_WREN"
   *   - Asynchronous Wait = Disable
   */

  /* Bank1 NOR/SRAM control register configuration */

  putreg32(FSMC_BCR_SRAM | FSMC_BCR_MWID8 | FSMC_BCR_WREN, STM32_FSMC_BCR1);

  /* Bank1 NOR/SRAM timing register configuration */

  putreg32(FSMC_BTR_ADDSET(5) | FSMC_BTR_ADDHLD(0) | FSMC_BTR_DATAST(9) | FSMC_BTR_BUSTURN(0) |
           FSMC_BTR_CLKDIV(0) | FSMC_BTR_DATLAT(0) | FSMC_BTR_ACCMODA, STM32_FSMC_BTR1);

  putreg32(0xffffffff, STM32_FSMC_BWTR1);

  /* Enable the bank by setting the MBKEN bit */

  putreg32(FSMC_BCR_MBKEN | FSMC_BCR_SRAM | FSMC_BCR_MWID8 | FSMC_BCR_WREN, STM32_FSMC_BCR1);
}

/**************************************************************************************
 * Public Functions
 **************************************************************************************/

/**************************************************************************************
 * Name:  board_lcd_initialize
 *
 * Description:
 *   Initialize the LCD video hardware.  The initial state of the LCD is fully
 *   initialized, display memory cleared, and the LCD ready to use, but with the power
 *   setting at 0 (full off).
 *
 **************************************************************************************/

int board_lcd_initialize(void)
{
  /* Only initialize the driver once */

  if ( g_lcd != NULL )
  {
      lcderr("Already initialized\n");
      return OK;
  }

  lcdinfo("Initializing\n");

  /* Configure GPIO pins and configure the FSMC to support the LCD */

  stm32_selectlcd();

#ifdef GPIO_LCD_PWM_PIN
  stm32_configgpio(GPIO_LCD_PWM_PIN);
#endif
#ifdef GPIO_LCD_PWM_DEV

    lcd_pwm_dev = open(GPIO_LCD_PWM_DEV, O_RDONLY);
    if (lcd_pwm_dev < 0)
    {
        lcderr("LCD pwm open failed errno: %d\n");
    }
#endif

  /* Configure and enable the LCD */

  up_mdelay(50);

  g_lcd = st7565_initialize(&stm32_st7565_lcd,0);
  if (!g_lcd)
    {
      lcderr("ERROR: ssd1289_lcdinitialize failed\n");
      return -ENODEV;
    }

  /* Turn the display off */

  g_lcd->setpower(g_lcd, 0);
  return OK;
}

/**************************************************************************************
 * Name:  board_lcd_getdev
 *
 * Description:
 *   Return a a reference to the LCD object for the specified LCD.  This allows support
 *   for multiple LCD devices.
 *
 **************************************************************************************/

FAR struct lcd_dev_s *board_lcd_getdev(int lcddev)
{
  DEBUGASSERT(lcddev == 0);
  return g_lcd;
}

/**************************************************************************************
 * Name:  board_lcd_uninitialize
 *
 * Description:
 *   Unitialize the LCD support
 *
 **************************************************************************************/

void board_lcd_uninitialize(void)
{
  /* Turn the display off */

  g_lcd->setpower(g_lcd, 0);
}



