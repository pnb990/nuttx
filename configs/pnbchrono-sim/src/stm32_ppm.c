/****************************************************************************
 * configs/stm32f4-pnbppm_v2/src/stm32_ppm.c
 *
 *   Copyright (C) 2015 Pierre-noel Bouteville. All rights reserved.
 *   Author: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>

#include <nuttx/arch.h>
#include <nuttx/irq.h>

#include <arch/irq.h>
#include <arch/board/board.h>
#include "pnbchrono-v2.h"

#include <string.h>
#include <poll.h>
#include <errno.h>
#include <nuttx/kmalloc.h>

#include "up_arch.h"
#include "stm32_gpio.h"
#include "stm32_capture.h"

#ifdef CONFIG_STM32_PPM_DEBUG 
#   define ppminfo(fmt,...)   _info(fmt, ##__VA_ARGS__)
#else
#   define ppminfo(fmt,...)
#endif

#ifdef CONFIG_STM32_PPM_ERROR 
#   define ppmerr(fmt,...)    _err(fmt, ##__VA_ARGS__)
#else
#   define ppmerr(fmt,...)
#endif

#ifndef CONFIG_STM32_PPM_BUFSIZE
#  define CONFIG_STM32_PPM_BUFSIZE 64
#endif

#ifndef CONFIG_STM32_PPM_FILTER_DELAYS_S
#  define CONFIG_STM32_PPM_FILTER_DELAYS_S 1
#endif

/* only one timer per ppm and only channel 1 is now supported */
#define PPM_CH  1   
/* only 16 bit mode timer is now supported */
#define PPM_COUNTER_SIZE    (1ULL<<16)

static void stm32_ppm_new_event(struct stm32_ppm_s* dev, int32_t time_raw);
static int stm32_ppm_tim_isr_handler (struct stm32_ppm_s* dev);
static int stm32_ppm1_isr_handler (int irq, void *context);
static int stm32_ppm9_isr_handler (int irq, void *context);

/****************************************************************************
 * Name: struct stm32_ppm_cfg_s
 * Description:
 *  
 ****************************************************************************/
struct stm32_ppm_cfg_s
{
    unsigned long       src_freq;
    stm32_cap_ch_cfg_t  ch_cfg;
    xcpt_t              isr_func;
};


/****************************************************************************
 * Name: struct stm32_ppm_s
 * Description:
 *  variable of keypad
 ****************************************************************************/
struct stm32_ppm_s
{
    const struct stm32_ppm_cfg_s *cfg;
    int     freq;           /* current clock frequency */
    int missing_event_nbr;  /* number of event missed due to buffer or capture 
                             * overflow 
                             */
    FAR struct stm32_cap_dev_s *capture;    /* timer used */

    int32_t last_pulse_time;    /* time of last loop trigger */

    int32_t last_diff;           /* time of last loop trigger */

};

/****************************************************************************
 * Name: stm32_ppm
 * Description:
 *  variable of keypad instance.
 ****************************************************************************/

struct stm32_ppm_s stm32_ppm1;
const struct stm32_ppm_cfg_s stm32_ppm1_cfg = 
{
    .src_freq   = STM32_APB2_TIM1_CLKIN,
    .ch_cfg     = GPIO_PPM1_CH1CFG,
    .isr_func   = stm32_ppm1_isr_handler
};

struct stm32_ppm_s stm32_ppm9;
const struct stm32_ppm_cfg_s stm32_ppm9_cfg = 
{
    .src_freq   = STM32_APB2_TIM9_CLKIN,
    .ch_cfg     = GPIO_PPM9_CH1CFG,
    .isr_func   = stm32_ppm9_isr_handler
};

/****************************************************************************
 * Name: stm32_ppm_new_event
 ****************************************************************************/

static void stm32_ppm_new_event(struct stm32_ppm_s* dev, int32_t time_raw)
{
    int32_t diff;

    ASSERT(dev != NULL);

    /* get time between 2 pulse */
    diff = time_raw - dev->last_pulse_time;

//    ppminfo("rpm time_raw %d last %d diff %d\n",time_raw, dev->last_pulse_time,
//            diff);

    dev->last_diff = diff;
}




/****************************************************************************
 * irq handler
 ****************************************************************************/

static int stm32_ppm_tim_isr_handler (struct stm32_ppm_s* dev)
{
    int32_t val;
    int32_t cnt;
    stm32_cap_flags_t flags;

    ASSERT(dev);

    flags = STM32_CAP_GETFLAGS(dev->capture);
    cnt   = STM32_CAP_GETCAPTURE(dev->capture,STM32_CAP_CHANNEL_COUNTER);
    //ppminfo("Irq flags 0x%08X\n",flags);

    if (flags & STM32_CAP_FLAG_IRQ_CH(PPM_CH))
    {
        if (flags & STM32_CAP_FLAG_OF_CH(PPM_CH))
            dev->missing_event_nbr++;

        val = STM32_CAP_GETCAPTURE(dev->capture,PPM_CH);

        if ( (flags & STM32_CAP_FLAG_IRQ_COUNTER) && ( val <= cnt ) )
            val -= PPM_COUNTER_SIZE;

        stm32_ppm_new_event(dev,val);
        dev->last_pulse_time = val;
    }

    if (flags & STM32_CAP_FLAG_IRQ_COUNTER)
    {   
        dev->last_pulse_time -= PPM_COUNTER_SIZE;
        
        /* limits to about 1Hz */

        if ( dev->last_pulse_time > dev->freq ) 
            stm32_ppm_new_event(dev,0);

    }

    STM32_CAP_ACKFLAGS(dev->capture,flags);

    return 0;
}

static int stm32_ppm1_isr_handler (int irq, void *context)
{ 
    UNUSED(irq);
    UNUSED(context);
    return stm32_ppm_tim_isr_handler( &stm32_ppm1 );
}

static int stm32_ppm9_isr_handler (int irq, void *context)
{ 
    UNUSED(irq);
    UNUSED(context);
    return stm32_ppm_tim_isr_handler( &stm32_ppm9 );
}



/****************************************************************************
 * Name: stm32_ppm_init
 *
 * Description:
 *  Initialize All GPIO for key pad.
 * Input parameters:
 *  _key_map    - first key mapping of mapping GPIO<=>Key list.
 *                    to Finish list set Pin with negative value.
 * Returned Value:
 *   None (User allocated instance initialized).
 ****************************************************************************/
struct stm32_ppm_s* stm32_ppm_init( int ppm_id ,unsigned int freq)
{
    irqstate_t irqstate;
    struct stm32_ppm_s *dev;

    /* Disable interrupts until we are done.  This guarantees that the
     * following operations are atomic.
     */
    switch (ppm_id)
    {
        case 1:
            dev = &stm32_ppm1;
            dev->cfg = &stm32_ppm1_cfg;
            break;
        case 9:
            dev = &stm32_ppm9;
            dev->cfg = &stm32_ppm9_cfg;
            break;
        default:
            return NULL;
    }

    irqstate = up_irq_save();

    ASSERT(dev->capture == NULL);
    dev->capture = stm32_cap_init(ppm_id);

    if (dev->capture)
    {
        int ppm_flags;
        dev->missing_event_nbr = 0;

        STM32_CAP_SETCHANNEL(dev->capture, PPM_CH, dev->cfg->ch_cfg);

        STM32_CAP_SETISR(dev->capture, dev->cfg->isr_func);

        ppm_flags = STM32_CAP_FLAG_IRQ_COUNTER;
        ppm_flags |=STM32_CAP_FLAG_IRQ_CH(PPM_CH);

        STM32_CAP_ACKFLAGS(dev->capture,ppm_flags);
        STM32_CAP_ENABLEINT(dev->capture,ppm_flags,true);
    }

    up_irq_restore(irqstate);

    if (dev->capture)
        stm32_ppm_setfreq(dev,freq);

    return dev;
}

float stm32_ppm_get_last( struct stm32_ppm_s* dev )
{
    float ppm_val;
    int32_t diff = dev->last_diff; 

    if ( diff == 0 )
    {
        ppm_val = 0;
    }
    else
    {
        ppm_val = dev->freq;
        ppm_val /= diff;
    }

    ppminfo("PPM diff %d => %d Hz\n",diff, (unsigned int)ppm_val);

    return ppm_val;
}

int stm32_ppm_setfreq(struct stm32_ppm_s* dev , int freq)
{
    int32_t prescaler;
    unsigned long src_freq = dev->cfg->src_freq;
    irqstate_t irqstate;

    ASSERT(dev);
    ASSERT(freq);

    irqstate = up_irq_save();


    prescaler = STM32_CAP_SETCLOCK(dev->capture,  
                       STM32_CAP_CLK_INT,
                       src_freq/freq,
                       UINT32_MAX
                      );


    if ( prescaler < 0 )
    {
        ppmerr("Cannot set This frequency %d\n",freq);
        dev->freq = 0;
    }
    else
    {
        dev->freq = src_freq/(prescaler+1);
    }

    up_irq_restore(irqstate);

    ppminfo("ppm: set frequency %d\n",dev->freq);

    return dev->freq;
}


