/************************************************************************************
 * configs/pnbchrono-v2/src/stm32_pwm.c
 *
 *   Copyright (C) 2011 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>

#include <nuttx/drivers/pwm.h>
#include <arch/board/board.h>

#include "chip.h"
#include "up_arch.h"
#include "stm32_pwm.h"
#include "pnbchrono-v2.h"

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
/* Configuration *******************************************************************/

/************************************************************************************
 * Private Functions
 ************************************************************************************/

static int pwm_setup(const char *dev_path,int timer_nbr)
{
    int ret;
    struct pwm_lowerhalf_s *pwm;

    /* Call stm32_pwminitialize() to get an instance of the PWM interface */

    pwm = stm32_pwminitialize(timer_nbr);
    if (!pwm)
    {
        pwmerr("Failed to get the STM32 PWM lower half\n");
        return -ENODEV;
    }

    /* Register the PWM driver at GPIO_LCD_PWM_DEV */

    ret = pwm_register(dev_path, pwm);

    if (ret < 0)
    {
        pwmerr("pwm_register failed: %d\n", ret);
        return ret;
    }
    return 0;
}

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: board_pwm_setup
 *
 * Description:
 *   All STM32 architectures must provide the following interface to work with
 *   examples/pwm.
 *
 ************************************************************************************/

int stm32_pwm_setup(void)
{
    static bool initialized = false;
    int ret;

    /* Have we already initialized? */

    if (!initialized)
    {

        if ( pwm_setup(GPIO_LCD_PWM_DEV,GPIO_LCD_PWM_TIMER) < 0 )
            ret = -1;

        if ( pwm_setup(GPIO_LED_PWM_DEV,GPIO_LED_PWM_TIMER) < 0 )
            ret = -1;

        /* Now we are initialized */

        initialized = true;
    }

    return ret;
}

