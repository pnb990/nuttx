
/******************************************************************************\
 * Includes
 \******************************************************************************/

#include <nuttx/config.h>
#include <nuttx/clock.h>

#include <semaphore.h>
#include <debug.h>

#include <arch/board/board.h>
#include <nuttx/irq.h>
#include <nuttx/arch.h>
#include <arch/board/can_non_posix.h>

#include "up_internal.h"
#include "up_arch.h"

#include "chip.h"
#include "stm32.h"
#include <nuttx/kmalloc.h>

#include "pnbchrono-v2.h"

/******************************************************************************\
 * PRIVATE Macro
 \******************************************************************************/

#if (CONFIG_CAN_NON_POSIX_CAN_USED == 1)
#   define CAN_BASE STM32_CAN1_BASE

#   define GPIO_CAN_RX              GPIO_CAN1_RX
#   define GPIO_CAN_TX              GPIO_CAN1_TX

#   define RCC_APB1ENR_CANEN        RCC_APB1ENR_CAN1EN
#   define RCC_APB1RSTR_CANRST      RCC_APB1RSTR_CAN1RST

#   define STM32_IRQ_CANRX0         STM32_IRQ_CAN1RX0
#   define STM32_IRQ_CANRX1         STM32_IRQ_CAN1RX1
#   define STM32_IRQ_CANTX          STM32_IRQ_CAN1TX

/* All filter for CAN1 */
#   define STM32_CAN_FMR_CAN2SB     CAN_FMR_CAN2SB_MASK

#elif (CONFIG_CAN_NON_POSIX_CAN_USED == 2)
#   define CAN_BASE STM32_CAN2_BASE

#   define GPIO_CAN_RX GPIO_CAN2_RX
#   define GPIO_CAN_TX GPIO_CAN2_TX

/* CAN2 clock enable.  NOTE: CAN2 needs CAN1 clock as well. */
#   define RCC_APB1ENR_CANEN        (RCC_APB1ENR_CAN1EN | RCC_APB1ENR_CAN2EN)
#   define RCC_APB1RSTR_CANRST      RCC_APB1RSTR_CAN2RST

#   define STM32_IRQ_CANRX0         STM32_IRQ_CAN2RX0
#   define STM32_IRQ_CANRX1         STM32_IRQ_CAN2RX1
#   define STM32_IRQ_CANTX          STM32_IRQ_CAN2TX

/* All filter for CAN 2 */
#   define STM32_CAN_FMR_CAN2SB     0

#else
#   error "Not implemented"
#endif

#define canPRESCALER(pclk,baudrate,sjw_tq,bs1_tq,bs2_tq) \
    (\
     (STM32_PCLK1_FREQUENCY)\
     /   \
     ( ( (sjw_tq)+(bs1_tq)+(bs2_tq) ) * ( baudrate ) ) \
     )

#define canFRX_EXTENDED(ext_id,rtr) \
    ( (((ext_id)&((1<<29)-1))<<3) | (1<<2) | ((rtr)?(1<<1):0) )

#define canFRX_STANDARD(std_id,rtr) \
    ( ((std_id)<<21) | ((rtr)?(1<<1):0) )

#define canTIMEOUT_VALUE_MS 100
#define canFIFO_SIZE_RX_DEFAULT CONFIG_CAN_NON_POSIX_RX_FIFO_SIZE

/******************************************************************************\
 * PRIVATE type definitions
 \******************************************************************************/

typedef struct
{
    volatile unsigned int r_idx;
    volatile unsigned int w_idx;
    unsigned int    size;
    sem_t rd_sem;
    can_np_msg_t    *buf;
}can_fifo_rx_t;

typedef struct
{
    sem_t   tx_sem;
    int     tsr;
}can_mailbox_t;


/******************************************************************************\
 * PRIVATE function prototypes
 \******************************************************************************/

static inline uint32_t  stm32_can_getreg(int offset);
static inline void      stm32_can_putreg(int offset, uint32_t value);
static inline void      stm32_can_clrbits(int offset, uint32_t bits);
static inline void      stm32_can_setbits(int offset, uint32_t bits);
static inline void      stm32_can_modifyreg(int offset, uint32_t mask,
                                            uint32_t regval);

/******************************************************************************\
 * PRIVATE constant data definitions
 \******************************************************************************/

/******************************************************************************\
 * PRIVATE variable definitions
 \******************************************************************************/

static can_fifo_rx_t stm32_can_fifo_rx[can_npFIFO_NBR];
static can_mailbox_t stm32_can_mailbox[can_npMAILBOX_NBR];

/******************************************************************************\
 * PRIVATE function
 \******************************************************************************/

static inline uint32_t stm32_can_getreg(int offset)
{
    return getreg32(CAN_BASE + offset);
}

static inline void stm32_can_putreg(int offset, uint32_t value)
{
    putreg32(value, CAN_BASE + offset);
}

static inline void stm32_can_modifyreg(int offset, uint32_t mask, uint32_t regval)
{
    modifyreg32(CAN_BASE+offset,mask,regval);
}

static inline void      stm32_can_clrbits(int offset, uint32_t bits)
{
    modifyreg32(CAN_BASE+offset,bits,0);
}

static inline void      stm32_can_setbits(int offset, uint32_t bits)
{
    modifyreg32(CAN_BASE+offset,0,bits);
}

static inline int stm32_can_takesem(FAR sem_t *sem, struct timespec* tp)
{
    while (sem_timedwait(sem,tp) != OK)
    {
        if (errno == ETIMEDOUT)
            return -1;
    }
    return 0;
}

#define stm32_can_givesem(sem) (void)sem_post(sem)


/******************************************************************************
 * stm32_can_write_mailbox
 *
 * Description
 *  Write message msg in mail box of mailbox_id
 *
 * Params
 *  - mailbox_id index of mailbox
 *  - msg message to write
 *
 ******************************************************************************/
static inline int stm32_can_write_mailbox(int mailbox_id, can_np_msg_t* msg)
{
    uint32_t regval;

    regval = stm32_can_getreg(STM32_CAN_TSR_OFFSET);
    if ( ( regval & (CAN_TSR_TME0<<mailbox_id) ) == 0 )
        return -1;

    /* Set up the DLC */
    stm32_can_modifyreg(STM32_CAN_TDTR_OFFSET(mailbox_id), CAN_TDTR_DLC_MASK,
                        (msg->size << CAN_TDTR_DLC_SHIFT));

    /* Set up the data field */
    regval = (((uint32_t)msg->data[0] << CAN_TDLR_DATA0_SHIFT) |
              ((uint32_t)msg->data[1] << CAN_TDLR_DATA1_SHIFT) |
              ((uint32_t)msg->data[2] << CAN_TDLR_DATA2_SHIFT) |
              ((uint32_t)msg->data[3] << CAN_TDLR_DATA3_SHIFT)
             );
    stm32_can_putreg(STM32_CAN_TDLR_OFFSET(mailbox_id),regval);

    regval = (((uint32_t)msg->data[4] << CAN_TDHR_DATA4_SHIFT) |
              ((uint32_t)msg->data[5] << CAN_TDHR_DATA5_SHIFT) |
              ((uint32_t)msg->data[6] << CAN_TDHR_DATA6_SHIFT) |
              ((uint32_t)msg->data[7] << CAN_TDHR_DATA7_SHIFT)
             );
    stm32_can_putreg(STM32_CAN_TDHR_OFFSET(mailbox_id),regval);

    assert(msg->flags & can_npFLAGS_EXT_ID_MASK);

    /* Set up the Id and Request transmission */
    regval = CAN_TIR_TXRQ                                           | \
             CAN_TIR_IDE                                            | \
             ((msg->flags & can_npFLAGS_RTR_ID_MASK)?CAN_TIR_RTR:0) | \
             ((msg->id) << CAN_TIR_EXID_SHIFT                     ) ;
    stm32_can_putreg(STM32_CAN_TIR_OFFSET(mailbox_id), regval);

    return 0;
}

/******************************************************************************
 * stm32_can_read_fifo
 *
 * Description
 *  Write message msg in mail box of mailbox_id
 *
 * Params
 *  - fifo_id index of fifo
 *  - msg message to put data
 *
 ******************************************************************************/
static inline void stm32_can_read_fifo(int fifo_id, can_np_msg_t* msg)
{
    uint32_t regval;

    /* Get the CAN ID */
    regval = stm32_can_getreg(STM32_CAN_RIR_OFFSET(fifo_id));
    msg->id = (regval & CAN_RIR_EXID_MASK) >> CAN_RIR_EXID_SHIFT;

    if ((regval & CAN_RIR_IDE) != 0)
    {
        msg->id    = (regval & CAN_RIR_EXID_MASK) >> CAN_RIR_EXID_SHIFT;
        msg->flags = can_npFLAGS_EXT_ID_MASK;
    }
    else
    {
        msg->id    = (regval & CAN_RIR_STID_MASK) >> CAN_RIR_STID_SHIFT;
        msg->flags = 0;
    }

    /* Get the RTR bit */

    if ( regval & CAN_RIR_RTR  )
    {
        msg->flags |= can_npFLAGS_RTR_ID_MASK;
    }


    /* Get the DLC */
    regval = stm32_can_getreg(STM32_CAN_RDTR_OFFSET(fifo_id));
    msg->size = (regval & CAN_RDTR_DLC_MASK) >> CAN_RDTR_DLC_SHIFT;

    /* Get the FMI don't care */
    //msg->filter = (regval & CAN_RDT0R_FMI) >> CAN_RDT0R_FMI_Pos;

    /* Get the data field */
    regval = stm32_can_getreg(STM32_CAN_RDLR_OFFSET(fifo_id));
    msg->data[0] = ( regval & CAN_RDLR_DATA0_MASK) >> CAN_RDLR_DATA0_SHIFT;
    msg->data[1] = ( regval & CAN_RDLR_DATA1_MASK) >> CAN_RDLR_DATA1_SHIFT;
    msg->data[2] = ( regval & CAN_RDLR_DATA2_MASK) >> CAN_RDLR_DATA2_SHIFT;
    msg->data[3] = ( regval & CAN_RDLR_DATA3_MASK) >> CAN_RDLR_DATA3_SHIFT;
    regval = stm32_can_getreg(STM32_CAN_RDHR_OFFSET(fifo_id));
    msg->data[4] = ( regval & CAN_RDHR_DATA4_MASK) >> CAN_RDHR_DATA4_SHIFT;
    msg->data[5] = ( regval & CAN_RDHR_DATA5_MASK) >> CAN_RDHR_DATA5_SHIFT;
    msg->data[6] = ( regval & CAN_RDHR_DATA6_MASK) >> CAN_RDHR_DATA6_SHIFT;
    msg->data[7] = ( regval & CAN_RDHR_DATA7_MASK) >> CAN_RDHR_DATA7_SHIFT;
}

/******************************************************************************
 * stm32_can_interrupt_tx
 *
 * Description
 *  Handles CAN TX interrupt request
 ******************************************************************************/
static int stm32_can_interrupt_tx(int irq, FAR void *context, FAR void *arg)
{

    uint32_t tsr;

    UNUSED(context);
    UNUSED(arg);

    board_autoled_on(LED_CAN_IT_TX);

    /* Check End of transmission flag */
    if ( ( stm32_can_getreg(STM32_CAN_IER_OFFSET) & CAN_IER_TMEIE ) == 0 )
    {
        board_autoled_off(LED_CAN_IT_TX);
        return 0;
    }

    tsr = stm32_can_getreg(STM32_CAN_TSR_OFFSET);

    /* Check Transmit request completion status */
    /* mailbox 0 */
#if (can_npMAILBOX_NBR >= 1)
    if ( (tsr&(CAN_TSR_TME0|CAN_TSR_RQCP0)) == (CAN_TSR_TME0|CAN_TSR_RQCP0) )
    { /* Mail box Empty */
        stm32_can_mailbox[0].tsr = tsr;
        sem_post(&stm32_can_mailbox[0].tx_sem);
    }
#endif

    /* mailbox 1 */
#if (can_npMAILBOX_NBR >= 2)
    if ( (tsr&(CAN_TSR_TME1|CAN_TSR_RQCP1)) == (CAN_TSR_TME1|CAN_TSR_RQCP1) )
    { /* Mail box Empty */
        stm32_can_mailbox[1].tsr = tsr>>8;
        sem_post(&stm32_can_mailbox[1].tx_sem);
    }
#endif

    /* mailbox 2 */
#if (can_npMAILBOX_NBR >= 3)
    if ( (tsr&(CAN_TSR_TME2|CAN_TSR_RQCP2)) == (CAN_TSR_TME2|CAN_TSR_RQCP2) )
    { /* Mail box Empty */
        stm32_can_mailbox[2].tsr = tsr>>16;
        sem_post(&stm32_can_mailbox[2].tx_sem);
    }
#endif

    /* Clear transmission status flags RQCPx */
    tsr &= (CAN_TSR_RQCP0  | \
            CAN_TSR_RQCP1  | \
            CAN_TSR_RQCP2    \
           );
    stm32_can_putreg(STM32_CAN_TSR_OFFSET,tsr);

    board_autoled_off(LED_CAN_IT_TX);
    return 0;
}

/******************************************************************************
 * stm32_can_interrupt_rx
 *
 * Description
 *  Handles CAN RX interrupt request
 ******************************************************************************/
static int stm32_can_interrupt_rx(int irq, FAR void *context, FAR void *arg)
{
    int w_idx;
    unsigned int next;
    uint32_t regval;
    int fifo_id;
    can_fifo_rx_t* fifo;
    struct timespec tp;

    board_autoled_on(LED_CAN_IT_RX);

    stm32_chrono_get_time(&tp);

    fifo_id = (int)arg;

    regval = stm32_can_getreg(STM32_CAN_RFR_OFFSET(fifo_id));

    if ( ( regval & CAN_RFR_FMP_MASK ) == 0 )
    {
        board_autoled_off(LED_CAN_IT_RX);
        return 0;
    }

    if ( regval & CAN_RFR_FOVR )
    {
        board_autoled_on(LED_CAN_OVERFLOW);
        regval &= ~CAN_RFR_FOVR ;
        stm32_can_putreg(STM32_CAN_RFR_OFFSET(fifo_id),regval);
    }
    else
    {
        board_autoled_off(LED_CAN_OVERFLOW);
    }

    assert(fifo_id < can_npMAILBOX_NBR);

    fifo = &stm32_can_fifo_rx[fifo_id];

    w_idx = fifo->w_idx;

    next = w_idx + 1;

    if ( next >= fifo->size )
    {
        next -= fifo->size;
    }

    /* fifo full */
    if ( next == fifo->r_idx )
    {
        board_autoled_on(LED_FIFO_OVERFLOW);
        DEBUGASSERT(false);
    }
    else
    {
        board_autoled_off(LED_FIFO_OVERFLOW);
        stm32_can_read_fifo(fifo_id,&fifo->buf[w_idx]);
        fifo->buf[w_idx].tp = tp;
        stm32_can_givesem(&fifo->rd_sem);

        fifo->w_idx = next;
    }

    /* Release the FIFO */
    stm32_can_setbits(STM32_CAN_RFR_OFFSET(fifo_id),CAN_RFR_RFOM);

    board_autoled_off(LED_CAN_IT_RX);
    return 0;
}



/******************************************************************************\
 * PUBLIC function
 \******************************************************************************/

/**
 * @brief  Led initialisation.
 * @param  None
 * @return None
 */
int stm32_can_np_init(void)
{
    uint32_t regval;
    irqstate_t flags;
    int i;
    systime_t tick_end;

    i = can_npFIFO_NBR;
    while(i--)
    {
        can_fifo_rx_t* fifo;

        fifo = &stm32_can_fifo_rx[i];

        sem_init(&fifo->rd_sem, 0, 0);

        fifo->r_idx = 0;
        fifo->w_idx = 0;
        fifo->size = 0;

        fifo->buf = kmm_malloc(sizeof(can_np_msg_t)*canFIFO_SIZE_RX_DEFAULT);
        if ( fifo->buf != NULL )
        {
            fifo->size = canFIFO_SIZE_RX_DEFAULT;
        }
    }

    stm32_configgpio(GPIO_CAN_RX);
    stm32_configgpio(GPIO_CAN_TX);

    /* Disable interrupts momentarily to stop any ongoing CAN event processing
     * and to prevent any concurrent access to the AHB1RSTR register.
     */

    flags = enter_critical_section();

    /* Enable CAN clock */
    regval  = getreg32(STM32_RCC_APB1ENR);
    regval |= RCC_APB1ENR_CANEN;
    putreg32(regval, STM32_RCC_APB1ENR);
    /* Delay after an RCC peripheral clock enabling done by reset procedure */

    /* Reset the CAN */
    regval  = getreg32(STM32_RCC_APB1RSTR);
    regval |= RCC_APB1RSTR_CANRST;
    putreg32(regval, STM32_RCC_APB1RSTR);

    regval &= ~RCC_APB1RSTR_CANRST;
    putreg32(regval, STM32_RCC_APB1RSTR);

    leave_critical_section(flags);


    /* Exit from sleep mode and Request initialisation */
    stm32_can_modifyreg(STM32_CAN_MCR_OFFSET, CAN_MCR_SLEEP , CAN_MCR_INRQ);

    /* Get tick */
    tick_end = g_system_timer + MSEC2TICK(canTIMEOUT_VALUE_MS);

    /* Wait the acknowledge */
    while( ! ( stm32_can_getreg(STM32_CAN_MSR_OFFSET) & CAN_MSR_INAK) )
    {
        if( tick_end < g_system_timer )
        {
            return -1;
        }
    }

    stm32_can_clrbits(STM32_CAN_MCR_OFFSET,
                      CAN_MCR_TTCM |
                      CAN_MCR_ABOM |
                      CAN_MCR_AWUM |
                      CAN_MCR_NART |
                      CAN_MCR_RFLM |
                      CAN_MCR_TXFP
                     );

    /* Set the bit timing register */
    regval = stm32_can_getreg(STM32_CAN_BTR_OFFSET);
    regval &= ~(
                CAN_BTR_SILM  |
                CAN_BTR_LBKM  |
                CAN_BTR_SJW_MASK  |
                CAN_BTR_TS1_MASK  |
                CAN_BTR_TS2_MASK  |
                CAN_BTR_BRP_MASK
               );
    regval |= (
               /* No silent     */
               /* No loop back  */
               ((CONFIG_CAN_NON_POSIX_SJW   - 1) << CAN_BTR_SJW_SHIFT ) |
               ((CONFIG_CAN_NON_POSIX_TSEG1 - 1) << CAN_BTR_TS1_SHIFT ) |
               ((CONFIG_CAN_NON_POSIX_TSEG2 - 1) << CAN_BTR_TS2_SHIFT ) |
               ((canPRESCALER(STM32_PCLK1_FREQUENCY,
                              CONFIG_CAN_NON_POSIX_BAUD,
                              CONFIG_CAN_NON_POSIX_SJW,
                              CONFIG_CAN_NON_POSIX_TSEG1,
                              CONFIG_CAN_NON_POSIX_TSEG2
                             ) - 1) << CAN_BTR_BRP_SHIFT )
              );
    stm32_can_putreg(STM32_CAN_BTR_OFFSET,regval);

    /* enable Filter setting */
    regval = getreg32(STM32_CAN1_FMR);
    regval |= CAN_FMR_FINIT; 
    putreg32(regval, STM32_CAN1_FMR);

    /* set all Filter for used CAN */
    regval &= ~CAN_FMR_CAN2SB_MASK; 
    regval |= STM32_CAN_FMR_CAN2SB; 
    putreg32(regval, STM32_CAN1_FMR);

    /* disable all filter */
    modifyreg32(STM32_CAN1_FA1R, CAN_FA1R_FACT_MASK, 0);

    /* Disable Filter setting */
    regval &= ~CAN_FMR_FINIT; 
    putreg32(regval, STM32_CAN1_FMR);

    /* Request leave initialisation */
    stm32_can_clrbits(STM32_CAN_MCR_OFFSET, CAN_MCR_INRQ);

    /* Get tick */
    tick_end = g_system_timer + MSEC2TICK(canTIMEOUT_VALUE_MS);

    /* Wait the acknowledge */
    while( stm32_can_getreg(STM32_CAN_MSR_OFFSET) & CAN_MSR_INAK )
    {
        if( tick_end < g_system_timer )
        {
            return -1;
        }
    }


    irq_attach(STM32_IRQ_CANRX0, stm32_can_interrupt_rx, (void*)0);
    irq_attach(STM32_IRQ_CANRX1, stm32_can_interrupt_rx, (void*)1);
    irq_attach(STM32_IRQ_CANTX,  stm32_can_interrupt_tx, NULL);

    up_enable_irq(STM32_IRQ_CANRX0);
    up_enable_irq(STM32_IRQ_CANRX1);
    up_enable_irq(STM32_IRQ_CANTX);

    /* Enable interrupts: */
    /*  - Enable Transmit mailbox empty Interrupt */
    regval = CAN_IER_TMEIE  |
        CAN_IER_FMPIE0 |
        CAN_IER_FMPIE1 |
        CAN_IER_TMEIE  |
        /* don't manage error */
#if 0
        /*  - Enable Error warning Interrupt */
        CAN_IT_EWG |
        /*  - Enable Error passive Interrupt */
        CAN_IT_EPV |
        /*  - Enable Bus-off Interrupt */
        CAN_IT_BOF |
        /*  - Enable Last error code Interrupt */
        CAN_IT_LEC |
        /*  - Enable Error Interrupt */
        CAN_IT_ERR |
#endif
        0;
    stm32_can_setbits(STM32_CAN_IER_OFFSET, regval);

    return 0;
}

/**
 * @brief Change receive FIFO size.
 * @note all message in this FIFO will be deleted.
 * @param[in] fifo_id : FIFO id
 * @param[in] size    : new FIFO size.
 * @return 0 on success, -1 in case of error.
 */
int can_np_set_rx_fifo_size(int fifo_id,int size)
{
    int ret = 0;
    can_fifo_rx_t* fifo;
    irqstate_t irqstate;
    irqstate = up_irq_save();

    assert(fifo_id < can_npFIFO_NBR);

    fifo = &stm32_can_fifo_rx[fifo_id];

    fifo->r_idx = 0;
    fifo->w_idx = 0;
    fifo->size = size;

    free(fifo->buf);

    fifo->buf  = kmm_malloc( sizeof(can_np_msg_t) * size);
    if ( fifo->buf == NULL )
    {
        fifo->size = 0;
        ret = -1;
    }

    up_irq_restore(irqstate);
    return ret;
}

/**
 * @brief Add filter to fifo.
 * @param[in] fifo_id   : fifo id
 * @param[in] msg       : message to send.
 * @param[in] fifo_id   : filter id used.
 * @return 0 on success, -1 in case of error.
 */
int can_np_add_filter(int fifo_id, can_np_filter_t* filter, uint32_t filter_id)
{
    uint32_t regval;
    uint32_t mask;

    irqstate_t irqstate;
    irqstate = up_irq_save();

    mask = 1 << filter_id;

    if ( ( ( mask & CAN_FS1R_FSC_MASK           ) == 0 ) || 
         ( ( getreg32(STM32_CAN1_FA1R) & mask   ) != 0 )
       )
    {
        up_irq_restore(irqstate);
        canerr("ERROR: Filter %d is not free\n",filter_id);
        return -1;
    }

    /* Initialisation mode for the filter */
    modifyreg32(STM32_CAN1_FMR, 0, CAN_FMR_FINIT);

    /* 32-bit filters, 16 bits is not implemented */
    modifyreg32(STM32_CAN1_FS1R,0 ,mask);


    /* id1 */
    regval = canFRX_EXTENDED(filter->id1,
                             filter->flags & can_npFLAGS_RTR_ID1_ID_MASK);
    putreg32(regval,STM32_CAN1_FIR(filter_id,1));

    /* id2 or mask */
    regval = canFRX_EXTENDED(filter->id2,
                             filter->flags & can_npFLAGS_RTR_ID2_ID_MASK);
    putreg32(regval,STM32_CAN1_FIR(filter_id,2));

    regval = getreg32(STM32_CAN1_FM1R);
    if ( filter->flags & can_npFLAGS_FILT_ID_MASK )
    {
        /* Set Filter Mode in Id/Mask mode for the filter*/
        regval &= ~mask;
    }
    else
    {
        /* Set Filter Mode in Id/Mask mode for the filter*/
        regval |= mask;
    }
    putreg32(regval, STM32_CAN1_FM1R);

    /* Filter and fifo association */
    regval = getreg32(STM32_CAN1_FFA1R);
    switch(fifo_id)
    {
        case 0: regval &= ~mask; break;
        case 1: regval |=  mask; break;
        default: assert(false); break;
    }
    putreg32(regval, STM32_CAN1_FFA1R);

    /* Filter activation */
    modifyreg32(STM32_CAN1_FA1R, 0, mask );

    /* Leave the initialisation mode for the filter */
    modifyreg32(STM32_CAN1_FMR, CAN_FMR_FINIT, 0);

    up_irq_restore(irqstate);

    return 0;
}

/**
 * @brief push a message in tx fifo.
 * @param[in] mailbox_id    : mail box id
 * @param[in] msg           : message to send.
 * @param[in] timeout_ms    : Time out in millisecond
 * @return 0 on success, -1 in case of error.
 */
int can_np_send(int mailbox_id, can_np_msg_t* msg, int timeout_ms)
{
    int ret = 0;
    struct timespec tp;

    assert(mailbox_id < can_npMAILBOX_NBR);

    if ( clock_gettime(CLOCK_REALTIME, &tp) < 0 )
        return -1;

    tp.tv_sec  += timeout_ms/1000;
    tp.tv_nsec += (timeout_ms%1000)*1000000;
    if ( tp.tv_nsec >= 1000000000 )
    {
        tp.tv_nsec -= 1000000000;
        tp.tv_sec++;
    }

    /* Mailbox should be empty empty by construction */
    {
        irqstate_t irqstate;
        irqstate = up_irq_save();

        /* Mailbox empty */
        ret = stm32_can_write_mailbox(mailbox_id,msg);

        up_irq_restore(irqstate);
    }

    do
    {
        irqstate_t irqstate;

        if ( ret >= 0 )
        {
            ret = stm32_can_takesem(&stm32_can_mailbox[mailbox_id].tx_sem, &tp);
        }

        irqstate = up_irq_save();
        if ( ret >= 0 )
        {
            int tsr = stm32_can_mailbox[mailbox_id].tsr;
            if ( tsr & CAN_TSR_RQCP0 )
            { /* last Request is Done */
                if ( tsr & CAN_TSR_TXOK0 )
                {
                    ret = 1;
                }
                else
                {
                    ret = -1;
                }
            }
        }
        if ( ret < 0 )
        {
            int bits = 0;

            switch(mailbox_id)
            {
                case 0: bits = CAN_TSR_ABRQ0; break;
                case 1: bits = CAN_TSR_ABRQ1; break;
                case 2: bits = CAN_TSR_ABRQ2; break;
                default: ASSERT(false);
            }
            stm32_can_setbits(STM32_CAN_TSR_OFFSET,bits);

        }
        up_irq_restore(irqstate);
    }while( ret == 0 );

    return ret;
}


int can_np_recv(int fifo_id, can_np_msg_t* frame, int timeout_ms)
{
    int ret;
    struct timespec tp;
    can_fifo_rx_t* fifo;

    if ( clock_gettime(CLOCK_REALTIME, &tp) < 0 )
        return -1;

    tp.tv_sec  += timeout_ms/1000;
    tp.tv_nsec += (timeout_ms%1000)*1000000; /* 1e6 treated as double */
    if ( tp.tv_nsec >= 1000000000 ) /* 1e9 treated as double */
    {
        tp.tv_nsec -= 1000000000; /* 1e9 treated as double */
        tp.tv_sec++;
    }

    assert(fifo_id < can_npFIFO_NBR);

    ret = 0;
    fifo = &stm32_can_fifo_rx[fifo_id];
    while( ret == 0 )
    {
        int r;
        unsigned int r_idx;
        r = stm32_can_takesem(&fifo->rd_sem,&tp);

        irqstate_t irqstate;
        irqstate = up_irq_save();

        r_idx = fifo->r_idx;
        if ( r_idx != fifo->w_idx )
        {
            *frame = fifo->buf[r_idx];
            r_idx++;

            if ( r_idx >= fifo->size )
            {
                r_idx -= fifo->size;
            }

            fifo->r_idx = r_idx;
            ret = 1;
        }
        up_irq_restore(irqstate);

        if ( ( r < 0 ) && ( ret == 0 ) )
        {
            ret = -1;
            break; /* timeout */
        }
    }
    return ret;
}


