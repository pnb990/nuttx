/****************************************************************************
 * config/pnbchrono-v2/src/stm32_gpio_keypad.c
 * Driver for pnbchrono keypad hardware
 *
 *   Copyright (C) 2014 Pierre-noel Bouteville. All rights reserved.
 *   Author: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>

#include <nuttx/arch.h>
#include <nuttx/board.h>
#include <arch/board/board.h>

#include <stm32_gpio.h>

#include "pnbchrono-v2.h"

#include <string.h> // memset
#include <poll.h>
#include <errno.h>
#include <nuttx/kmalloc.h>
#include "nuttx/input/kbd_codec.h"

#if ( ( ! defined(CONFIG_DISABLE_POLL               ) ) &&\
      ( ! defined(CONFIG_STM32_GPIO_KEYPAD_INTERRUPT) ) )
#   include <nuttx/wqueue.h>
#   include <nuttx/clock.h>
#endif

//#include "up_arch.h"


#ifdef CONFIG_LIB_KBDCODEC
#   define STM32_GPIO_KEY_NONE      STM32_GPIO_KEY_SPECIAL(LAST_KEYCODE+1)
#   define STM32_GPIO_KEY_SPECIAL(x)    ( -(x)      ) 
#   define STM32_GPIO_KEY_IS_SPECIAL(x) ( (x) < 0   ) 
#endif

#define STM32_GPIO_KBD_LOG(...)
//#define STM32_GPIO_KBD_LOG(...) lldbg(__VA_ARGS__)
//#define STM32_GPIO_KBD_LOG(...) syslog(LOG_NOTICE,__VA_ARGS__)

#if ( defined(CONFIG_STM32_GPIO_KBD_BUFSIZE) && \
      (CONFIG_STM32_GPIO_KBD_BUFSIZE > 0) \
    )
#define HAS_KEY_BOARD
//#  define CONFIG_STM32_GPIO_KBD_BUFSIZE 64
//#  define CONFIG_STM32_GPIO_KBD_POLL_MS 100
#endif

/****************************************************************************
 * Fileops Prototypes and Structures
 ****************************************************************************/

typedef FAR struct file file_t;

static int stm32_gpio_kbd_open(file_t * filep);
static int stm32_gpio_kbd_close(file_t * filep);
static ssize_t stm32_gpio_kbd_read(file_t * filep, FAR char *buffer, size_t buflen);
#ifndef CONFIG_DISABLE_POLL
static int stm32_gpio_kbd_poll(file_t * filep, FAR struct pollfd *fds, bool setup);
#endif

static const struct file_operations keypad_ops =
{
    stm32_gpio_kbd_open,  /* open   */
    stm32_gpio_kbd_close, /* close  */
    stm32_gpio_kbd_read,  /* read   */
    NULL,                 /* write  */
    NULL,                 /* seek   */
    NULL                  /* ioctl  */
#ifndef CONFIG_DISABLE_POLL
    ,stm32_gpio_kbd_poll  /* poll   */
#endif
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
    ,NULL                 /* unlink */
#endif
};


/****************************************************************************
 * Private Type
 ****************************************************************************/
typedef struct 
{
    sem_t       mutex;
    sem_t       rd_sem;
    uint16_t    inndx;
    uint16_t    outndx;
    uint8_t     buf[CONFIG_STM32_GPIO_KBD_BUFSIZE];

#if ( ( ! defined(CONFIG_DISABLE_POLL               ) ) &&\
      ( ! defined(CONFIG_STM32_GPIO_KEYPAD_INTERRUPT) ) )
    sem_t   *poll_sem;
    struct work_s work;
#endif

}stm32_gpio_kbd_t;

struct stm32_gpio_kbd_stream 
{
    struct lib_outstream_s  stream;
    stm32_gpio_kbd_t         *dev;
};

typedef struct 
{

    /* pin and port of GPIO */

    int gpio;

#ifdef CONFIG_LIB_KBDCODEC
    /* in case of normal key, set special_key to KEYCODE_NORMAL */
 
    int key;
#endif

#ifdef CONFIG_STM32_GPIO_KEYPAD_INTERRUPT
    /* IRQ id of GPIO */

    int irq;
#endif

}stm32_gpio_mapping_t;

/****************************************************************************
 * Private Data.
 ****************************************************************************/
#define NUM_GPIO_KEYPAD (sizeof(key_mapping)/sizeof(key_mapping[0]))
static const stm32_gpio_mapping_t key_mapping[] = GPIO_KEYBOARD_MAPPING;

#ifndef CONFIG_STM32_GPIO_KEYPAD_INTERRUPT
static void stm32_gpio_kbd_worker(FAR void *arg);
#endif

static stm32_gpio_kbd_t stm32_gpio_kbd;
static uint32_t         stm32_gpio_pressed_key = 0;

/****************************************************************************
 * Private function.
 ****************************************************************************/



/******************************************************************************
 * Name: stm32_gpio_kbd_takesem
 ******************************************************************************/

static int stm32_gpio_kbd_takesem(FAR sem_t *sem, bool errout)
{
  /* Loop, ignoring interrupts, until we have successfully acquired the semaphore */

  while (sem_wait(sem) != OK)
    {
      /* The only case that an error should occur here is if the wait was awakened
       * by a signal.
       */

      ASSERT(get_errno() == EINTR);

      /* When the signal is received, should we errout? Or should we just continue
       * waiting until we have the semaphore?
       */

      if (errout)
        {
          return -EINTR;
        }
    }

  return OK;
}

/******************************************************************************
 * Name: stm32_gpio_kbd_givesem
 ******************************************************************************/

#define stm32_gpio_kbd_givesem(sem) (void)sem_post(sem)


/****************************************************************************
 * Name: stm32_gpio_kbd_putc
 ****************************************************************************/
static void stm32_gpio_kbd_putc(FAR struct lib_outstream_s *this, int ch)
{
    int next;
    struct stm32_gpio_kbd_stream *p = (struct stm32_gpio_kbd_stream*)this;
    stm32_gpio_kbd_t *dev = p->dev;
    irqstate_t irqstate;

    ASSERT(dev!=NULL);

    /* Pre-calculate the next 'inndx'.  We do this so that we can
     * check if the FIFO is full.
     */

    irqstate = up_irq_save(); /* disable IRQ */

    next = dev->inndx + 1;
    if (next >= CONFIG_STM32_GPIO_KBD_BUFSIZE)
    {
        next -= CONFIG_STM32_GPIO_KBD_BUFSIZE;
    }

    /* Is there space in the circular buffer for another byte?  If
     * the next 'inndx' would be equal to the 'outndx', then the
     * circular buffer is full.
     */

    if (next != dev->outndx)
    {
        /* Yes.. add the byte to the circular buffer */

        dev->buf[dev->inndx] = ch;
        dev->inndx = next;
        sem_post(&dev->rd_sem);
#ifndef CONFIG_DISABLE_POLL
        /* add event to waiting semaphore */
        if ( dev->poll_sem )
        {
            sem_post( dev->poll_sem );
        }
#endif
    }
    else
    {
        /* No.. the we have lost data */

        STM32_GPIO_KBD_LOG("Buffer overflow\n");
    }
    up_irq_restore(irqstate); /* enabled IRQ */
}

/****************************************************************************
 * Name: stm32_gpio_kbd_check_key
 ****************************************************************************/
static inline void stm32_gpio_kbd_check_key(int key_id, 
                                            struct lib_outstream_s * p_stream
                                           )
{
    uint32_t key_mask = 1<<key_id;
#ifdef CONFIG_LIB_KBDCODEC
    int key_code = key_mapping[key_id].key;
#endif

    if ( stm32_gpioread(key_mapping[key_id].gpio) == 0)
    {
        if ( ( stm32_gpio_pressed_key & key_mask ) == 0 )
        {
#ifdef CONFIG_LIB_KBDCODEC
            if ( STM32_GPIO_KEY_IS_SPECIAL(key_code) )
            {
                kbd_specpress(STM32_GPIO_KEY_SPECIAL(key_code),p_stream);
            }
            else
            {
                kbd_press(key_code,p_stream);
            }
#else
            p_stream->put(p_stream, key_id | BOARD_KEY_PRESSED );
#endif
            stm32_gpio_pressed_key |= key_mask;
        }
    }
    else
    {
        if ( ( stm32_gpio_pressed_key & key_mask ) != 0 )
        {
#ifdef CONFIG_LIB_KBDCODEC
            if ( STM32_GPIO_KEY_IS_SPECIAL(key_code) )
            {
                kbd_specrel(STM32_GPIO_KEY_SPECIAL(key_code),p_stream);
            }
            else
            {
                kbd_release(key_code,p_stream);
            }
#else
            p_stream->put(p_stream, key_id );
#endif
            stm32_gpio_pressed_key &= ~key_mask;
        }
    }
}

/****************************************************************************
 * Name: stm32_gpio_kbd_set_next_poll
 ****************************************************************************/
#ifndef CONFIG_DISABLE_POLL
static void stm32_gpio_kbd_set_next_poll(stm32_gpio_kbd_t* dev)
{
    int ret;
    ret = work_queue(HPWORK, &(dev->work), stm32_gpio_kbd_worker,
                    dev, MSEC2TICK(CONFIG_STM32_GPIO_KBD_POLL_MS));
    ASSERT(ret == OK);
}
#endif

/****************************************************************************
 * irq handler
 ****************************************************************************/
#ifdef CONFIG_STM32_GPIO_KEYPAD_INTERRUPT
int stm32_gpio_kbd_irq(int irq, FAR void* context)
{
    int i;

    struct stm32_gpio_kbd_stream stream = {
        .stream = { .put  = stm32_gpio_kbd_putc, .nput = 0 },
        .dev    = dev
    };

    UNUSED(context);

    i = NUM_GPIO_KEYPAD;
    while ( i-- )
    {
        if ( key_mapping[i].irq == irq )
        {
            stm32_gpio_kbd_check_key(i,(struct lib_outstream_s*)&stream);
        }
    }

    return 0;
}
#elif ( ! defined(CONFIG_DISABLE_POLL) )
static void stm32_gpio_kbd_worker(FAR void *arg)
{
    int i;

    stm32_gpio_kbd_t *dev = (stm32_gpio_kbd_t*)arg;

    struct stm32_gpio_kbd_stream stream = {
        .stream = { .put  = stm32_gpio_kbd_putc, .nput = 0 },
        .dev    = dev
    };

    i = NUM_GPIO_KEYPAD;
    while ( i-- )
    {
        stm32_gpio_kbd_check_key(i,(struct lib_outstream_s*)&stream);
    }

    stm32_gpio_kbd_set_next_poll(dev);

    return;
}
#endif


/****************************************************************************
 * Name: stm32_gpio_keypad_irq_init
 *
 * Description:
 *  Initialize All GPIO for key pad.
 * Input parameters:
 * Returned Value:
 *   None (User allocated instance initialized).
 ****************************************************************************/
void stm32_gpio_kbd_config_gpio(void)
{
    irqstate_t irqstate;
    int i = NUM_GPIO_KEYPAD;

    const stm32_gpio_mapping_t* _key_map = key_mapping;

#ifndef CONFIG_STM32_GPIO_KEYPAD_INTERRUPT
    /* stm32_gpio_kbd_check_key doesn't support more than 32 key */

    ASSERT(NUM_GPIO_KEYPAD<32);
#endif

    /* Disable interrupts until we are done.  This guarantees that the
     * following operations are atomic.
     */

    irqstate = up_irq_save(); /* disable IRQ */

    while(i--)
    {
        /* Configure the pin */

        stm32_configgpio(_key_map->gpio);

#ifdef CONFIG_STM32_GPIO_KEYPAD_INTERRUPT
        /* Configure the interrupt */

        stm32_gpiosetevent(_key_map->gpio, true, true, true, stm32_gpio_kbd_irq);
#endif

        _key_map++;
    }

    up_irq_restore(irqstate); /* enabled IRQ */
}


/****************************************************************************
 * Name: stm32_gpio_keypad_init
 *  Warning: should be called only one time
 *
 * Description:
 *  Initialize All GPIO for key pad.
 * Returned Value:
 *   None.
 ****************************************************************************/
int stm32_gpio_kbd_init(void)
{
    stm32_gpio_kbd_t *dev = &stm32_gpio_kbd;
    stm32_gpio_kbd_config_gpio();

    memset(dev,0,sizeof(*dev));
    //dev->poll_sem = NULL; already done */
    sem_init(&dev->rd_sem,    1, 0);
    sem_init(&dev->mutex,     1, 1);

#if ( ( ! defined(CONFIG_DISABLE_POLL               ) ) &&\
      ( ! defined(CONFIG_STM32_GPIO_KEYPAD_INTERRUPT) ) )
    stm32_gpio_kbd_set_next_poll(dev);
#endif

    register_driver("/dev/keypad", &keypad_ops, 0444, dev);

    return OK;
}



/****************************************************************************
 * Name: stm32_gpio_kbd_open
 ****************************************************************************/

static int stm32_gpio_kbd_open(file_t * filep)
{

    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_gpio_kbd_t *dev    = inode->i_private;

    ASSERT( dev != NULL );

    return OK;
}

/****************************************************************************
 * Name: stm32_gpio_kbd_close
 ****************************************************************************/

static int stm32_gpio_kbd_close(file_t * filep)
{

    /* nothing to do */

    return OK;
}

/****************************************************************************
 * Name: stm32_gpio_kbd_poll
 ****************************************************************************/

#ifndef CONFIG_DISABLE_POLL
static int stm32_gpio_kbd_poll(file_t * filep, FAR struct pollfd *fds, bool setup)
{

    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_gpio_kbd_t *dev    = inode->i_private;

    int ret = 0;

    /* Are we setting up the poll?  Or tearing it down? */

    ret = stm32_gpio_kbd_takesem(&dev->mutex, true);

    if (ret < 0)
    {
        /* A signal received while waiting for access to the poll data
         * will abort the operation.
         */

        return ret;
    }

    if (setup)
    {
        fds->revents = 0;
        /* This is a request to set up the poll.  Find an available
         * slot for the poll structure reference
         */

        if ( dev->poll_sem != NULL)
        {
            ret = -EINVAL;
            goto errout;
        }

        if ( dev->inndx != dev->outndx )
        {
            fds->revents |= (fds->events & POLLIN);
        }

        if ( fds->revents == 0 )
        {
            dev->poll_sem = fds->sem ;
        }
        else
        {
            sem_post(fds->sem);
            ret = 1;
        }
    }
    else if ( dev->poll_sem == fds->sem )
    {
        dev->poll_sem = NULL;
    }
errout:
    stm32_gpio_kbd_givesem(&dev->mutex);
    return ret;
}
#endif

/****************************************************************************
 * Name: stm32_gpio_kbd_read
 ****************************************************************************/

static ssize_t stm32_gpio_kbd_read(file_t * filep, FAR char *buf, size_t buflen)
{
    int ret;
    size_t size = 0;

    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_gpio_kbd_t *dev    = inode->i_private;
    ASSERT( dev != NULL );

    ret = stm32_gpio_kbd_takesem(&dev->mutex,true);
    if (ret < 0)
    {
        /* A signal received while waiting for access to the poll data
         * will abort the operation.
         */

        return ret;
    }

    while (size < buflen)
    {
        irqstate_t irqstate;

        irqstate = up_irq_save(); /* disable IRQ */

        if ( dev->inndx == dev->outndx )
        {
            up_irq_restore(irqstate); /* enabled IRQ */

            if ( size == 0 )
            {
                sem_wait( &dev->rd_sem);
            }
            else if ( sem_trywait( &dev->rd_sem) < 0 )
            {
                break;
            }

            irqstate = up_irq_save(); /* disable IRQ */
        }
        else
        {
            int ndx = dev->outndx;

            buf[size] = dev->buf[ndx];

            ndx++;
            if (ndx >= CONFIG_STM32_GPIO_KBD_BUFSIZE )
                ndx-=CONFIG_STM32_GPIO_KBD_BUFSIZE;

            dev->outndx = ndx;

            STM32_GPIO_KBD_LOG("Read %c,0x%2X\n",buf[size],buf[size]);

            size++; 
        }

        up_irq_restore(irqstate); /* enabled IRQ */
    }

    stm32_gpio_kbd_givesem( &dev->mutex );

    return size;
}

