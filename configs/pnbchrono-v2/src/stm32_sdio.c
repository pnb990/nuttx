/****************************************************************************
 * config/pnbchrono-v2/src/stm32_sdio.c
 *
 *   Copyright (C) 2016 Pierre-Noel Bouteville. All rights reserved.
 *   Author: Pierre-Noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdbool.h>
#include <stdio.h>
#include <debug.h>
#include <errno.h>
#include <sys/boardctl.h>
#include <string.h>

#include <arch/board/board.h>
#include <nuttx/board.h>
#include <nuttx/sdio.h>
#include <nuttx/mmcsd.h>

#include "stm32.h"
#include "pnbchrono-v2.h"

#ifdef CONFIG_FS_AUTOMOUNTER
#include <nuttx/fs/automount.h>
#endif

#ifdef HAVE_SDIO

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/* Configuration ************************************************************/

/* Card detections requires card support and a card detection GPIO */

#define HAVE_NCD   1
#if !defined(HAVE_SDIO) || !defined(GPIO_SDIO_NCD)
#  undef HAVE_NCD
#endif

#define HAVE_AUTOMOUNTER   1
#if !defined(HAVE_NCD) || !defined(CONFIG_FS_AUTOMOUNTER)
#  undef HAVE_AUTOMOUNTER
#endif

/************************************************************************************
 * Private Function Prototypes
 ************************************************************************************/

static int stm32_ncd_interrupt(int irq, FAR void *context, FAR void* arg);

#ifdef HAVE_AUTOMOUNTER
static int  stm32_automount_attach(FAR const struct automount_lower_s *lower,
                         automount_handler_t isr, FAR void *arg);
static void stm32_automount_enable(FAR const struct automount_lower_s *lower, 
                                   bool enable);
static bool stm32_automount_inserted(FAR const struct automount_lower_s *lower);
#endif

/************************************************************************************
 * Private Types
 ************************************************************************************/

#ifdef HAVE_AUTOMOUNTER
struct stm32_automount_state_s
{
    volatile automount_handler_t handler;    /* Upper half handler */
    FAR void *arg;                           /* Handler argument */
};
#endif

/****************************************************************************
 * Private Data
 ****************************************************************************/

static FAR struct sdio_dev_s *g_sdio_dev;

#ifdef HAVE_AUTOMOUNTER
struct stm32_automount_state_s g_stm32_automount_state;

static const struct automount_lower_s g_stm32_automount_lower = 
{
    .fstype     = BOARD_SDHC_FSTYPE,
    .blockdev   = BOARD_SDHC_BLOCK_DEV_PATH,
    .mountpoint = BOARD_SDHC_MOUNT_PATH,
    .ddelay     = MSEC2TICK(BOARD_SDHC_DEBOUNDE_MS),
    .udelay     = MSEC2TICK(BOARD_SDHC_UNMOUNT_MS),
    .attach     = stm32_automount_attach,
    .enable     = stm32_automount_enable,
    .inserted   = stm32_automount_inserted
};
#endif

/****************************************************************************
 * Private Functions
 ****************************************************************************/

#ifdef HAVE_AUTOMOUNTER
/**
 * @brief Attach a new MMSCD0 event handler
 *
 * @param[in]  lower An instance of the auto-mounter lower half state structure
 * @param[in]  isr   The new event handler to be attach
 * @param[in]  arg   Client data to be provided when the event handler is invoked.
 * @return Always returns OK
 *
 */
static int stm32_automount_attach(FAR const struct automount_lower_s *lower,
                                  automount_handler_t isr, FAR void *arg)
{
    /* Save the new handler info (clearing the handler first to eliminate race
     * conditions).
     */
    g_stm32_automount_state.handler = isr;
    g_stm32_automount_state.arg     = arg;
    return OK;
}

/**
 * @brief Enable card insertion/removal event detection
 *
 * @param[in]  lower  An instance of the auto-mounter lower half state structure
 * @param[in]  enable True: enable event detection; False: disable
 * @return None
 */
static void stm32_automount_enable(FAR const struct automount_lower_s *lower, 
                                   bool enable)
{
    UNUSED(lower);
    if (enable)
    {
        stm32_gpiosetevent(GPIO_SDIO_NCD,true,true,true,stm32_ncd_interrupt,
                           NULL);
    }
    else
    {
        stm32_gpiosetevent(GPIO_SDIO_NCD,false,false,false,NULL,NULL);
    }
}

/**
 * @brief Check if a card is inserted into the slot.
 *
 * @param[in]  lower An instance of the auto-mounter lower half state structure
 *
 * @return True if the card is inserted; False otherwise
 *
 ************************************************************************************/

static bool stm32_automount_inserted(FAR const struct automount_lower_s *lower)
{
    UNUSED(lower);
    return stm32_cardinserted();
}
#endif

/**
 * @brief Card detect interrupt handler.
 */
static int stm32_ncd_interrupt(int irq, FAR void *context, FAR void* arg)
{
    bool inserted;
    UNUSED(irq);
    UNUSED(context);
    UNUSED(arg);

    inserted = stm32_cardinserted();

    sdio_mediachange(g_sdio_dev, inserted);

#   ifdef HAVE_AUTOMOUNTER
    g_stm32_automount_state.handler(&g_stm32_automount_lower,
                                    g_stm32_automount_state.arg,inserted);
#   endif

    return OK;
}




/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32_sdio_initialize
 *
 * Description:
 *   Initialize SDIO-based MMC/SD card support
 *
 ****************************************************************************/

int stm32_sdio_initialize(void)
{
    int ret;

    /* Mount the SDIO-based MMC/SD block driver */
    /* First, get an instance of the SDIO interface */

    finfo("Initializing SDIO slot %d\n", SDIO_SLOTNO);

    g_sdio_dev = sdio_initialize(SDIO_SLOTNO);
    if (!g_sdio_dev)
    {
        ferr("ERROR: Failed to initialize SDIO slot %d\n", SDIO_SLOTNO);
        return -ENODEV;
    }

    /* Now bind the SDIO interface to the MMC/SD driver */

    finfo("Bind SDIO to the MMC/SD driver, minor=%d\n", SDIO_MINOR);

    ret = mmcsd_slotinitialize(SDIO_MINOR, g_sdio_dev);
    if (ret != OK)
    {
        ferr("ERROR: Failed to bind SDIO to the MMC/SD driver: %d\n", ret);
        return ret;
    }

    finfo("Successfully bound SDIO to the MMC/SD driver\n");

#ifdef HAVE_NCD
    stm32_configgpio(GPIO_SDIO_NCD); /* configure GPIO */
#   ifdef HAVE_AUTOMOUNTER
    if ( automount_initialize(&g_stm32_automount_lower) == NULL)
    {
        ferr("ERROR: Failed to initialize auto-mounter\n");
    }
#   else
    stm32_gpiosetevent(GPIO_SDIO_NCD, true, true, true, stm32_ncd_interrupt,
                       NULL);
#   endif
#endif
    {
        irqstate_t flags = enter_critical_section();
        stm32_ncd_interrupt(0,NULL,NULL);
        leave_critical_section(flags);
    }

    return OK;
}

/****************************************************************************
 * Name: stm32_cardinserted
 *
 * Description:
 *   Check if a card is inserted into the selected HSMCI slot
 *
 ****************************************************************************/

bool stm32_cardinserted(void)
{
    bool inserted;

#ifdef HAVE_NCD
    inserted = stm32_gpioread(GPIO_SDIO_NCD);
    if ( inserted )
    {
        finfo("Slot inserted\n");
    }
    else
    {
        finfo("Slot ejected\n");
    }
#else
    inserted = true;
    finfo("Slot No card detect assume inserted\n");
#endif
    return inserted;
}


#endif /* HAVE_SDIO */
