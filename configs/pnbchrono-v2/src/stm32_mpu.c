/************************************************************************************
 * configs/pnbchrono-v2/src/stm32_mpu.c
 *
 *   Copyright (C) 2015 Pierre-Noel Bouteville. All rights reserved.
 *   Author: Pierre-Noel Bouteville <pnb990@gmail.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>

#include <nuttx/board.h>
#include <arch/board/board.h>

#include "chip.h"
#include "up_arch.h"
#include "up_internal.h"
#include "stm32.h"
#include "pnbchrono-v2.h"

#if ( ( defined CONFIG_SENSOR_MPU6050 ) || \
      ( defined CONFIG_SENSOR_MPU9150 ) || \
      ( defined CONFIG_SENSOR_MPU6500 ) || \
      ( defined CONFIG_SENSOR_MPU9250 ) )
#include <nuttx/sensors/inv_mpu.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/


int stm32_initialize_mpu(void)
{
    struct mpu_low_s * low;
    struct i2c_master_s * i2c;
    struct mpu_inst_s* mpu_inst;

    /* i2c port 1 */

    i2c = stm32_i2cbus_initialize(GPIO_MPU_I2C);

    if ( i2c == NULL )
    {
        syslog(LOG_ERR,"Cannot initialize I2C !\n");
        return -1;
    }

    low = mpu_low_i2c_init(0, MPU9250_MPU_ADDR, MPU9250_AKM_ADDR, i2c, 
                           BOARD_I2C_FREQUENCY);
    if ( low == NULL )
    {
        syslog(LOG_ERR,"Cannot initialize mpu_low !\n");
        return -1;
    }

    mpu_inst = mpu_instantiate(low);
    if ( mpu_inst == NULL ) 
    {
        syslog(LOG_ERR,"Cannot initialize mpu instance !\n");
        return -1;
    }

    if ( mpu_fileops_init(mpu_inst, "/dev/invmpu0", 0) < 0 )
    {
        syslog(LOG_ERR,"Cannot register mpu device !\n");
        return -1;
    }

    return OK;
}

#endif
