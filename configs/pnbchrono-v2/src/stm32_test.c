/****************************************************************************
 * configs/pnbchrono-v2/src/stm32_test.c
 *
 *   Copyright (C) 2012, 2015 Gregory Nutt. All rights reserved.
 *   Authors: Gregory Nutt <gnutt@nuttx.org>
 *            Diego Sanchez <dsanchez@nx-engineering.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <arch/board/board.h>
#include <nuttx/config.h>

#include <debug.h>

#include <nuttx/arch.h>
#include <nuttx/board.h>
#include <nuttx/clock.h>

#include <fcntl.h>
#include <stdlib.h>
#include <nuttx/mm/mm.h>
#include <nuttx/can/can.h>

#include "pnbchrono-v2.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/* Configuration ************************************************************/


/****************************************************************************
 * Private Data
 ****************************************************************************/

#ifdef CONFIG_SCHED_CPULOAD
static systime_t next_monitoring = 0;
#endif

/****************************************************************************
 * Private Functions
 ****************************************************************************/

#ifdef CONFIG_SCHED_CPULOAD
static void stm32_monitoring(void)
{
    syslog(LOG_INFO,"\f");
#ifdef CONFIG_STACK_COLORATION
    int stack_size;
    int stack_used;
#endif
    {
        struct mallinfo mem;

        (void)mm_mallinfo(&g_mmheap,&mem);
        syslog(LOG_INFO,"             total       used       free    largest\n");
        syslog(LOG_INFO,"Mem:   %11d%11d%11d%11d\n",mem.arena, mem.uordblks, 
               mem.fordblks, mem.mxordblk);
    }

#if CONFIG_ARCH_INTERRUPTSTACK > 3
    stack_used = up_check_intstack();
    stack_size = CONFIG_ARCH_INTERRUPTSTACK;
    syslog(LOG_INFO,"Int   :               (%4d/%4d) name:Interrupt\n",
           stack_used,stack_size);
#endif
    {
        int i;
        for (i = 0; i < CONFIG_MAX_TASKS; i++ )
        {
            struct cpuload_s cpuload;
            const char* name = "NoName";
            struct tcb_s *tcb;
            uint8_t priority = 0;
            tcb = sched_gettcb(i);

            if ( tcb != NULL )
            {
#if CONFIG_TASK_NAME_SIZE > 0
                name = tcb->name;
#endif
                priority = tcb->sched_priority;
            }

#ifdef CONFIG_STACK_COLORATION
            {
                stack_used = up_check_tcbstack(tcb);
                stack_size = tcb->adj_stack_size;
            }
#endif

            if ( clock_cpuload(i,&cpuload) < 0 )
                continue;

#ifdef CONFIG_STACK_COLORATION
            syslog(LOG_INFO,"PID %2d: %3d (%3d/%3d) (%4d/%4d) prio %d name:%s\n",i,
                   cpuload.active*100/cpuload.total,cpuload.active,cpuload.total,
                   stack_used,stack_size,priority,name);
#else
            syslog(LOG_INFO,"PID %2d: %3d (%3d/%3d) prio %d name:%s\n",i,
                   cpuload.active*100/cpuload.total,cpuload.active,cpuload.total,
                   priority,name);
#endif
        }
    }
}
#endif

#ifdef CONFIG_SCHED_CPULOAD
static inline systime_t stm32_get_next(int period_s)
{
    systime_t now = clock_systimer();
    return ( ( (now / SEC2TICK(period_s) ) +1 ) * SEC2TICK(period_s));
}
#endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void stm32_test(void)
{
#if 0
    struct timespec tp;
    static time_t last;

    stm32_chrono_get_time(&tp);
    if ( last != tp.tv_sec )
    {
        last = tp.tv_sec;
        if ( last & 1 )
        {
            board_autoled_on(LED_USER2);
        }
        else
        {
            board_autoled_off(LED_USER2);
        }
    }
#endif

#ifdef CONFIG_SCHED_CPULOAD
    if ( next_monitoring < clock_systimer() )
    {
        stm32_monitoring();
        next_monitoring = stm32_get_next(CONFIG_SCHED_CPULOAD_TIMECONSTANT);
    }
#endif

}

