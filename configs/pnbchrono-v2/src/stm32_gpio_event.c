/****************************************************************************
 * configs/stm32gg-pnbchrono/src/stm32_gpio_event.c
 *
 *   Copyright (C) 2018 Pierre-noel Bouteville. All rights reserved.
 *   Author: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>

#include <nuttx/arch.h>
#include <nuttx/irq.h>
#include <nuttx/clock.h>

#include <arch/irq.h>
#include <arch/board/board.h>
#include <pnbchrono/gpio_event.h>

#include <string.h>
#include <poll.h>
#include <errno.h>
#include <nuttx/kmalloc.h>

#include "up_arch.h"
#include "stm32_gpio.h"
#include "pnbchrono-v2.h"

#ifdef CONFIG_STM32_GPIO_EVENT
#   define gpioevinfo(fmt,...)   _info("SNSinf "fmt, ##__VA_ARGS__)
#else
#   define gpioevinfo(fmt,...)
#endif

#ifdef CONFIG_STM32_SENSORS_ERROR 
#   define gpioeverr(fmt,...)    _err("SNSerr "fmt, ##__VA_ARGS__)
#else
#   define gpioeverr(fmt,...)
#endif


/****************************************************************************
 * SENSORS config
 ****************************************************************************/
#define FIFO_SIZE           16

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Fileops Prototypes and Structures
 ****************************************************************************/

typedef FAR struct file file_t;

static int stm32_gpioev_open(      file_t * filep);
static int stm32_gpioev_close(     file_t * filep);
static ssize_t stm32_gpioev_read(  file_t * filep, FAR char *buf, 
                                      size_t buflen);
static int stm32_gpioev_ioctl(     file_t * filep, int cmd, 
                                      unsigned long arg );
#ifndef CONFIG_DISABLE_POLL
static int stm32_gpioev_poll(file_t * filep, 
                                FAR struct pollfd *fds, 
                                bool setup
                               );
#endif

static const struct file_operations gpioev_ops =
{
    .open   = stm32_gpioev_open,
    .close  = stm32_gpioev_close,
    .read   = stm32_gpioev_read,
    .write  = NULL,                
    .seek   = NULL,
    .ioctl  = stm32_gpioev_ioctl, 
#ifndef CONFIG_DISABLE_POLL
    .poll   = stm32_gpioev_poll, 
#endif
};

/****************************************************************************
 * Name: stm32_gpioev_dev_t
 * Description:
 *  variable of keypad
 ****************************************************************************/
typedef struct 
{
    int     open_count;     /* open counter */

    uint32_t gpio;          /* GPIO monitored used */
    bool    risingedge;     /* GPIO trigged rising edge */
    bool    fallingedge;    /* GPIO trigged falling edge */

    /* fifo */
    sem_t   *poll_sem;      /* Poll event semaphore */
    sem_t   mutex;          /* mutex protection */
    sem_t   rd_sem;         /* fifo semaphore */
    int     inndx;          /* fifo read index */
    int     outndx;         /* fifo write index */
    gpioev_t buf[FIFO_SIZE];/* fifo data */

    int (*gettime)(struct timespec* tp);/* get time function */

}stm32_gpioev_dev_t;


/************************************************************************************
 * Name: stm32_gpioev_takesem
 ************************************************************************************/

static int stm32_gpioev_takesem(FAR sem_t *sem, bool errout)
{
  /* Loop, ignoring interrupts, until we have successfully acquired the semaphore */

  while (sem_wait(sem) != OK)
    {
      /* The only case that an error should occur here is if the wait was awakened
       * by a signal.
       */

      ASSERT(get_errno() == EINTR);

      /* When the signal is received, should we errout? Or should we just continue
       * waiting until we have the semaphore?
       */

      if (errout)
        {
          return -EINTR;
        }
    }

  return OK;
}

/*******************************************************************************
 * Name: stm32_gpioev_givesem
 ******************************************************************************/

#define stm32_gpioev_givesem(sem) (void)sem_post(sem)


/****************************************************************************
 * Name: stm32_chrono_level
 ****************************************************************************/

/****************************************************************************
 * Name: stm32_gpioev_irq
 *
 * Description:
 *  Manage gpio interrupt
 * Input parameters:
 *  _key_map    - first key mapping of mapping GPIO<=>Key list.
 *                    to Finish list set Pin with negative value.
 * Returned Value:
 *   None (User allocated instance initialized).
 ****************************************************************************/
int stm32_gpioev_irq(int irq, FAR void *context, FAR void *arg)
{
    int ndx;
    gpioev_type_t type;
    struct timespec tp;
    gpioev_t* ptr;
    FAR stm32_gpioev_dev_t *dev = arg;

    if ( ( dev->risingedge == true ) && ( dev->fallingedge == false ) )
    {
        if ( stm32_gpioread(dev->gpio) )
        {
            type = gpioev_RAISED;
        }
        else
        {
            type = gpioev_FALLED;
        }
    }
    else if ( dev->risingedge == true )
    {
        type = gpioev_RAISED;
    }
    else if ( dev->fallingedge == true )
    {
        type = gpioev_FALLED;
    }
    else
    {
        return ERROR;
    }

    if ( dev->gettime(&tp) < 0 )
    {
        return ERROR;
    }

    ndx = dev->inndx;

    ptr = &dev->buf[ndx];

    ndx++;
    if ( ndx >= FIFO_SIZE )
    {
        ndx -= FIFO_SIZE;
    }

    if ( ndx == dev->outndx )
    {
        return ERROR; /* FIFO Full ! */
    }

    ptr->type = type;
    ptr->tp   = tp;

    dev->inndx = ndx;

    return OK;
}

/****************************************************************************
 * Name: stm32_gpioev_init
 *
 * Description:
 *  Initialize All GPIO for key pad.
 * Input parameters:
 *  _key_map    - first key mapping of mapping GPIO<=>Key list.
 *                    to Finish list set Pin with negative value.
 * Returned Value:
 *   None (User allocated instance initialized).
 ****************************************************************************/
int stm32_gpioev_register( const char* devname, uint32_t gpio, bool risingedge, 
                           bool fallingedge, 
                           int (*gettime)(struct timespec *tp) )
{
    irqstate_t irqstate;
    stm32_gpioev_dev_t *dev;

    dev = (stm32_gpioev_dev_t*)kmm_malloc(sizeof(stm32_gpioev_dev_t));
    if ( dev == NULL )
    {
        gpioeverr("Cannot allocate it!\n");
        return -ENODEV;
    }


    /* Disable interrupts until we are done.  This guarantees that the
     * following operations are atomic.
     */

    irqstate = up_irq_save(); /* Disable IRQ ================================= */

    memset(dev,0,sizeof(*dev));
    //dev->open_count = 0; already done */
    //dev->poll_sem = NULL; already done */
    sem_init(&dev->mutex,  0, 1);
    dev->risingedge     = risingedge;
    dev->fallingedge    = fallingedge;
    dev->gettime        = gettime;

    up_irq_restore(irqstate); /* Enable IRQ ================================= */

    return register_driver(devname, &gpioev_ops, 0444, dev);
}


/****************************************************************************
 * Name: stm32_gpioev_open
 ****************************************************************************/

static int stm32_gpioev_open(file_t * filep)
{
    int ret;
    FAR struct inode        *inode = filep->f_inode;
    FAR stm32_gpioev_dev_t  *dev   = inode->i_private;

    ASSERT( dev != NULL );

    ret = stm32_gpioev_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    dev->open_count++;

    if ( dev->open_count == 1 )
    {
        /* reset FIFO */

        dev->inndx  = 0;
        dev->outndx = 0;

        /* configure GPIO */

        ret = stm32_gpiosetevent(dev->gpio, dev->risingedge, dev->fallingedge,
                       false, stm32_gpioev_irq, dev);

        if ( ret < 0 )
        {
            gpioeverr("Cannot configure GPIO IRQ");
            dev->open_count--;
            ret = ERROR;
        }
    }

    stm32_gpioev_givesem( &dev->mutex );

    return ret;
}

/****************************************************************************
 * Name: stm32_gpioev_close
 ****************************************************************************/

static int stm32_gpioev_close(file_t * filep)
{
    int ret;
    FAR struct inode *inode = filep->f_inode;
    FAR stm32_gpioev_dev_t *dev    = inode->i_private;

    ret = stm32_gpioev_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    dev->open_count--;

    DEBUGASSERT(dev->open_count >= 0);

    if ( dev->open_count == 0 )
    {
        ret = stm32_gpiosetevent(dev->gpio, false, false, false, NULL, NULL);
        if ( ret < 0 )
        {
            gpioeverr("Cannot reset GPIO IRQ");
            ret = ERROR;
        }
    }

    stm32_gpioev_givesem( &dev->mutex );

    return ret;
}


/****************************************************************************
 * Name: stm32_gpioev_ioctl
 ****************************************************************************/

static int stm32_gpioev_ioctl(file_t *filep, int cmd, unsigned long arg)
{
    int ret;
    FAR struct inode *inode         = filep->f_inode;
    FAR stm32_gpioev_dev_t *dev    = inode->i_private;

    irqstate_t irqstate;

    ASSERT( dev != NULL );

    ret = stm32_gpioev_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    switch(cmd)
    {
        case GPIOEV_GET_LAST:
            irqstate = up_irq_save(); /* Disable IRQ ======================= */
            if ( dev->inndx != dev->outndx )
            {
                int ndx = dev->inndx - 1;
                if ( ndx < 0 )
                {
                    ndx += FIFO_SIZE;
                }
                *((gpioev_t*)arg) = dev->buf[ndx];
                dev->inndx =0;
                dev->outndx=0;
            }
            else
            {
                ret = -EINVAL;
            }
            up_irq_restore(irqstate); /* Enable  IRQ ======================= */
            
        default:
            ret = -EINVAL;
    }

    stm32_gpioev_givesem( &dev->mutex );

    return ret;
}

/****************************************************************************
 * Name: stm32_gpioev_poll
 ****************************************************************************/

#ifndef CONFIG_DISABLE_POLL
static int stm32_gpioev_poll(file_t * filep, FAR struct pollfd *fds, bool setup)
{
    int ret;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_gpioev_dev_t *dev    = inode->i_private;
    irqstate_t irqstate;

    ret = stm32_gpioev_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    if (setup)
    {

        fds->revents = 0;
        /* This is a request to set up the poll.  Find an available
         * slot for the poll structure reference
         */

        if ( dev->poll_sem != NULL)
        {
            ret = -EINVAL;
            goto errout;
        }

        irqstate = up_irq_save();
        if ( dev->inndx != dev->outndx )
        {
            fds->revents |= (fds->events & POLLIN);
        }
        up_irq_restore(irqstate);

        if ( fds->revents == 0 )
        {
            dev->poll_sem = fds->sem ;
        }
        else
        {
            sem_post(fds->sem);
            ret = 1;
        }
    }
    else if ( dev->poll_sem == fds->sem )
    {
        irqstate = up_irq_save();
        if ( dev->inndx != dev->outndx )
        {
            fds->revents |= (fds->events & POLLIN);
        }
        up_irq_restore(irqstate);
        dev->poll_sem = NULL;
    }
errout:
    stm32_gpioev_givesem(&dev->mutex);
    return ret;
}
#endif

/****************************************************************************
 * Name: stm32_gpioev_read
 ****************************************************************************/

static ssize_t stm32_gpioev_read(file_t * filep, FAR char *buf, size_t buflen)
{
    int ret;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_gpioev_dev_t *dev    = inode->i_private;

    gpioev_t* ptr = (gpioev_t*)buf;
    ssize_t size = 0;

    ASSERT( dev != NULL );

    ret = stm32_gpioev_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    while ( buflen >= sizeof(gpioev_t) )
    {
        irqstate_t irqstate;
        int len = buflen;
        UNUSED(len);

        if ( size == 0 )
            sem_wait( &dev->rd_sem);
        else if ( sem_trywait( &dev->rd_sem ) < 0 )
            break;

        irqstate = up_irq_save(); /* disable IRQ */

        if ( dev->inndx != dev->outndx )
        {
            int ndx = dev->outndx;

            *ptr = dev->buf[ndx];

            ndx++;
            if (ndx >= FIFO_SIZE )
                ndx -= FIFO_SIZE;

            dev->outndx = ndx;
        }

        up_irq_restore(irqstate); /* enabled IRQ */


        gpioevinfo("Read %d bytes of ev %d timespec %8d.%03d\n",
                      len,
                      ptr->tp.tv_sec,
                      ptr->tp.tv_nsec/1000000
                     );

        ptr++;
        buflen  -= sizeof(gpioev_t);
        size    += sizeof(gpioev_t); 
    }

    stm32_gpioev_givesem( &dev->mutex );

    return size;
}




