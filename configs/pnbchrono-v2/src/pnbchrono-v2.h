/****************************************************************************
 * configs/pnbchrono_v2/src/stm32f4-pnbchrono.h
 *
 *   Copyright (C) 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Pierre-Noel Bouteville <pnb990@gmail.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __CONFIGS_PNBCHRONO_SRC_STM32F4_PNBCHRONO_H
#define __CONFIGS_PNBCHRONO_SRC_STM32F4_PNBCHRONO_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/compiler.h>
#include <time.h>               /* for stm32_chrono_get_time */
#include <stdint.h>
#include <arch/stm32/chip.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
/* Configuration *************************************************************/
/* How many SPI modules does this chip support? */

/* Assume that we have everything */

#define HAVE_USBDEV     1
#define HAVE_USBHOST    1
#define HAVE_USBMONITOR 1
#define HAVE_SDIO       1
#define HAVE_RTC_DRIVER 1
#define HAVE_ELF        1

/* Can't support USB host or device features if USB OTG FS is not enabled */

#ifndef CONFIG_STM32_OTGFS
#  undef HAVE_USBDEV
#  undef HAVE_USBHOST
#  undef HAVE_USBMONITOR
#endif

/* Can't support USB device monitor if USB device is not enabled */

#ifndef CONFIG_USBDEV
#  undef HAVE_USBDEV
#  undef HAVE_USBMONITOR
#endif

/* Can't support USB host is USB host is not enabled */

#ifndef CONFIG_USBHOST
#  undef HAVE_USBHOST
#endif

/* Check if we should enable the USB monitor before starting NSH */

#if !defined(CONFIG_USBDEV_TRACE) || !defined(CONFIG_SYSTEM_USBMONITOR)
#  undef HAVE_USBMONITOR
#endif

/* Can't support MMC/SD features if mountpoints are disabled or if SDIO support
 * is not enabled.
 */

#if defined(CONFIG_DISABLE_MOUNTPOINT) || !defined(CONFIG_STM32_SDIO)
#  undef HAVE_SDIO
#endif

#define SDIO_MINOR  0  /* Any minor number, default 0 */
#define SDIO_SLOTNO 0  /* Only one slot */

/* Check if we can support the RTC driver */

#if !defined(CONFIG_RTC) || !defined(CONFIG_RTC_DRIVER)
#  undef HAVE_RTC_DRIVER
#endif

/* ELF */

#if defined(CONFIG_BINFMT_DISABLE) || !defined(CONFIG_ELF)
#  undef HAVE_ELF
#endif


/****************************************************************************
 * Public Types
 ****************************************************************************/

typedef enum
{
    ANA_FRONT = 0,
    ANA_REAR,
    ANA_EXT1,
    ANA_EXT2,
    ANA_LIGHT_TEMP,
    ANA_VPWR,
    ANA_VBAT,
    ANA_NBR
}ana_id_t;

typedef enum
{
    vl53l0x_eID_FONT = 0,
    vl53l0x_eID_REAR,
    vl53l0x_eNBR
}vl53l0x_id_t;

/****************************************************************************
 * Public data
 ****************************************************************************/

extern uint16_t ana_values[ANA_NBR]; /* adc value stored in this array */
extern uint32_t ana_overrun_nbr;

#ifndef __ASSEMBLY__

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32_spiinitialize
 *
 * Description:
 *   Called to configure SPI chip select GPIO pins for the PNBChrono
 *   board.
 *
 ****************************************************************************/

void weak_function stm32_spiinitialize(void);

/****************************************************************************
 * Name: stm32_bringup
 *
 * Description:
 *   Perform architecture-specific initialization
 *
 *   CONFIG_BOARD_INITIALIZE=y :
 *     Called from board_initialize().
 *
 *   CONFIG_BOARD_INITIALIZE=y && CONFIG_NSH_ARCHINIT=y :
 *     Called from the NSH library
 *
 ****************************************************************************/

int stm32_bringup(void);

/****************************************************************************
 * Name: stm32_extmemgpios
 *
 * Description:
 *   Initialize GPIOs for external memory usage
 *
 ****************************************************************************/

#ifdef CONFIG_STM32_FSMC
void stm32_extmemgpios(const uint32_t *gpios, int ngpios);
#endif

/****************************************************************************
 * Name: stm32_extmemaddr
 *
 * Description:
 *   Initialize adress line GPIOs for external memory access
 *
 ****************************************************************************/

#ifdef CONFIG_STM32_FSMC
void stm32_extmemaddr(int naddrs);
#endif

/****************************************************************************
 * Name: stm32_extmemdata
 *
 * Description:
 *   Initialize data line GPIOs for external memory access
 *
 ****************************************************************************/

#ifdef CONFIG_STM32_FSMC
void stm32_extmemdata(int ndata);
#endif

/****************************************************************************
 * Name: stm32_enablefsmc
 *
 * Description:
 *  enable clocking to the FSMC module
 *
 ****************************************************************************/

#ifdef CONFIG_STM32_FSMC
void stm32_enablefsmc(void);
#endif

/****************************************************************************
 * Name: stm32_disablefsmc
 *
 * Description:
 *  enable clocking to the FSMC module
 *
 ****************************************************************************/

#ifdef CONFIG_STM32_FSMC
void stm32_disablefsmc(void);
#endif

/**
 *   @brief configure keyboard.
 */
int stm32_gpio_kbd_init(void);

/**
 *   @brief configure Bus Can.
 */
#ifdef CONFIG_CAN
int stm32_can_initialise(void);
#endif
#ifdef CONFIG_CAN_NON_POSIX_CAN
int stm32_can_np_init(void);
#endif

/**
 *   @brief idle function to manage test functions.
 */
void stm32_test(void);

/**
 * @brief Initialize SDIO-based MMC/SD card support.
 */
#if !defined(CONFIG_DISABLE_MOUNTPOINT) && defined(CONFIG_STM32_SDIO)
int stm32_sdio_initialize(void);
#endif

#if ( ( defined CONFIG_SENSOR_MPU6050 ) || \
      ( defined CONFIG_SENSOR_MPU9150 ) || \
      ( defined CONFIG_SENSOR_MPU6500 ) || \
      ( defined CONFIG_SENSOR_MPU9250 ) )
int stm32_initialize_mpu(void);
#endif

#if defined(CONFIG_STM32_SDIO)
bool stm32_cardinserted(void);
#endif

/**
 *   @brief initialize chronometer.
 */
int stm32_chrono_init(void);

/**
 *   @brief give chronometer current time.
 */
int stm32_chrono_get_time(struct timespec* tp);

/****************************************************************************
 * Name: stm32_fast_sns_init
 ****************************************************************************/
int stm32_fast_sns_init( void );

/****************************************************************************
 * name: stm32_initialize_mlx90614
 ****************************************************************************/
int stm32_initialize_mlx90614( void );

/****************************************************************************
 * name: stm32_initialize_vl53l0x
 * name: stm32_read_vl53l0x
 ****************************************************************************/
#if defined(CONFIG_VL53L0X)
int stm32_vl53l0x_initialize( void );
int stm32_vl53l0x_lpwork_reset(void);
int stm32_vl53l0x_get_range(vl53l0x_id_t id);
const char *stm32_vl53l0x_id_str(vl53l0x_id_t id);
#endif

/****************************************************************************
 * Name: stm32_analog_init
 ****************************************************************************/
int stm32_analog_init( void );

/****************************************************************************
 * Name: stm32_gps_register
 ****************************************************************************/
#ifdef CONFIG_STM32_GPS_DRV
int stm32_gps_register( const char* devname, uint32_t gpio, 
                        int (*gettime)(struct timespec *tp) );
#endif

/****************************************************************************
 * Name: stm32_gpioev_register
 ****************************************************************************/
int stm32_gpioev_register( const char* devname, uint32_t gpio, bool risingedge, 
                           bool fallingedge, 
                           int (*gettime)(struct timespec *tp) );

/****************************************************************************
 * Name: stm32_pwm_setup
 ****************************************************************************/
int stm32_pwm_setup(void);

/****************************************************************************
 * Name: stm32_ppm
 ****************************************************************************/
struct stm32_ppm_s;
struct stm32_ppm_s* stm32_ppm_init( int ppm_id ,unsigned int freq);
float stm32_ppm_get_last(   struct stm32_ppm_s* dev             );
void  stm32_ppm_set_rate(   struct stm32_ppm_s* dev, float rate );
int   stm32_ppm_setfreq(    struct stm32_ppm_s* dev, int freq   );

/****************************************************************************
 * Name: stm32_usb
 ****************************************************************************/
int stm32_usbdev_init(void);
void stm32_usbdev_show_trace(void);
int stm32_usbdev_disable_usbmsc(void);
int stm32_usbdev_enable_usbmsc(void);
int stm32_usbdev_is_enable(void);
int stm32_usbdev_is_connected(void);

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_STM32F4_PNBCHRONO_SRC_STM32F4_PNBCHRONO_H */
