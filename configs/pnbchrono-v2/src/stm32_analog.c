/****************************************************************************
 * configs/stm32gg-pnbchrono/src/stm32_analog.c
 *
 *   Copyright (C) 2015 Pierre-noel Bouteville. All rights reserved.
 *   Author: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>

#include <nuttx/arch.h>
#include <nuttx/irq.h>

#include <arch/irq.h>
#include <arch/board/board.h>

#include <string.h>
#include <poll.h>
#include <errno.h>
#include <nuttx/kmalloc.h>

#include "up_arch.h"
#include "stm32_gpio.h"
#include "stm32_rcc.h"
#include "stm32_adc.h"
#include "stm32_dma.h"
#include "stm32_tim.h"
#include "pnbchrono-v2.h"

//#define STM32_ANALOG_LOG(...)
//#define STM32_ANALOG_LOG(lvl,...)    dbg(__VA_ARGS__)
//#define STM32_ANALOG_LOG(lvl,...)    lldbg(__VA_ARGS__)
//#define STM32_ANALOG_LOG(...)        syslog(__VA_ARGS__)
#define STM32_ANALOG_LOG(lvl,...)      {\
    if (lvl <= LOG_WARNING)\
        syslog(lvl,__VA_ARGS__);\
}


/****************************************************************************
 * Analog config
 ****************************************************************************/

//#define ADC_DMA_CHANNEL 0
#define ADC_BASE        STM32_ADC1_BASE
#define ADC_IRQ         STM32_IRQ_ADC
#define ADC_INPUT_NBR   ANA_NBR
#define TIMER_CLK_FREQ  10000 /* 10 kHz */
#define TIMER_FREQ      100   /* 100 Hz */
#define ADC_DMA_CONTROL_WORD (DMA_SCR_MSIZE_16BITS | \
                              DMA_SCR_PSIZE_16BITS | \
                              DMA_SCR_MINC | \
                              DMA_SCR_CIRC | \
                              DMA_SCR_DIR_P2M)
/* TODO: ADC CPU temperature for TAMB */

static const uint32_t  g_gpiolist[ADC_INPUT_NBR]     = 
{
    GPIO_ADC_FRONT,
    GPIO_ADC_REAR,
    GPIO_ADC_EXT1,
    GPIO_ADC_EXT2,
    GPIO_ADC_LIGHT_TEMP,
    GPIO_ADC_VPWR,
    GPIO_ADC_VBAT
};

static const uint8_t  g_chanlist[ADC_INPUT_NBR]     = 
{
    GPIO_ADC_CH_FRONT,
    GPIO_ADC_CH_REAR,
    GPIO_ADC_CH_EXT1,
    GPIO_ADC_CH_EXT2,
    GPIO_ADC_CH_LIGHT_TEMP,
    GPIO_ADC_CH_VPWR,
    GPIO_ADC_CH_VBAT
};

/* RCC reset ****************************************************************/

#define STM32_RCC_RSTR   STM32_RCC_APB2RSTR
#define RCC_RSTR_ADC1RST RCC_APB2RSTR_ADCRST
#define RCC_RSTR_ADC2RST RCC_APB2RSTR_ADCRST
#define RCC_RSTR_ADC3RST RCC_APB2RSTR_ADCRST
#define RCC_RSTR_ADC4RST RCC_APB2RSTR_ADCRST

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* ADC Register access */

static inline uint32_t  adc_getreg(int offset);
static inline void      adc_putreg(int offset, uint32_t value);
static inline void      adc_modifyreg(int offset, uint32_t mask, uint32_t regval);

/****************************************************************************
 * Private data Prototypes
 ****************************************************************************/
static int                  stm32_ana_current;
FAR struct stm32_tim_dev_s *stm32_ana_tim; /* timer used */
#ifdef ADC_DMA_CHANNEL
static DMA_HANDLE           adc_dma;       /* Allocated DMA channel */
#endif

/****************************************************************************
 * Private data
 ****************************************************************************/

//static int raw_filter_pos = 0;
//static uint32_t raw_ch[ANA_NBR];

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: adc_getreg
 *
 * Description:
 *   Read the value of an ADC register.
 *
 * Input Parameters:
 *   offset - The offset to the register to read
 *
 * Returned Value:
 *   The current contents of the specified register
 *
 ****************************************************************************/

static inline uint32_t adc_getreg(int offset)
{
    return getreg32(ADC_BASE + offset);
}

/****************************************************************************
 * Name: adc_putreg
 *
 * Description:
 *   Write a value to an ADC register.
 *
 * Input Parameters:
 *   offset - The offset to the register to write to
 *   value  - The value to write to the register
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

static inline void adc_putreg(int offset, uint32_t value)
{
    putreg32(value, ADC_BASE + offset);
}

/****************************************************************************
 * Name: adc_modifyreg
 *
 * Description:
 *   Modify the value of an ADC register (not atomic).
 *
 * Input Parameters:
 *   offset  - The offset to the register to modify
 *   mask - The bits to clear
 *   regval - The bits to set
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

static inline void adc_modifyreg(int offset, uint32_t mask, uint32_t regval)
{
    modifyreg32(ADC_BASE+offset,mask,regval);
}


static int adc_set_sampling_time(int ch,int sample_time)
{
    uint32_t offset;
    int      shift;
    int      mask;
    switch(ch)
    {
        case 0:
            offset  = STM32_ADC_SMPR2_OFFSET;
            shift   = ADC_SMPR2_SMP0_SHIFT;
            mask    = ADC_SMPR2_SMP0_MASK;
            break;
        case 1:
            offset  = STM32_ADC_SMPR2_OFFSET;
            shift   = ADC_SMPR2_SMP1_SHIFT;
            mask    = ADC_SMPR2_SMP1_MASK;
            break;
        case 2:
            offset  = STM32_ADC_SMPR2_OFFSET;
            shift   = ADC_SMPR2_SMP2_SHIFT;
            mask    = ADC_SMPR2_SMP2_MASK;
            break;
        case 3:
            offset  = STM32_ADC_SMPR2_OFFSET;
            shift   = ADC_SMPR2_SMP3_SHIFT;
            mask    = ADC_SMPR2_SMP3_MASK;
            break;
        case 4:
            offset  = STM32_ADC_SMPR2_OFFSET;
            shift   = ADC_SMPR2_SMP4_SHIFT;
            mask    = ADC_SMPR2_SMP4_MASK;
            break;
        case 5:
            offset  = STM32_ADC_SMPR2_OFFSET;
            shift   = ADC_SMPR2_SMP5_SHIFT;
            mask    = ADC_SMPR2_SMP5_MASK;
            break;
        case 6:
            offset  = STM32_ADC_SMPR2_OFFSET;
            shift   = ADC_SMPR2_SMP6_SHIFT;
            mask    = ADC_SMPR2_SMP6_MASK;
            break;
        case 7:
            offset  = STM32_ADC_SMPR2_OFFSET;
            shift   = ADC_SMPR2_SMP7_SHIFT;
            mask    = ADC_SMPR2_SMP7_MASK;
            break;
        case 8:
            offset  = STM32_ADC_SMPR2_OFFSET;
            shift   = ADC_SMPR2_SMP8_SHIFT;
            mask    = ADC_SMPR2_SMP8_MASK;
            break;
        case 9:
            offset  = STM32_ADC_SMPR2_OFFSET;
            shift   = ADC_SMPR2_SMP9_SHIFT;
            mask    = ADC_SMPR2_SMP9_MASK;
        case 10:
            offset  = STM32_ADC_SMPR1_OFFSET;
            shift   = ADC_SMPR1_SMP10_SHIFT;
            mask    = ADC_SMPR1_SMP10_MASK;
            break;
        case 11:
            offset  = STM32_ADC_SMPR1_OFFSET;
            shift   = ADC_SMPR1_SMP11_SHIFT;
            mask    = ADC_SMPR1_SMP11_MASK;
            break;
        case 12:
            offset  = STM32_ADC_SMPR1_OFFSET;
            shift   = ADC_SMPR1_SMP12_SHIFT;
            mask    = ADC_SMPR1_SMP12_MASK;
            break;
        case 13:
            offset  = STM32_ADC_SMPR1_OFFSET;
            shift   = ADC_SMPR1_SMP13_SHIFT;
            mask    = ADC_SMPR1_SMP13_MASK;
            break;
        case 14:
            offset  = STM32_ADC_SMPR1_OFFSET;
            shift   = ADC_SMPR1_SMP14_SHIFT;
            mask    = ADC_SMPR1_SMP14_MASK;
            break;
        case 15:
            offset  = STM32_ADC_SMPR1_OFFSET;
            shift   = ADC_SMPR1_SMP15_SHIFT;
            mask    = ADC_SMPR1_SMP15_MASK;
            break;
        case 16:
            offset  = STM32_ADC_SMPR1_OFFSET;
            shift   = ADC_SMPR1_SMP16_SHIFT;
            mask    = ADC_SMPR1_SMP16_MASK;
            break;
        case 17:
            offset  = STM32_ADC_SMPR1_OFFSET;
            shift   = ADC_SMPR1_SMP17_SHIFT;
            mask    = ADC_SMPR1_SMP17_MASK;
            break;
        case 18:
            offset  = STM32_ADC_SMPR1_OFFSET;
            shift   = ADC_SMPR1_SMP18_SHIFT;
            mask    = ADC_SMPR1_SMP18_MASK;
            break;
        default:
            return ERROR; 
    }
    adc_modifyreg(offset,mask,sample_time<<shift);
    return OK;
}

static inline int adc_set_sequence(const uint8_t *chanlist, int nbr)
{
  uint32_t regval;

  regval = ((uint32_t)nbr - 1) << ADC_SQR1_L_SHIFT;

  if ( nbr >= 16 ) regval |= chanlist[15] << ADC_SQR1_SQ16_SHIFT;
  if ( nbr >= 15 ) regval |= chanlist[14] << ADC_SQR1_SQ15_SHIFT;
  if ( nbr >= 14 ) regval |= chanlist[13] << ADC_SQR1_SQ14_SHIFT;
  if ( nbr >= 13 ) regval |= chanlist[12] << ADC_SQR1_SQ13_SHIFT;
  adc_modifyreg( STM32_ADC_SQR1_OFFSET, ~ADC_SQR1_RESERVED, regval);
  regval = 0;

  if ( nbr >= 12 ) regval |= chanlist[11] << ADC_SQR2_SQ12_SHIFT;
  if ( nbr >= 11 ) regval |= chanlist[10] << ADC_SQR2_SQ11_SHIFT;
  if ( nbr >= 10 ) regval |= chanlist[9] << ADC_SQR2_SQ10_SHIFT;
  if ( nbr >=  9 ) regval |= chanlist[8] << ADC_SQR2_SQ9_SHIFT;
  if ( nbr >=  8 ) regval |= chanlist[7] << ADC_SQR2_SQ8_SHIFT;
  if ( nbr >=  7 ) regval |= chanlist[6] << ADC_SQR2_SQ7_SHIFT;
  adc_modifyreg( STM32_ADC_SQR2_OFFSET, ~ADC_SQR2_RESERVED, regval);
  regval = 0;

  if ( nbr >=  6 ) regval |= chanlist[5] << ADC_SQR3_SQ6_SHIFT;
  if ( nbr >=  5 ) regval |= chanlist[4] << ADC_SQR3_SQ5_SHIFT;
  if ( nbr >=  4 ) regval |= chanlist[3] << ADC_SQR3_SQ4_SHIFT;
  if ( nbr >=  3 ) regval |= chanlist[2] << ADC_SQR3_SQ3_SHIFT;
  if ( nbr >=  2 ) regval |= chanlist[1] << ADC_SQR3_SQ2_SHIFT;
  if ( nbr >=  1 ) regval |= chanlist[0] << ADC_SQR3_SQ1_SHIFT;
  adc_modifyreg( STM32_ADC_SQR3_OFFSET, ~ADC_SQR3_RESERVED, regval);

  return OK;
}


/*******************************************************************************
 * Name: stm32_analog_adc_irq
 ******************************************************************************/

static int adc_interrupt(int irq, FAR void *context, FAR void* arg)
{
    uint32_t regval;
    uint32_t pending;

    regval  = adc_getreg(STM32_ADC_SR_OFFSET);

    pending = regval & ( ADC_SR_EOC | ADC_SR_OVR );
    if (pending == 0)
        return OK;

#if 0
    if (regval & ADC_SR_AWD)
        watchdog_callback();
#endif

    if (regval & ADC_SR_OVR)
    {
        STM32_ANALOG_LOG(LOG_ERR," Overrun: SR=0x%08X\n",regval);
        ana_overrun_nbr++;
        regval &= ~(ADC_SR_OVR|ADC_SR_STRT);
        adc_putreg(STM32_ADC_SR_OFFSET, regval);
        stm32_ana_current = 0;
    }
    else if ((regval & ADC_SR_EOC) != 0) /* EOC: End of conversion */
    {
        int32_t  data;

        if ( stm32_ana_current >= ADC_INPUT_NBR)
            stm32_ana_current = 0;

        data = adc_getreg( STM32_ADC_DR_OFFSET) & ADC_DR_RDATA_MASK;
        ana_values[stm32_ana_current++] = data;

        if ( stm32_ana_current == ADC_INPUT_NBR)
        {
            int i;
            STM32_ANALOG_LOG(LOG_DEBUG," values \n");
            for ( i = 0 ; i < ADC_INPUT_NBR; i++ )
                STM32_ANALOG_LOG(LOG_DEBUG," %05d\n",ana_values[i]);
        }
        else
        {
            /* Start Next ADC Conversion */
            adc_modifyreg(STM32_ADC_CR2_OFFSET,0,ADC_CR2_SWSTART);
        }
    }

    return OK;
}

/****************************************************************************
 * Name: adc_dmacovcallback
 *
 * Description:
 *   Callback for DMA.  Called from the DMA transfer complete interrupt after
 *   all channels have been converted and transferred with DMA.
 *
 * Input Parameters:
 *
 *   handle - handle to DMA
 *   isr -
 *   arg - adc device
 *
 * Returned Value:
 *
 ****************************************************************************/

#ifdef ADC_DMA_CHANNEL
static void adc_dmaconvcallback(DMA_HANDLE handle, uint8_t isr, FAR void *arg)
{
    int i;
    STM32_ANALOG_LOG(LOG_DEBUG," values \n");
    for ( i = 0 ; i < ANA_NBR; i++ )
        STM32_ANALOG_LOG(LOG_DEBUG," %05d\n",ana_values[i]);

#if 0
    /* Restart DMA for the next conversion series */
    adc_modifyreg(STM32_ADC_CR2_OFFSET, ADC_CR2_DMA, 0);
    adc_modifyreg(STM32_ADC_CR2_OFFSET, 0, ADC_CR2_DMA);
#endif
}
#endif

static int stm32_analog_tim_handler(int irq, void *context, void* arg)
{
    UNUSED(irq);
    UNUSED(context);

    //STM32_ANALOG_LOG(LOG_DEBUG,"Start Conv\n");

    /* Clear all */
    adc_putreg(STM32_ADC_SR_OFFSET, 0);

    /* Start Conversion ADC */
    adc_modifyreg(STM32_ADC_CR2_OFFSET,0,ADC_CR2_SWSTART);

    STM32_TIM_ACKINT(stm32_ana_tim, 0);
    return OK;
}

/****************************************************************************
 * Public variable 
 ****************************************************************************/

uint16_t ana_values[ANA_NBR]; /* adc value stored in this array */
uint32_t ana_overrun_nbr;


/****************************************************************************
 * Name: stm32_analog_init
 *
 * Description:
 *  Initialize All GPIO for key pad.
 * Input parameters:
 *  _key_map    - first key mapping of mapping GPIO<=>Key list.
 *                    to Finish list set Pin with negative value.
 * Returned Value:
 *   None (User allocated instance initialized).
 ****************************************************************************/
int stm32_analog_init( void )
{
    int i;
    uint32_t regval;

    irqstate_t flags;

    stm32_ana_tim = stm32_tim_init(BOARD_ANALOG_TIMER);

    if (stm32_ana_tim == NULL)
        return -EINVAL;

    flags = enter_critical_section();

    STM32_TIM_SETMODE(  stm32_ana_tim, STM32_TIM_MODE_UP);
    STM32_TIM_SETISR(stm32_ana_tim,stm32_analog_tim_handler, NULL, 0);

        /* SETCLOCK should be after SET PERIOD do be sure that current counter 
         * value is not already after period value
         */
    STM32_TIM_SETPERIOD(stm32_ana_tim, TIMER_CLK_FREQ/TIMER_FREQ); 
    STM32_TIM_SETCLOCK( stm32_ana_tim, TIMER_CLK_FREQ); /* base time 1ms */

    /* Configure the pins as analog inputs for the selected channels */

    i = ANA_NBR;
    while(i--)
    {
        stm32_configgpio(g_gpiolist[i]);
    }

    /* reset ADC */
    modifyreg32(STM32_RCC_APB2RSTR, 0, RCC_APB2RSTR_ADCRST); 
    modifyreg32(STM32_RCC_APB2RSTR, RCC_APB2RSTR_ADCRST, 0); 

#if (ADC_BASE == STM32_ADC1_BASE)
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_ADC1EN); 
#elif (ADC_BASE == STM32_ADC2_BASE)
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_ADC2EN); 
#elif (ADC_BASE == STM32_ADC3_BASE)
    modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_ADC3EN); 
#else
#   error "3 invalid ADC"
#endif


    /* Configure the pins as analog inputs for the selected channels */

    i = ANA_NBR;
    while(i--)
    {
        adc_set_sampling_time(g_chanlist[i],ADC_SMPR_480);
    }

    /* ADC CCR configuration */

    regval  = 0;
#if ((STM32_PCLK2_FREQUENCY/2) < 36000000)
    regval |= ADC_CCR_ADCPRE_DIV2;
    STM32_ANALOG_LOG(LOG_INFO,"ADC clk: %d MHz\n",STM32_PCLK2_FREQUENCY/2000000);
#elif ((STM32_PCLK2_FREQUENCY/4) < 36000000)
    regval |= ADC_CCR_ADCPRE_DIV4;
    STM32_ANALOG_LOG(LOG_INFO,"ADC clk: %d MHz\n",STM32_PCLK2_FREQUENCY/4000000);
#elif ((STM32_PCLK2_FREQUENCY/6) < 36000000)
    regval |= ADC_CCR_ADCPRE_DIV6;
    STM32_ANALOG_LOG(LOG_INFO,"ADC clk: %d MHz\n",STM32_PCLK2_FREQUENCY/6000000);
#elif ((STM32_PCLK2_FREQUENCY/8) < 36000000)
    regval |= ADC_CCR_ADCPRE_DIV8;
    STM32_ANALOG_LOG(LOG_INFO,"ADC clk: %d MHz\n",STM32_PCLK2_FREQUENCY/8000000);
#else
#   error "PCLK too high to use ADC"
#endif
    regval |= ADC_CCR_VBATE;

    modifyreg32(STM32_ADC_CCR, ~ADC_CCR_RESERVED, regval);

    /* CR1 */

    regval = 0;
    regval |= ADC_CR1_RES_12BIT;
    regval |= ADC_CR1_SCAN;
#ifndef ADC_DMA_CHANNEL 
    /* When NODMA data are ready faster than CPU can get it 
     * So we need to use discontinous mode.
     */
    regval |= ADC_CR1_DISCEN;
#endif

    adc_modifyreg(STM32_ADC_CR1_OFFSET,~ADC_CR1_RESERVED,regval);

    /* CR2 */
    regval = 0;
#ifdef ADC_DMA_CHANNEL
    regval |= ADC_CR2_DMA;
    regval |= ADC_CR2_DDS;
#endif
    //regval |= ADC_CR2_CONT;
    //regval |= BOARD_ANALOG_EXT_EDGE;
    //regval |= BOARD_ANALOG_EXT_SRC;

    adc_modifyreg(STM32_ADC_CR2_OFFSET,~ADC_CR2_RESERVED,regval);

    adc_set_sequence(g_chanlist,ADC_INPUT_NBR);

    /* CCR leave all by default */

#ifdef ADC_DMA_CHANNEL
    /* Enable DMA */
    /* Stop and free DMA if it was started before */

    if (adc_dma != NULL)
    {
        stm32_dmastop(adc_dma);
        stm32_dmafree(adc_dma);
    }

    adc_dma = stm32_dmachannel(ADC_DMA_CHANNEL);

    stm32_dmasetup(adc_dma,
                   ADC_BASE + STM32_ADC_DR_OFFSET,
                   (uint32_t)ana_values,
                   ADC_INPUT_NBR,
                   ADC_DMA_CONTROL_WORD);

    stm32_dmastart(adc_dma, adc_dmaconvcallback, NULL, false);
#else
    /* set It at each end of conversion */
    adc_modifyreg(STM32_ADC_CR2_OFFSET,0,ADC_CR2_EOCS);
#endif

    /* Enable IRQ chain */
    irq_attach(ADC_IRQ, adc_interrupt, NULL);
    adc_modifyreg(STM32_ADC_CR1_OFFSET,0,ADC_CR1_EOCIE|ADC_CR1_OVRIE);
    up_enable_irq(ADC_IRQ);

    /* Enable ADC */
    adc_modifyreg(STM32_ADC_CR2_OFFSET,0,ADC_CR2_ADON);

    STM32_TIM_ENABLEINT(stm32_ana_tim, true);

    leave_critical_section(flags);

    return OK;

}

