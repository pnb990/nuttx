/****************************************************************************
 * configs/stm32gg-pnbchrono/src/stm32_gps.c
 *
 *   Copyright (C) 2018 Pierre-noel Bouteville. All rights reserved.
 *   Author: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>

#include <nuttx/arch.h>
#include <nuttx/irq.h>
#include <nuttx/clock.h>

#include <arch/irq.h>
#include <arch/board/board.h>
#include <pnbchrono/gps.h>

#include <string.h>
#include <poll.h>
#include <errno.h>
#include <nuttx/kmalloc.h>

#include "up_arch.h"
#include "stm32_rcc.h"
#include "stm32_gpio.h"
#include "stm32_uart.h"
#include "pnbchrono-v2.h"

#ifdef CONFIG_STM32_GPS_INFO
#   define gpsinfo(fmt,...)   _info("SNSinf "fmt, ##__VA_ARGS__)
#else
#   define gpsinfo(fmt,...)
#endif

#ifdef CONFIG_STM32_GPS_ERR
#   define gpserr(fmt,...)    _err("SNSerr "fmt, ##__VA_ARGS__)
#else
#   define gpserr(fmt,...)
#endif

#if (GPIO_GPS_USART == 1)
#   define GPIO_GPS_USART_IRQ       STM32_IRQ_USART1
#   define GPIO_GPS_USART_APBCLOCK  STM32_PCLK2_FREQUENCY
#   define GPIO_GPS_USART_BASE      STM32_USART1_BASE
#   define GPIO_GPS_USART_RCC_EN    RCC_APB2ENR_USART1EN
#   define GPIO_GPS_USART_RCC_ADDR  STM32_RCC_APB2ENR
#   ifdef CONFIG_STM32_USART1_SERIALDRIVER
#       error "in conflict with serial driver."
#   endif
#elif (GPIO_GPS_USART == 2)
#   define GPIO_GPS_USART_IRQ       STM32_IRQ_USART2
#   define GPIO_GPS_USART_APBCLOCK  STM32_PCLK1_FREQUENCY
#   define GPIO_GPS_USART_BASE      STM32_USART2_BASE
#   define GPIO_GPS_USART_RCC_EN    RCC_APB1ENR_USART2EN
#   define GPIO_GPS_USART_RCC_ADDR  STM32_RCC_APB1ENR
#   ifdef CONFIG_STM32_USART2_SERIALDRIVER
#       error "in conflict with serial driver."
#   endif
#elif (GPIO_GPS_USART == 3)
#   define GPIO_GPS_USART_IRQ       STM32_IRQ_USART3
#   define GPIO_GPS_USART_APBCLOCK  STM32_PCLK1_FREQUENCY
#   define GPIO_GPS_USART_BASE      STM32_USART3_BASE
#   define GPIO_GPS_USART_RCC_EN    RCC_APB1ENR_USART3EN
#   define GPIO_GPS_USART_RCC_ADDR  STM32_RCC_APB1ENR
#   ifdef CONFIG_STM32_USART3_SERIALDRIVER
#       error "in conflict with serial driver."
#   endif
#elif (GPIO_GPS_USART == 4)
#   define GPIO_GPS_USART_IRQ       STM32_IRQ_UART4
#   define GPIO_GPS_USART_APBCLOCK  STM32_PCLK1_FREQUENCY
#   define GPIO_GPS_USART_BASE      STM32_UART4_BASE
#   define GPIO_GPS_USART_RCC_EN    RCC_APB1ENR_UART4EN
#   define GPIO_GPS_USART_RCC_ADDR  STM32_RCC_APB1ENR
#   ifdef CONFIG_STM32_UART4_SERIALDRIVER
#       error "in conflict with serial driver."
#   endif
#elif (GPIO_GPS_USART == 5)
#   define GPIO_GPS_USART_IRQ       STM32_IRQ_UART5
#   define GPIO_GPS_USART_APBCLOCK  STM32_PCLK1_FREQUENCY
#   define GPIO_GPS_USART_BASE      STM32_UART5_BASE
#   define GPIO_GPS_USART_RCC_EN    RCC_APB1ENR_UART5EN
#   define GPIO_GPS_USART_RCC_ADDR  STM32_RCC_APB1ENR
#   ifdef CONFIG_STM32_UART5_SERIALDRIVER
#       error "in conflict with serial driver."
#   endif
#elif (GPIO_GPS_USART == 6)
#   define GPIO_GPS_USART_IRQ       STM32_IRQ_USART6
#   define GPIO_GPS_USART_APBCLOCK  STM32_PCLK2_FREQUENCY
#   define GPIO_GPS_USART_BASE      STM32_USART6_BASE
#   define GPIO_GPS_USART_RCC_EN    RCC_APB2ENR_USART6EN
#   define GPIO_GPS_USART_RCC_ADDR  STM32_RCC_APB2ENR
#   ifdef CONFIG_STM32_USART6_SERIALDRIVER
#       error "in conflict with serial driver."
#   endif
#elif (GPIO_GPS_USART == 7)
#   define GPIO_GPS_USART_IRQ       STM32_IRQ_UART7
#   define GPIO_GPS_USART_APBCLOCK  STM32_PCLK1_FREQUENCY
#   define GPIO_GPS_USART_BASE      STM32_UART7_BASE
#   define GPIO_GPS_USART_RCC_EN    RCC_APB1ENR_UART7EN
#   define GPIO_GPS_USART_RCC_ADDR  STM32_RCC_APB1ENR
#   ifdef CONFIG_STM32_UART7_SERIALDRIVER
#       error "in conflict with serial driver."
#   endif
#elif (GPIO_GPS_USART == 8)
#   define GPIO_GPS_USART_IRQ       STM32_IRQ_UART8
#   define GPIO_GPS_USART_APBCLOCK  STM32_PCLK1_FREQUENCY
#   define GPIO_GPS_USART_BASE      STM32_UART8_BASE
#   define GPIO_GPS_USART_RCC_EN    RCC_APB1ENR_UART8EN
#   define GPIO_GPS_USART_RCC_ADDR  STM32_RCC_APB1ENR
#   ifdef CONFIG_STM32_UART8_SERIALDRIVER
#       error "in conflict with serial driver."
#   endif
#else
#   error "GPIO_GPS_USART Invalid"
#endif


/****************************************************************************
 * SENSORS config
 ****************************************************************************/
#define FIFO_RX_SIZE           16
#define FIFO_TX_SIZE           16

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Fileops Prototypes and Structures
 ****************************************************************************/

typedef FAR struct file file_t;

static int stm32_gps_open(      file_t * filep);
static int stm32_gps_close(     file_t * filep);
static ssize_t stm32_gps_read(  file_t * filep, FAR char *buf, 
                                      size_t buflen);
static ssize_t stm32_gps_write(  file_t * filep, FAR const char *buf, 
                                      size_t buflen);
static int stm32_gps_ioctl(     file_t * filep, int cmd, 
                                      unsigned long arg );
#ifndef CONFIG_DISABLE_POLL
static int stm32_gps_poll(file_t * filep, 
                                FAR struct pollfd *fds, 
                                bool setup
                               );
#endif

static const struct file_operations gps_ops =
{
    .open   = stm32_gps_open,
    .close  = stm32_gps_close,
    .read   = stm32_gps_read,
    .write  = stm32_gps_write,
    .seek   = NULL,
    .ioctl  = stm32_gps_ioctl,
#ifndef CONFIG_DISABLE_POLL
    .poll   = stm32_gps_poll,
#endif
};

/****************************************************************************
 * Name: stm32_gps_dev_t
 * Description:
 *  variable of keypad
 ****************************************************************************/
typedef struct 
{
    int     open_count;     /* open counter */
    speed_t baud;           /* serial port baudrate */
//    int     parity;         /* serial parity */
//    int     bits;           /* serial bits number */
//    bool    stopbits2;      /* serial 2 stop bits */

    uint32_t gpio_pps;      /* GPIO for PPS used */
    bool    pol;            /* GPIO PPS Polarity */
    struct timespec last_tp;/* last pps time */
    gps_t*  frame;          /* gps frame position */
    int     frame_next_ndx; /* gps frame next position */

    /* fifo */
    sem_t   *poll_sem;      /* Poll event semaphore */
    sem_t   mutex;          /* mutex protection */
    /* read fifo */
    sem_t   rd_sem;         /* fifo semaphore */
    int     rd_inndx;          /* fifo read index */
    int     rd_outndx;         /* fifo write index */
    gps_t   rd_buf[FIFO_RX_SIZE]; /* fifo data */
    /* write fifo */
    sem_t   wr_sem;         /* fifo semaphore */
    int     wr_inndx;          /* fifo read index */
    int     wr_outndx;         /* fifo write index */
    char    wr_buf[FIFO_TX_SIZE]; /* fifo data */

    int (*gettime)(struct timespec* tp);/* get time function */

}stm32_gps_dev_t;


/************************************************************************************
 * Name: stm32_gps_takesem
 ************************************************************************************/

static int stm32_gps_takesem(FAR sem_t *sem, bool errout)
{
  int ret;

  do
    {
      /* Take the semaphore (perhaps waiting) */

      ret = nxsem_wait(sem);
      if (ret < 0)
        {
          /* The only case that an error should occur here is if the wait was
           * awakened by a signal.
           */

          DEBUGASSERT(ret == -EINTR);

          /* When the signal is received, should we errout? Or should we just
           * continue waiting until we have the semaphore?
           */

          if (errout)
            {
              return ret;
            }
        }
    }
  while (ret == -EINTR);

  return ret;
}

/*******************************************************************************
 * Name: stm32_gps_givesem
 ******************************************************************************/

#define stm32_gps_givesem(sem) (void)nxsem_post(sem)

/****************************************************************************
 * Name: stm32_serial_get32
 ****************************************************************************/

static inline uint32_t stm32_serial_get32(int offset)
{
  return getreg32(GPIO_GPS_USART_BASE + offset);
}

/****************************************************************************
 * Name: stm32_serial_put32
 ****************************************************************************/

static inline void stm32_serial_put32(int offset, uint32_t value)
{
  putreg32(value, GPIO_GPS_USART_BASE + offset);
}


/****************************************************************************
 * Name: stm32_gps_irq
 *
 * Description:
 *  Manage gpio interrupt
 * Input parameters:
 *  _key_map    - first key mapping of mapping GPIO<=>Key list.
 *                    to Finish list set Pin with negative value.
 * Returned Value:
 *   None (User allocated instance initialized).
 ****************************************************************************/
int stm32_gps_irq(int irq, FAR void *context, FAR void *arg)
{
    FAR stm32_gps_dev_t *dev = arg;

    if ( dev->gettime(&dev->last_tp) < 0 )
    {
        return ERROR;
    }

    /* TODO reset frame ?? */
    
    return OK;
}

/****************************************************************************
 * Name: stm32_set_format
 *
 * Description:
 *   Set the serial line format and speed.
 *
 ****************************************************************************/

static void stm32_set_format(stm32_gps_dev_t *dev)
{
  uint32_t usartdiv32;
  uint32_t mantissa;
  uint32_t fraction;
  uint32_t regval;
  uint32_t brr;

  /* Load CR1 */

  regval = stm32_serial_get32(STM32_USART_CR1_OFFSET);

  /* This second implementation is for U[S]ARTs that support fractional
   * dividers.
   *
   * Configure the USART Baud Rate.  The baud rate for the receiver and
   * transmitter (Rx and Tx) are both set to the same value as programmed
   * in the Mantissa and Fraction values of USARTDIV.
   *
   *   baud     = fCK / (16 * usartdiv)
   *   usartdiv = fCK / (16 * baud)
   *
   * Where fCK is the input clock to the peripheral (PCLK1 for USART2, 3, 4, 5
   * or PCLK2 for USART1)
   *
   * First calculate (NOTE: all stand baud values are even so dividing by two
   * does not lose precision):
   *
   *   usartdiv32 = 32 * usartdiv = fCK / (baud/2)
   */

  usartdiv32 = GPIO_GPS_USART_APBCLOCK / (dev->baud >> 1);

  /* The mantissa part is then */

  mantissa   = usartdiv32 >> 5;

  /* The fractional remainder (with rounding) */

  fraction   = (usartdiv32 - (mantissa << 5) + 1) >> 1;

#if defined(CONFIG_STM32_STM32F4XXX)
  /* The F4 supports 8 X in oversampling additional to the
   * standard oversampling by 16.
   *
   * With baud rate of fCK / (16 * Divider) for oversampling by 16.
   * and baud rate of  fCK /  (8 * Divider) for oversampling by 8
   */

  /* Check if 8x oversampling is necessary */

  if (mantissa == 0)
    {
      regval |= USART_CR1_OVER8;

      /* Rescale the mantissa */

      mantissa = usartdiv32 >> 4;

      /* The fractional remainder (with rounding) */

      fraction = (usartdiv32 - (mantissa << 4) + 1) >> 1;
    }
  else
    {
      /* Use 16x Oversampling */

      regval &= ~USART_CR1_OVER8;
    }
#endif

  brr  = mantissa << USART_BRR_MANT_SHIFT;
  brr |= fraction << USART_BRR_FRAC_SHIFT;

  stm32_serial_put32(STM32_USART_CR1_OFFSET, regval);
  stm32_serial_put32(STM32_USART_BRR_OFFSET, brr);

  /* Configure parity mode */

  regval &= ~(USART_CR1_PCE | USART_CR1_PS | USART_CR1_M);

  /* only priority None and 8bits are implemented and needed for GPS */
#if 0 
  if (dev->parity == 1)       /* Odd parity */
    {
      regval |= (USART_CR1_PCE | USART_CR1_PS);
    }
  else if (dev->parity == 2)  /* Even parity */
    {
      regval |= USART_CR1_PCE;
    }

  /* Configure word length (parity uses one of configured bits)
   *
   * Default: 1 start, 8 data (no parity), n stop, OR
   *          1 start, 7 data + parity, n stop
   */

  if (dev->bits == 9 || (dev->bits == 8 && dev->parity != 0))
    {
      /* Select: 1 start, 8 data + parity, n stop, OR
       *         1 start, 9 data (no parity), n stop.
       */

      regval |= USART_CR1_M;
    }
#endif

  stm32_serial_put32(STM32_USART_CR1_OFFSET, regval);

  /* Configure STOP bits */

  regval = stm32_serial_get32(STM32_USART_CR2_OFFSET);
  regval &= ~(USART_CR2_STOP_MASK);

  /* only one stop bit is use in GPS so not implemented */

#if 0
  if (dev->stopbits2)
    {
      regval |= USART_CR2_STOP2;
    }
#endif

  stm32_serial_put32(STM32_USART_CR2_OFFSET, regval);

  /* Configure hardware flow control */

  regval  = stm32_serial_get32(STM32_USART_CR3_OFFSET);
  regval &= ~(USART_CR3_CTSE | USART_CR3_RTSE);

  stm32_serial_put32(STM32_USART_CR3_OFFSET, regval);
}

/****************************************************************************
 * Name: up_set_apb_clock
 *
 * Description:
 *   Enable or disable APB clock for the USART peripheral
 *
 * Input parameters:
 *   dev - A reference to the UART driver state structure
 *   on  - Enable clock if 'on' is 'true' and disable if 'false'
 *
 ****************************************************************************/

static void up_set_apb_clock(stm32_gps_dev_t *dev, bool on)
{

  /* Enable/disable APB 1/2 clock for USART */

  if (on)
    {
      modifyreg32(GPIO_GPS_USART_RCC_ADDR, 0, GPIO_GPS_USART_RCC_EN);
    }
  else
    {
      modifyreg32(GPIO_GPS_USART_RCC_ADDR, GPIO_GPS_USART_RCC_EN, 0);
    }
}
/****************************************************************************
 * Name: gps_serial_init
 *
 * Description:
 *   Configure the USART baud, bits, parity, etc. This method is called the
 *   first time that the serial port is opened.
 *
 ****************************************************************************/

static int stm32_gps_setup(stm32_gps_dev_t *dev)
{
  uint32_t regval;

  /* Note: The logic here depends on the fact that that the USART module
   * was enabled in stm32_lowsetup().
   */

  /* Enable USART APB1/2 clock */

  up_set_apb_clock(dev, true);

  /* Configure pins for USART use */

  stm32_configgpio(GPIO_GPS_USART_TX);
  stm32_configgpio(GPIO_GPS_USART_RX);

  /* Configure CR2 */
  /* Clear STOP, CLKEN, CPOL, CPHA, LBCL, and interrupt enable bits */

  regval  = stm32_serial_get32(STM32_USART_CR2_OFFSET);
  regval &= ~(USART_CR2_STOP_MASK | USART_CR2_CLKEN | USART_CR2_CPOL |
              USART_CR2_CPHA | USART_CR2_LBCL | USART_CR2_LBDIE);

  /* Configure STOP bits */

  /* only one stop bit is use in GPS so not implemented */

#if 0
  if (dev->stopbits2)
    {
      regval |= USART_CR2_STOP2;
    }
#endif

  stm32_serial_put32(STM32_USART_CR2_OFFSET, regval);

  /* Configure CR1 */
  /* Clear TE, REm and all interrupt enable bits */

  regval  = stm32_serial_get32(STM32_USART_CR1_OFFSET);
  regval &= ~(USART_CR1_TE | USART_CR1_RE | USART_CR1_ALLINTS);

  stm32_serial_put32(STM32_USART_CR1_OFFSET, regval);

  /* Configure CR3 */
  /* Clear CTSE, RTSE, and all interrupt enable bits */

  regval  = stm32_serial_get32(STM32_USART_CR3_OFFSET);
  regval &= ~(USART_CR3_CTSIE | USART_CR3_CTSE | USART_CR3_RTSE | USART_CR3_EIE);

  stm32_serial_put32(STM32_USART_CR3_OFFSET, regval);

  /* Configure the USART line format and speed. */

  stm32_set_format(dev);

  /* Enable Rx, Tx, and the USART */

  regval      = stm32_serial_get32(STM32_USART_CR1_OFFSET);
  regval     |= (USART_CR1_UE | USART_CR1_TE | USART_CR1_RE);
  stm32_serial_put32(STM32_USART_CR1_OFFSET, regval);

  return OK;
}

/****************************************************************************
 * Name: stm32_gps_frame_put_char
 *
 * Description:
 *  Threat received char on GPS serial port.
 *
 ****************************************************************************/

void stm32_gps_frame_put_char(stm32_gps_dev_t* dev, char c)
{
    gps_t* frame;

    if ( dev->frame == NULL )
    {

        /* check if new frame is free */

        if ( dev->frame_next_ndx == dev->rd_outndx )
        {
            return;
        }
        dev->frame = &dev->rd_buf[dev->frame_next_ndx];
    }

    frame = dev->frame;

    if ( c == '$' )
    {
#ifdef CONFIG_STM32_GPS_NO_PPS
        dev->gettime(&dev->last_tp);
#endif
        frame->size = 0;
        frame->tp = dev->last_tp;
        dev->last_tp.tv_sec = 0;
        dev->last_tp.tv_nsec = 0;
    }

    if ( frame->size < gpsFRAME_SIZE_MAX )
    {
        frame->data[frame->size++] = c;
    }

    if ( c == '\n' )
    {
        /* validate finished frame */
        dev->frame = NULL;
        dev->rd_inndx = dev->frame_next_ndx;

        /* compute next frame index */

        dev->frame_next_ndx++;
        if ( dev->frame_next_ndx >= FIFO_RX_SIZE )
        {
            dev->frame_next_ndx -= FIFO_RX_SIZE;
        }

        /* indicate new received frame */

        gpsinfo("New GPS frame.\n");

        nxsem_post(&dev->rd_sem);
        if ( dev->poll_sem )
        {
            nxsem_post(dev->poll_sem);
        }
    }
}

/****************************************************************************
 * Name: stm32_gps_interrupt
 *
 * Description:
 *   This is the USART interrupt handler.  It will be invoked when an
 *   interrupt received on the 'irq' 
 *
 ****************************************************************************/

static int stm32_serial_interrupt(int irq, void *context, void *arg)
{
  stm32_gps_dev_t *dev = (stm32_gps_dev_t *)arg;
  int  passes;
  bool handled;
  uint32_t sr;
  int32_t cr1;

  DEBUGASSERT(dev != NULL);

  /* Loop until there are no characters to be transferred or,
   * until we have been looping for a long time.
   */

  /* disable Tx Interrupt */
  cr1 = stm32_serial_get32(STM32_USART_CR1_OFFSET);

  handled = true;
  for (passes = 0; passes < 256 && handled; passes++)
    {
      handled = false;

      /* Get the masked USART status word. */

      sr = stm32_serial_get32(STM32_USART_SR_OFFSET);

      /* USART interrupts:
       *
       * Enable             Status          Meaning                         Usage
       * ------------------ --------------- ------------------------------- ----------
       * USART_CR1_IDLEIE   USART_SR_IDLE   Idle Line Detected              (not used)
       * USART_CR1_RXNEIE   USART_SR_RXNE   Received Data Ready to be Read
       * "              "   USART_SR_ORE    Overrun Error Detected
       * USART_CR1_TCIE     USART_SR_TC     Transmission Complete           (used only for RS-485)
       * USART_CR1_TXEIE    USART_SR_TXE    Transmit Data Register Empty
       * USART_CR1_PEIE     USART_SR_PE     Parity Error
       *
       * USART_CR2_LBDIE    USART_SR_LBD    Break Flag                      (not used)
       * USART_CR3_EIE      USART_SR_FE     Framing Error
       * "           "      USART_SR_NE     Noise Error
       * "           "      USART_SR_ORE    Overrun Error Detected
       * USART_CR3_CTSIE    USART_SR_CTS    CTS flag                        (not used)
       *
       * NOTE: Some of these status bits must be cleared by explicity writing zero
       * to the SR register: USART_SR_CTS, USART_SR_LBD. Note of those are currently
       * being used.
       */

      /* Handle incoming, receive bytes. */

      if ( ((sr & USART_SR_RXNE) != 0) && ( cr1 & USART_CR1_RXNEIE ) )
      {
          char c;
          c = stm32_serial_get32(STM32_USART_RDR_OFFSET);
          stm32_gps_frame_put_char(dev, c);
          handled = true;
      }

      /* We may still have to read from the DR register to clear any pending
       * error conditions.
       */

      else if ((sr & (USART_SR_ORE | USART_SR_NE | USART_SR_FE)) != 0)
        {
          /* If an error occurs, read from DR to clear the error (data has
           * been lost).  If ORE is set along with RXNE then it tells you
           * that the byte *after* the one in the data register has been
           * lost, but the data register value is correct.  That case will
           * be handled above if interrupts are enabled. Otherwise, that
           * good byte will be lost.
           */

          (void)stm32_serial_get32(STM32_USART_RDR_OFFSET);
        }

      /* Handle outgoing, transmit bytes */

      if ( ( (sr & USART_SR_TXE) != 0 ) && ( cr1 & USART_CR1_TXEIE ) )
        {
            int ndx;
            ndx = dev->wr_outndx;

            if ( ndx != dev->wr_inndx )
            {
                stm32_serial_put32(STM32_USART_TDR_OFFSET, dev->wr_buf[ndx]);

                ndx ++;

                if ( ndx >= FIFO_TX_SIZE )
                {
                    ndx -= FIFO_TX_SIZE;
                }
                dev->wr_outndx = ndx;

                nxsem_post(&dev->wr_sem);
                if ( dev->poll_sem )
                {
                    nxsem_post(dev->poll_sem);
                }
                handled = true;
            }
            else
            {
                stm32_serial_put32(STM32_USART_CR1_OFFSET,
                                   cr1 & ~USART_CR1_TXEIE);
            }
        }
    }

  return OK;
}


/****************************************************************************
 * Name: stm32_gps_init
 *
 * Description:
 *  Initialize All GPIO for key pad.
 * Input parameters:
 *  _key_map    - first key mapping of mapping GPIO<=>Key list.
 *                    to Finish list set Pin with negative value.
 * Returned Value:
 *   None (User allocated instance initialized).
 ****************************************************************************/
int stm32_gps_register( const char* devname, uint32_t gpio, 
                        int (*gettime)(struct timespec *tp) )
{
    int ret;
    irqstate_t irqstate;
    stm32_gps_dev_t *dev;

    dev = (stm32_gps_dev_t*)kmm_malloc(sizeof(stm32_gps_dev_t));
    if ( dev == NULL )
    {
        gpserr("Cannot allocate it!\n");
        return -ENODEV;
    }


    /* Disable interrupts until we are done.  This guarantees that the
     * following operations are atomic.
     */

    irqstate = up_irq_save(); /* Disable IRQ ================================= */

    memset(dev,0,sizeof(*dev));
    //dev->open_count = 0; already done */
    //dev->poll_sem = NULL; already done */
    sem_init(&dev->mutex,  0, 1);
    sem_init(&dev->rd_sem,  0, 0);
    sem_init(&dev->wr_sem,  0, 0);

    dev->gettime        = gettime;
    dev->baud           = 115200;
    //dev->bits           = 8;

    ret = irq_attach(GPIO_GPS_USART_IRQ, stm32_serial_interrupt, dev);
    if ( ret < 0 )
    {
        gpserr("Cannot allocate it!\n");
    }

    up_irq_restore(irqstate); /* Enable IRQ ================================= */

    if ( ret < 0 )
    {
        free(dev);
        return ret;
    }

    stm32_gps_setup(dev);

    return register_driver(devname, &gps_ops, 0666, dev);
}


/****************************************************************************
 * Name: stm32_gps_open
 ****************************************************************************/

static int stm32_gps_open(file_t * filep)
{
    int ret;
    FAR struct inode        *inode = filep->f_inode;
    FAR stm32_gps_dev_t  *dev   = inode->i_private;

    ASSERT( dev != NULL );

    ret = stm32_gps_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    dev->open_count++;

    if ( dev->open_count == 1 )
    {
        irqstate_t irqstate;

        irqstate = up_irq_save(); /* disable IRQ */

        /* reset FIFO */

        dev->wr_inndx  = 0;
        dev->wr_outndx = 0;
        dev->rd_inndx  = 0;
        dev->rd_outndx = 0;
        dev->frame = dev->rd_buf;
        dev->frame_next_ndx = 1;

        /* configure GPIO */

        ret = stm32_gpiosetevent(GPIO_GPS_PPS, true, false, false, 
                                 stm32_gps_irq, dev);

        if ( ret < 0 )
        {
            gpserr("Cannot configure GPIO IRQ");
            dev->open_count--;
            ret = ERROR;
        }
        else
        {
            uint32_t regval;
            /* Enable Tx/Rx Interrupt */
            regval = stm32_serial_get32(STM32_USART_CR1_OFFSET);
            regval |= USART_CR1_RXNEIE;
            stm32_serial_put32(STM32_USART_CR1_OFFSET,regval);

            up_enable_irq(GPIO_GPS_USART_IRQ);
        }

        up_irq_restore(irqstate); /* enabled IRQ */
    }

    stm32_gps_givesem( &dev->mutex );

    return ret;
}

/****************************************************************************
 * Name: stm32_gps_close
 ****************************************************************************/

static int stm32_gps_close(file_t * filep)
{
    int ret;
    FAR struct inode *inode = filep->f_inode;
    FAR stm32_gps_dev_t *dev    = inode->i_private;

    ret = stm32_gps_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    DEBUGASSERT(dev->open_count >= 0);

    /* TODO find an other way to unlock thread that wait on this file descriptor */
#if 0
    i = dev->open_count;
    while (i--)
    {
        nxsem_post(&dev->wr_sem);
        nxsem_post(&dev->rd_sem);
        if ( dev->poll_sem )
        {
            nxsem_post(dev->poll_sem);
        }
    }
#endif

    dev->open_count--;
    if ( dev->open_count == 0 )
    {
        irqstate_t irqstate;
        uint32_t regval;
        /* Disable Tx/Rx Interrupt */
        irqstate = up_irq_save(); /* disable IRQ */

        regval = stm32_serial_get32(STM32_USART_CR1_OFFSET);
        regval &= ~(USART_CR1_TXEIE | USART_CR1_RXNEIE);
        stm32_serial_put32(STM32_USART_CR1_OFFSET,regval);

        up_disable_irq(GPIO_GPS_USART_IRQ);

        ret = stm32_gpiosetevent(GPIO_GPS_PPS, false, false, false, NULL, NULL);

        up_irq_restore(irqstate); /* enabled IRQ */
        if ( ret < 0 )
        {
            gpserr("Cannot reset GPIO IRQ");
            ret = ERROR;
        }
    }

    stm32_gps_givesem( &dev->mutex );

    return ret;
}


/****************************************************************************
 * Name: stm32_gps_ioctl
 ****************************************************************************/

static int stm32_gps_ioctl(file_t *filep, int cmd, unsigned long arg)
{
  int ret;
  FAR struct inode *inode         = filep->f_inode;
  FAR stm32_gps_dev_t *dev    = inode->i_private;

  irqstate_t irqstate;

  ASSERT( dev != NULL );

  ret = stm32_gps_takesem(&dev->mutex, true);
  if (ret < 0)
      return ret;

  switch(cmd)
    {
#ifdef CONFIG_SERIAL_TERMIOS
      case TCGETS:
            {
              FAR struct termios *termiosp = (FAR struct termios *)arg;

              if (!termiosp)
                {
                  ret = -EINVAL;
                  break;
                }

              /* And update with flags from this layer */

              termiosp->c_iflag = 0;//ignored
              termiosp->c_oflag = 0;//ignored
              termiosp->c_lflag = 0;//ignored
              cfsetspeed(termiosp,dev->baud);
            }
          break;

      case TCSETS:
            {
              speed_t new_baud;
              FAR struct termios *termiosp = (FAR struct termios *)arg;

              if (!termiosp)
                {
                  ret = -EINVAL;
                  break;
                }

              /* Update the flags we keep at this layer */

              //dev->tc_iflag = termiosp->c_iflag; ignored
              //dev->tc_oflag = termiosp->c_oflag; ignored
              //dev->tc_lflag = termiosp->c_lflag; ignored
              new_baud = cfgetspeed(termiosp);
              if (new_baud != dev->baud)
                {
                  irqstate = up_irq_save();
                  dev->baud = new_baud;
                  stm32_set_format(dev);
                  up_irq_restore(irqstate);
                  ret = 0;
                }
            }
          break;
#endif

      default:
          ret = -EINVAL;
    }

  stm32_gps_givesem( &dev->mutex );

  return ret;
}

/****************************************************************************
 * Name: stm32_gps_poll
 ****************************************************************************/

#ifndef CONFIG_DISABLE_POLL
static int stm32_gps_poll(file_t * filep, FAR struct pollfd *fds, bool setup)
{
    int ret;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_gps_dev_t *dev    = inode->i_private;
    irqstate_t irqstate;

    ret = stm32_gps_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    if (setup)
    {

        fds->revents = 0;
        /* This is a request to set up the poll.  Find an available
         * slot for the poll structure reference
         */

        if ( dev->poll_sem != NULL)
        {
            ret = -EINVAL;
            goto errout;
        }

        irqstate = up_irq_save();
        if ( dev->rd_inndx != dev->rd_outndx )
        {
            fds->revents |= (fds->events & POLLIN);
        }
        up_irq_restore(irqstate);

        if ( fds->revents == 0 )
        {
            dev->poll_sem = fds->sem ;
        }
        else
        {
            nxsem_post(fds->sem);
            ret = 1;
        }
    }
    else if ( dev->poll_sem == fds->sem )
    {
        irqstate = up_irq_save();
        if ( dev->rd_inndx != dev->rd_outndx )
        {
            fds->revents |= (fds->events & POLLIN);
        }
        up_irq_restore(irqstate);
        dev->poll_sem = NULL;
    }
errout:
    stm32_gps_givesem(&dev->mutex);
    return ret;
}
#endif

/****************************************************************************
 * Name: stm32_gps_read
 ****************************************************************************/

static ssize_t stm32_gps_read(file_t * filep, FAR char *buf, size_t buflen)
{
    int ret = 0;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_gps_dev_t *dev    = inode->i_private;

    gps_t* ptr = (gps_t*)buf;
    ssize_t size = 0;

    ASSERT( dev != NULL );

    ret = stm32_gps_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    while ( buflen >= sizeof(gps_t) )
    {
        irqstate_t irqstate;
        int len = buflen;
        UNUSED(len);

        ret = 0;

        if ( size == 0 )
        {
            stm32_gps_givesem( &dev->mutex );
            nxsem_wait(&dev->rd_sem);
            ret = stm32_gps_takesem(&dev->mutex, true);
        }
        else
        {
            ret = sem_trywait( &dev->rd_sem );
        }

        if (ret < 0)
            break;
        if ( filep->f_inode == NULL )
        {
            ret = -EBADFD;
            break; /* file was closed */
        }

        irqstate = up_irq_save(); /* disable IRQ */

        if ( dev->rd_inndx != dev->rd_outndx )
        {
            int ndx = dev->rd_outndx;

            *ptr = dev->rd_buf[ndx];

            ndx++;
            if (ndx >= FIFO_RX_SIZE )
                ndx -= FIFO_RX_SIZE;

            dev->rd_outndx = ndx;
            ret = 1;
        }

        up_irq_restore(irqstate); /* enabled IRQ */

        if ( ret == 0 )
        {
        }

        gpsinfo("Read %d bytes of timespec %8d.%03d\n",
                ptr->size,
                ptr->tp.tv_sec,
                ptr->tp.tv_nsec/1000000
               );
        ptr++;
        buflen  -= sizeof(gps_t);
        size    += sizeof(gps_t); 
    }

    stm32_gps_givesem( &dev->mutex );

    if ( ret < 0 )
        return ret;
    return size;
}

/****************************************************************************
 * Name: stm32_gps_write
 ****************************************************************************/

static ssize_t stm32_gps_write(file_t * filep, FAR const char *buf, 
                               size_t buflen)
{
    int ret;
    FAR struct inode *inode     = filep->f_inode;
    FAR stm32_gps_dev_t *dev    = inode->i_private;

    ssize_t size = 0;

    ASSERT( dev != NULL );

    ret = stm32_gps_takesem(&dev->mutex, true);
    if (ret < 0)
        return ret;

    while ( buflen > 0 )
    {
        int next;
        int ndx;
        int32_t regval;
        irqstate_t irqstate;
        int len = buflen;
        UNUSED(len);

        ret = 0;

        irqstate = up_irq_save(); /* disable IRQ */
        ndx = dev->wr_inndx;
        next = ndx + 1;
        if ( next >= FIFO_TX_SIZE )
        {
            next -= FIFO_TX_SIZE;
        }

        if ( next != dev->wr_outndx )
        {
            dev->wr_buf[ndx] = *buf++;
            dev->wr_inndx = next;
            ret = 1;
            buflen --;
            size++;

            /* Enable Tx Interrupt */
            regval = stm32_serial_get32(STM32_USART_CR1_OFFSET);
            regval |= USART_CR1_TXEIE;
            stm32_serial_put32(STM32_USART_CR1_OFFSET,regval);
        }

        up_irq_restore(irqstate); /* enabled IRQ */
        if ( ret == 0 )
        {
            stm32_gps_givesem( &dev->mutex );
            nxsem_wait(&dev->wr_sem);
            ret = stm32_gps_takesem(&dev->mutex, true);
            if (ret < 0)
                break;
            if ( filep->f_inode == NULL )
            {
                ret = -EBADFD;
                break; /* file was closed */
            }
        }
    }

    stm32_gps_givesem( &dev->mutex );

    if (ret < 0)
        return ret;

    return size;
}


