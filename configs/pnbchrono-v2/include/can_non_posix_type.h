#ifndef __CAN_TYPE_H__
#define __CAN_TYPE_H__

/******************************************************************************\
 * Includes
\******************************************************************************/

/******************************************************************************\
 * PUBLIC Macro
\******************************************************************************/

#define can_npMSG_SIZE_MAX 8

#define can_npEXT_ID_MASK           ((1<<29)-1)
#define can_npSTD_ID_MASK           ((1<<11)-1)

#define can_npFLAGS_EXT_ID_MASK     (1<<0)
#define can_npFLAGS_RTR_ID_MASK     (1<<1)
#define can_npFLAGS_FILT_ID_MASK    (1<<2)
#define can_npFLAGS_RTR_ID1_ID_MASK (1<<3)
#define can_npFLAGS_RTR_ID2_ID_MASK (1<<4)

/******************************************************************************\
 * PUBLIC type definitions
\******************************************************************************/

typedef struct
{
    struct timespec tp;
    uint32_t id;
    uint8_t  flags;
    uint8_t  size;
    uint8_t  data[can_npMSG_SIZE_MAX];
}can_np_msg_t;

typedef struct {
    uint32_t id1;
    uint32_t id2;
    uint8_t  flags;
}can_np_filter_t;

/******************************************************************************\
 * PUBLIC constant data definitions
\******************************************************************************/

/******************************************************************************\
 * PUBLIC variable definitions
\******************************************************************************/

/******************************************************************************\
 * PUBLIC function prototypes
\******************************************************************************/

#endif /*__CAN_TYPE_H__ */
