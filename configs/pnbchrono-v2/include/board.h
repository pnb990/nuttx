/************************************************************************************
 * configs/pnbchrono-v2/include/board.h
 *
 *   Copyright (C) 2014 Pierre-Noel Bouteville. All rights reserved.
 *   Author: Pierre-Noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __CONFIG_STM32F4_PNBCHRONO_INCLUDE_BOARD_H
#define __CONFIG_STM32F4_PNBCHRONO_INCLUDE_BOARD_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include "gpio_pinout.h"

#ifndef __ASSEMBLY__
# include <stdint.h>
# include "stdbool.h"
#endif

#include <sys/boardctl.h>
#include <nuttx/board.h>


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Clocking *************************************************************************/
/* The STM32F4 Discovery board features a single 8MHz crystal.  Space is provided
 * for a 32kHz RTC backup crystal, but it is not stuffed.
 *
 * This is the canonical configuration:
 *   System Clock source           : PLL (HSE)
 *   SYSCLK(Hz)                    : 168000000    Determined by PLL configuration
 *   HCLK(Hz)                      : 168000000    (STM32_RCC_CFGR_HPRE)
 *   AHB Prescaler                 : 1            (STM32_RCC_CFGR_HPRE)
 *   APB1 Prescaler                : 4            (STM32_RCC_CFGR_PPRE1)
 *   APB2 Prescaler                : 2            (STM32_RCC_CFGR_PPRE2)
 *   HSE Frequency(Hz)             : 8000000      (STM32_BOARD_XTAL)
 *   PLLM                          : 8            (STM32_PLLCFG_PLLM)
 *   PLLN                          : 336          (STM32_PLLCFG_PLLN)
 *   PLLP                          : 2            (STM32_PLLCFG_PLLP)
 *   PLLQ                          : 7            (STM32_PLLCFG_PLLQ)
 *   Main regulator output voltage : Scale1 mode  Needed for high speed SYSCLK
 *   Flash Latency(WS)             : 5
 *   Prefetch Buffer               : OFF
 *   Instruction cache             : ON
 *   Data cache                    : ON
 *   Require 48MHz for USB OTG FS, : Enabled
 *   SDIO and RNG clock
 */

/* HSI - 25 MHz RC factory-trimmed
 * LSI - 32 KHz RC
 * HSE - On-board crystal frequency is 25MHz
 * LSE - 32.768 kHz
 */

#define STM32_BOARD_XTAL        25000000ul

#define STM32_HSI_FREQUENCY     16000000ul
#define STM32_LSI_FREQUENCY     32000
#define STM32_HSE_FREQUENCY     STM32_BOARD_XTAL
#define STM32_LSE_FREQUENCY     32768

/* Main PLL Configuration.
 *
 * PLL source is HSE
 * PLL_VCO = (STM32_HSE_FREQUENCY / PLLM) * PLLN
 *         = (25,000,000 / 25) * 336
 *         = 336,000,000
 * SYSCLK  = PLL_VCO / PLLP
 *         = 336,000,000 / 2 = 168,000,000
 * USB OTG FS, SDIO and RNG Clock
 *         =  PLL_VCO / PLLQ
 *         = 48,000,000
 */

#define STM32_PLLCFG_PLLM       RCC_PLLCFG_PLLM(25)
#define STM32_PLLCFG_PLLN       RCC_PLLCFG_PLLN(336)
#define STM32_PLLCFG_PLLP       RCC_PLLCFG_PLLP_2
#define STM32_PLLCFG_PLLQ       RCC_PLLCFG_PLLQ(7)

#define STM32_SYSCLK_FREQUENCY  168000000ul

/* AHB clock (HCLK) is SYSCLK (168MHz) */

#define STM32_RCC_CFGR_HPRE     RCC_CFGR_HPRE_SYSCLK  /* HCLK  = SYSCLK / 1 */
#define STM32_HCLK_FREQUENCY    STM32_SYSCLK_FREQUENCY
#define STM32_BOARD_HCLK        STM32_HCLK_FREQUENCY  /* same as above, to satisfy compiler */

/* APB1 clock (PCLK1) is HCLK/4 (42MHz) */

#define STM32_RCC_CFGR_PPRE1    RCC_CFGR_PPRE1_HCLKd4     /* PCLK1 = HCLK / 4 */
#define STM32_PCLK1_FREQUENCY   (STM32_HCLK_FREQUENCY/4)

/* Timers driven from APB1 will be twice PCLK1 */

#define STM32_APB1_TIM2_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM3_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM4_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM5_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM6_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM7_CLKIN   (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM12_CLKIN  (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM13_CLKIN  (2*STM32_PCLK1_FREQUENCY)
#define STM32_APB1_TIM14_CLKIN  (2*STM32_PCLK1_FREQUENCY)

/* APB2 clock (PCLK2) is HCLK/2 (84MHz) */

#define STM32_RCC_CFGR_PPRE2    RCC_CFGR_PPRE2_HCLKd2     /* PCLK2 = HCLK / 2 */
#define STM32_PCLK2_FREQUENCY   (STM32_HCLK_FREQUENCY/2)

/* Timers driven from APB2 will be twice PCLK2 */

#define STM32_APB2_TIM1_CLKIN   (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM8_CLKIN   (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM9_CLKIN   (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM10_CLKIN  (2*STM32_PCLK2_FREQUENCY)
#define STM32_APB2_TIM11_CLKIN  (2*STM32_PCLK2_FREQUENCY)

/* Timer Frequencies, if APBx is set to 1, frequency is same to APBx
 * otherwise frequency is 2xAPBx.
 * Note: TIM1,8 are on APB2, others on APB1
 */

#define BOARD_TIM1_FREQUENCY    STM32_HCLK_FREQUENCY
#define BOARD_TIM2_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM3_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM4_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM5_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM6_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM7_FREQUENCY    (STM32_HCLK_FREQUENCY / 2)
#define BOARD_TIM8_FREQUENCY    STM32_HCLK_FREQUENCY

/* SDIO dividers.  Note that slower clocking is required when DMA is disabled
 * in order to avoid RX overrun/TX underrun errors due to delayed responses
 * to service FIFOs in interrupt driven mode.  These values have not been
 * tuned!!!
 *
 * SDIOCLK=48MHz, SDIO_CK=SDIOCLK/(118+2)=400 KHz
 */

#define SDIO_INIT_CLKDIV        (118 << SDIO_CLKCR_CLKDIV_SHIFT)

/* DMA ON:  SDIOCLK=48MHz, SDIO_CK=SDIOCLK/(1+2)=16 MHz
 * DMA OFF: SDIOCLK=48MHz, SDIO_CK=SDIOCLK/(2+2)=12 MHz
 */

#ifdef CONFIG_SDIO_DMA
#  define SDIO_MMCXFR_CLKDIV    (1 << SDIO_CLKCR_CLKDIV_SHIFT)
#else
#  define SDIO_MMCXFR_CLKDIV    (2 << SDIO_CLKCR_CLKDIV_SHIFT)
#endif

/* DMA ON:  SDIOCLK=48MHz, SDIO_CK=SDIOCLK/(1+2)=16 MHz
 * DMA OFF: SDIOCLK=48MHz, SDIO_CK=SDIOCLK/(2+2)=12 MHz
 */

#ifdef CONFIG_SDIO_DMA
#  define SDIO_SDXFR_CLKDIV     (1 << SDIO_CLKCR_CLKDIV_SHIFT)
#else
#  define SDIO_SDXFR_CLKDIV     (2 << SDIO_CLKCR_CLKDIV_SHIFT)
#endif


// Retrieve a system timestamp. Cortex-M cycle counter (DWT_CYCCNT).
#define SEGGER_SYSVIEW_GET_TIMESTAMP()    (*(uint32_t *)(0xE0001004))
#define SEGGER_SYSVIEW_TIMESTAMP_BITS     32

// Frequency of the timestamp. Must match SEGGER_SYSVIEW_GET_TIMESTAMP in SEGGER_SYSVIEW_Conf.h
#define SYSVIEW_TIMESTAMP_FREQ  (STM32_SYSCLK_FREQUENCY)

// System Frequency. SystemcoreClock is used in most CMSIS compatible projects.
#define SYSVIEW_CPU_FREQ        (STM32_SYSCLK_FREQUENCY)

#define SYSTEMVIEW_BOARD_DESC()\
  SEGGER_SYSVIEW_SendSysDesc("I#73=GPS(DMA2_S1)"); \
  SEGGER_SYSVIEW_SendSysDesc("I#71=TIM7"); \
  SEGGER_SYSVIEW_SendSysDesc("I#60=TIM8(ADC)"); \
  SEGGER_SYSVIEW_SendSysDesc("I#55=WIFI(USART3)"); \
  SEGGER_SYSVIEW_SendSysDesc("I#41=I2C2_ER"); \
  SEGGER_SYSVIEW_SendSysDesc("I#40=I2C2_EV"); \
  SEGGER_SYSVIEW_SendSysDesc("I#34=ADC"); \
  SEGGER_SYSVIEW_SendSysDesc("I#28=WIFI(DMA1_S1)"); \
  SEGGER_SYSVIEW_SendSysDesc("I#15=Systick"); \
  SEGGER_SYSVIEW_SendSysDesc("I#41=SPEED(TIM1_UP_TIM10)"); \
  SEGGER_SYSVIEW_SendSysDesc("I#75="); \
  SEGGER_SYSVIEW_SendSysDesc("I#65="); \
  SEGGER_SYSVIEW_SendSysDesc("I#39="); \

//  SEGGER_SYSVIEW_SendSysDesc("I#3="); \

/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

#define BOARD_CHRONO_CLK_INT_FREQ   STM32_APB1_TIM2_CLKIN

#define BOARD_SPEED_PPM_FREQ        10000
#define BOARD_RPM_PPM_FREQ          10000
#define BOARD_SENSORS_TIMER         7
#define BOARD_ANALOG_TIMER          8 /* selected to have TRGO */
//#define BOARD_ANALOG_EXT_EDGE     ADC_CR2_EXTEN_RISING
//#define BOARD_ANALOG_EXT_SRC      ADC_CR2_EXTSEL_T8TRGO

#define BOARD_CAN_PORT1_PATH        "/dev/can1"
#define BOARD_CAN_PORT2_PATH        "/dev/can0"
#define BOARD_CAN_PATH              BOARD_CAN_PORT2_PATH

#define BOARD_SDHC_BLOCK_DEV_PATH   "/dev/mmcsd0"
#define BOARD_SDHC_MOUNT_PATH       "/mnt"
#define BOARD_SDHC_FSTYPE           "vfat"
#define BOARD_SDHC_DEBOUNDE_MS      1000
#define BOARD_SDHC_UNMOUNT_MS       1000

#define BOARD_PWM_LIGHT_DEV         GPIO_LED_PWM_DEV
#define BOARD_TTYS_WIFI_DEV         "/dev/ttyS0"
#define BOARD_TTYS_KLINE_DEV        "/dev/ttyS1"
#define BOARD_TTYS_GPS_DEV          "/dev/ttyS2"
#define BOARD_FAST_SNS_DEV          "/dev/fast_sns0"
//#define BOARD_GPS_PPS_PATH          "/dev/gps_pps"


#define BOARD_MLX90614_PATH         "/dev/temps_ir"
#define BOARD_MLX90614_I2C_ADDR(x)  (0x10+(x))
#define BOARD_MLX90614_NBR          6

#define BOARD_VL52L0X_I2C_ADDR_DEF      (0x52/2) /* default Address */
#define BOARD_VL52L0X_I2C_ADDR_FRONT    (BOARD_VL52L0X_I2C_ADDR_DEF+1)
#define BOARD_VL52L0X_I2C_ADDR_REAR     (BOARD_VL52L0X_I2C_ADDR_DEF)
#define BOARD_PCF9536_I2C_ADDR          (0x41)

#define BOARD_I2C_FREQUENCY             100000

#ifdef CONFIG_I2C_POLLED
#   error "I2C doesn't work well in polled mode
#endif

#define BOARD_KEY_PRESSED  0x80
#define BOARD_KEY_BACK      0
#define BOARD_KEY_UP        1
#define BOARD_KEY_DOWN      2
#define BOARD_KEY_OK        3


/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   This furnction is call before any initialisation (excepte StackCheck init)
 *
 ************************************************************************************/
#if defined(CONFIG_ARCH_BOARD_STM32_EARLYINIT)
void stm32_early_init(void);
#endif

/************************************************************************************
 * Name: stm32_boardinitialize
 *
 * Description:
 *   All STM32 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32_boardinitialize(void);


/************************************************************************************
 * Name:  Low level non posix function
 *
 * Description:
 *   If CONFIG_ARCH_LEDS is defined, then NuttX will control the on-board LEDs.  If
 *   CONFIG_ARCH_LEDS is not defined, then the following interfaces are available to
 *   control the LEDs from user applications.
 *
 ************************************************************************************/

#ifdef CONFIG_BOARDCTL_IOCTL

enum
{
    /**
      @brief Reboot in bootloader mode.
      @note board_ioctl cmd
     */
    BIOC_REBOOT_BTLDR      =  BOARDIOC_USER+1,
    /**
      @brief Format SDCard
      @note board_ioctl cmd
     */
    BIOC_SDCARD_FORMAT,

    /**
      @brief Mount ProcFs
      @note board_ioctl cmd
     */
    BIOC_PROCFS_MOUNT,

    /**
      @brief Mount SDCard
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is Mounted, can be NULL.
     */
    BIOC_SDCARD_MOUNT,

    /**
      @brief Mount SDCard
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is Mounted.
     */
    BIOC_SDCARD_IS_MOUNTED,

    /**
      @brief Check SDCARD is inserted
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is inserted.
     */
    BIOC_SDCARD_IS_PRESENT,

    /**
      @brief Umount SDCard
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is Mounted, can be NULL.
     */
    BIOC_SDCARD_UNMOUNT,

    /**
      @brief Enable USB Mass Storage Class
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is Connected.
     */
    BIOC_USB_IS_CONNECTED,

    /**
      @brief Enable USB Mass Storage Class
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is Enable.
     */
    BIOC_USB_IS_ENABLED,

    /**
      @brief Enable USB Mass Storage Class
      @note board_ioctl cmd
     */
    BIOC_USB_MSC_ENABLE,

    /**
      @brief Disable USB Mass Storage Class
      @note board_ioctl cmd
     */
    BIOC_USB_MSC_DISABLE,

    /**
      @brief is Charging
      @note board_ioctl cmd
      @param[out]   arg (bool*) True is charging.
     */
    BIOC_IS_CHARGING,

    /**
      @brief read battery
      @note board_ioctl cmd
      @param[out]   arg (int*) Battery level in percent.
     */
    BIOC_BAT_LEVEL,

    /**
      @brief read battery voltage in mv.
      @note board_ioctl cmd
      @param[out]   arg (int*) Battery level in mv.
     */
    BIOC_VBAT,

    /**
      @brief read power supply in mv.
      @note board_ioctl cmd
      @param[out]   arg (int*) Power Voltage in mV.
      @return 0 (OK) ons success, -1 in case of error.
     */
    BIOC_VPWR,

    /**
      @brief read ambient temperature in m°C.
      @note board_ioctl cmd
      @param[out]   arg (int*) ambient temperature in mV.
      @return 0 (OK) ons success, -1 in case of error.
     */
    BIOC_TAMB,

    /**
      @brief Power OFF.
      if arg is true, we unmount SD card 
      otherwise it failed SD card is mounted.
      @note board_ioctl cmd
      @param[in]   arg bool : force
      @return 0 (OK) ons success, -1 in case of error.
     */
    BIOC_POWER_OFF,
};
#endif


#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif  /* __CONFIG_STM32F4_PNBCHRONO_INCLUDE_BOARD_H */
