
/**
  @brief use by boot loader to test if firmware works.
  it set this tags and on next reboot firmware should ack it 
  <b> WHEN ALL IS OK ! </b> by change it to @ref btldrMODE_FIRM_WAS_OK.
  */
#define btldrMODE_TEST_FIRM ( ('M'<<24)|\
                         ('R'<<16)|\
                         ('I'<< 8)|\
                         ('F'<< 0)|\
                         0 )

/**
  @brief use by boot loader to start firmware after reset.
  */
#define btldrRUN_FIRM ( ('!'<<24)|\
                        ('N'<<16)|\
                        ('U'<< 8)|\
                        ('R'<< 0)|\
                        0 )


/**
  @brief use by firmware to force boot loader mode.
  */
#define btldrMODE_BTLDR ( ('R'<<24)|\
                          ('L'<<16)|\
                          ('T'<< 8)|\
                          ('B'<< 0)|\
                          0 )

/**
  @brief firmware should set it when all is OK
  @warning: when ALL is OK !! Not at beginning of main !!! 
 */
#define btldrMODE_FIRM_WAS_OK  ( ('Y'<<24)|\
                         ('X'<<16)|\
                         ('E'<< 8)|\
                         ('S'<< 0)|\
                         0 )


//uint32_t btldr_mode __attribute__ ((section(".btldr_mode")));
#define btldr_mode (*((uint32_t*)0x20000000))

uint8_t *firm_get_boot_address(void);


