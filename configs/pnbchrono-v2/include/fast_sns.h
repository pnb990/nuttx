/****************************************************************************
 * configs/pnbchrono-v1/include/sensors.h
 * 
 *
 *   Copyright (C) 2015 Pierre-noel Bouteville. All rights reserved.
 *   Author: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __INCLUDE_FAST_SNS_H
#define __INCLUDE_FAST_SNS_H

#include <time.h>
#include <nuttx/fs/ioctl.h>

typedef struct
{
    /* current chronometer value */

    struct  timespec tp;

    uint32_t speed;             /* speed m/h */
    uint32_t rpm;               /* RPM tr/min */
    uint32_t breaking;          
    int32_t suspension_front;   
    int32_t suspension_rear;
    int32_t ext1;
    int32_t ext2;

}fast_sns_t;

/* SENSORS_FREQUENCY set sensors acquisition frequency.
 *
 * Argument: frequency in Hz.
 */
#define SENSORS_TYPE_NONE       0
#define SENSORS_TYPE_ADC        1
#define SENSORS_TYPE_VL53L0X    2

#define SENSORS_FREQUENCY           _SNIOC(0x0000)
#define SENSORS_RESET_FIFO          _SNIOC(0x0001)
#define SENSORS_RPM_RATE            _SNIOC(0x0002) /* arg = tr/pulse*60 */
#define SENSORS_SPEED_RATE          _SNIOC(0x0003) /* arg = mm/pulse */
#define SENSORS_SUSPENSION_FRONT    _SNIOC(0x0004) /* arg = SENSOR_TYPE */
#define SENSORS_SUSPENSION_REAR     _SNIOC(0x0005) /* arg = SENSOR_TYPE */

#define SENSORS_BREAK_TYPE          _SNIOC(0x0006) /* arg = SENSOR_TYPE */
#define SENSORS_BREAK_GPIO_NORM 0
#define SENSORS_BREAK_GPIO_INV  1
#define SENSORS_BREAK_GPIO_EXT1 2
#define SENSORS_BREAK_GPIO_EXT2 3


#endif


