/****************************************************************************
 * sched/systemview/SEGGER_SYSVIEW_Nuttx.c
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Author: Pierre-Noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


#include <nuttx/config.h>
#include <nuttx/irq.h>
#include <nuttx/arch.h>
#include <arch/board/board.h>
#include <sched/sched.h>
#include <sys/types.h>
#include <string.h> // Required for memset

#include "nuttx/systemview.h"

unsigned g_idle_task_id;

/*********************************************************************
 *
 *       SYSVIEW_SendTaskInfo()
 *
 *  Function description
 *    Record task information.
 */
static void SYSVIEW_SendTaskInfo( FAR struct tcb_s * tcb ) 
{
    SEGGER_SYSVIEW_TASKINFO TaskInfo;

    memset(&TaskInfo, 0, sizeof(TaskInfo)); // Fill all elements with 0 to allow extending the structure in future version without breaking the code
    TaskInfo.TaskID     = (uint32_t)tcb->pid;
#if CONFIG_TASK_NAME_SIZE > 0
    TaskInfo.sName      = (const char*)tcb->name;
#else
    TaskInfo.sName      = (const char*)"NoName";
#endif
    TaskInfo.Prio       = (unsigned)tcb->sched_priority;
    TaskInfo.StackBase  = (uint32_t)tcb->adj_stack_ptr;
    TaskInfo.StackSize  = (unsigned)tcb->adj_stack_size;

    SEGGER_SYSVIEW_SendTaskInfo(&TaskInfo);
}


/*********************************************************************
 *
 *       _cbSendTaskList()
 *
 *  Function description
 *    This function is part of the link between Nuttx and SYSVIEW.
 *    Called from SystemView when asked by the host, it uses SYSVIEW
 *    functions to send the entire task list to the host.
 */
static void _cbSendTaskList(void) 
{
    irqstate_t flags;
    int i;

    /* Momentarily disable interrupts.  We need (1) the task to stay valid
     * while we are doing these operations and (2) the tick counts to be
     * synchronized when read.
     */

    flags = enter_critical_section();

    for (i=0;i<CONFIG_MAX_TASKS;i++)
    {
        if (g_pidhash[i].tcb)
        {
            SYSVIEW_SendTaskInfo(g_pidhash[i].tcb);
        }
    }

    leave_critical_section(flags);
}

/*********************************************************************
 *
 *       _cbGetTime()
 *
 *  Function description
 *    This function is part of the link between Nuttx and SYSVIEW.
 *    Called from SystemView when asked by the host, returns the
 *    current system time in micro seconds.
 */
static uint64_t _cbGetTime(void) 
{
    uint64_t time;

    time = clock_systimer();
    time *= USEC_PER_TICK;

    return time;
}

/*********************************************************************
 *
 *       Global functions
 *
 **********************************************************************
 */


/*********************************************************************
 *
 *       SYSVIEW_RecordU32x4()
 *
 *  Function description
 *    Record an event with 4 parameters
 */
void SYSVIEW_RecordU32x4(unsigned Id, uint32_t Para0, uint32_t Para1, uint32_t Para2, uint32_t Para3) 
{
    uint8_t  aPacket[SEGGER_SYSVIEW_INFO_SIZE + 4 * SEGGER_SYSVIEW_QUANTA_U32];
    uint8_t* pPayload;
    //
    pPayload = SEGGER_SYSVIEW_PREPARE_PACKET(aPacket);                // Prepare the packet for SystemView
    pPayload = SEGGER_SYSVIEW_EncodeU32(pPayload, Para0);             // Add the first parameter to the packet
    pPayload = SEGGER_SYSVIEW_EncodeU32(pPayload, Para1);             // Add the second parameter to the packet
    pPayload = SEGGER_SYSVIEW_EncodeU32(pPayload, Para2);             // Add the third parameter to the packet
    pPayload = SEGGER_SYSVIEW_EncodeU32(pPayload, Para3);             // Add the fourth parameter to the packet
    //
    SEGGER_SYSVIEW_SendPacket(&aPacket[0], pPayload, Id);             // Send the packet
}

/*********************************************************************
 *
 *       SYSVIEW_RecordU32x5()
 *
 *  Function description
 *    Record an event with 5 parameters
 */
void SYSVIEW_RecordU32x5(unsigned Id, uint32_t Para0, uint32_t Para1, uint32_t Para2, uint32_t Para3, uint32_t Para4) 
{
    uint8_t  aPacket[SEGGER_SYSVIEW_INFO_SIZE + 5 * SEGGER_SYSVIEW_QUANTA_U32];
    uint8_t* pPayload;
    //
    pPayload = SEGGER_SYSVIEW_PREPARE_PACKET(aPacket);                // Prepare the packet for SystemView
    pPayload = SEGGER_SYSVIEW_EncodeU32(pPayload, Para0);             // Add the first parameter to the packet
    pPayload = SEGGER_SYSVIEW_EncodeU32(pPayload, Para1);             // Add the second parameter to the packet
    pPayload = SEGGER_SYSVIEW_EncodeU32(pPayload, Para2);             // Add the third parameter to the packet
    pPayload = SEGGER_SYSVIEW_EncodeU32(pPayload, Para3);             // Add the fourth parameter to the packet
    pPayload = SEGGER_SYSVIEW_EncodeU32(pPayload, Para4);             // Add the fifth parameter to the packet
    //
    SEGGER_SYSVIEW_SendPacket(&aPacket[0], pPayload, Id);             // Send the packet
}

/*********************************************************************
 *
 *       Public API structures
 *
 **********************************************************************
 */
// Callbacks provided to SYSTEMVIEW by Nuttx
/* TODO:SEGGER */
const SEGGER_SYSVIEW_OS_API SYSVIEW_X_OS_TraceAPI = {
    _cbGetTime,
    _cbSendTaskList,
};

/********************************************************************* 
 *
 *       _cbSendSystemDesc()
 *
 *  Function description
 *    Sends SystemView description strings.
 */
static void _cbSendSystemDesc(void) {
    SEGGER_SYSVIEW_SendSysDesc("N="CONFIG_SEGGER_SYSTEMVIEW_APP_NAME","
                               "D="CONFIG_SEGGER_SYSTEMVIEW_DEVICE_NAME","
                               "O=Nuttx");
#ifdef SYSTEMVIEW_BOARD_DESC
    SYSTEMVIEW_BOARD_DESC()
#endif
}

void SEGGER_SYSVIEW_Conf(unsigned idle_task_id) {
    g_idle_task_id = idle_task_id;
    SEGGER_SYSVIEW_Init(SYSVIEW_TIMESTAMP_FREQ, SYSVIEW_CPU_FREQ, 
                        &SYSVIEW_X_OS_TraceAPI, _cbSendSystemDesc);
}


/*************************** End of file ****************************/
