/*******************************************************************************
Copyright � 2015, STMicroelectronics International N.V.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of STMicroelectronics nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS ARE DISCLAIMED.
IN NO EVENT SHALL STMICROELECTRONICS INTERNATIONAL N.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************/


#ifndef _VL53L0X_PLATFORM_LOG_H_
#define _VL53L0X_PLATFORM_LOG_H_

#include <stdio.h>
#include <string.h>
/* LOG Functions */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @file vl53l0x_platform_log.h
 *
 * @brief platform log function definition
 */

#define VL53L0X_LOG_ENABLE 1

#define TRACE_LEVEL_ERRORS  "ERR"
#define TRACE_LEVEL_WARNING "WAR"
#define TRACE_LEVEL_INFO    "INF"
#define TRACE_LEVEL_DEBUG   "DEB"

#define TRACE_MODULE_API        "API"
#define TRACE_MODULE_PLATFORM   "PLA"


#ifdef VL53L0X_LOG_ENABLE

void trace_print_module_function(uint32_t module, uint32_t level, uint32_t function, const char *format, ...);

#define _LOG_FUNCTION_START(module, fmt, ... ) \
        sninfo(module "<START> "fmt"\n", ##__VA_ARGS__);

#define _LOG_FUNCTION_END(module, status )\
        sninfo(module "<END> %d\n", (int)status)

#define _LOG_FUNCTION_END_FMT(module, status, fmt, ... )\
        sninfo(module, "<END> %d "fmt"\n", (int)status,##__VA_ARGS__)

#else /* VL53L0X_LOG_ENABLE no logging */
    #define VL53L0X_ErrLog(...) (void)0
    #define _LOG_FUNCTION_START(module, fmt, ... ) (void)0
    #define _LOG_FUNCTION_END(module, status, ... ) (void)0
    #define _LOG_FUNCTION_END_FMT(module, status, fmt, ... ) (void)0
#endif /* else */

#define VL53L0X_COPYSTRING(str, ...) strcpy(str, ##__VA_ARGS__)

#ifdef __cplusplus
}
#endif

#endif  /* _VL53L0X_PLATFORM_LOG_H_ */



