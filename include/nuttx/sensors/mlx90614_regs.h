/****************************************************************************
 * include/nuttx/sensors/mlx90614_regs.h
 * Character driver for the MLX90614 IR temperature sensor.
 *
 *   Copyright (C) 2016 Pierre-noel Bouteville . All rights reserved.
 *   Authors: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __INCLUDE_NUTTX_SENSORS_MLX90614_REGS_H
#define __INCLUDE_NUTTX_SENSORS_MLX90614_REGS_H

#define MLX90614_RAM_OFFSET     0x00
#define MLX90614_EEP_OFFSET     0x20

/* RAM register address */
#define MLX90614_RAW_TOBJ1          (MLX90614_RAM_OFFSET+0x04) /* Read only access */
#define MLX90614_RAW_TOBJ2          (MLX90614_RAM_OFFSET+0x05) /* Read only access */
#define MLX90614_TA                 (MLX90614_RAM_OFFSET+0x06) /* Read only access */
#define MLX90614_TOBJ1              (MLX90614_RAM_OFFSET+0x07) /* Read only access */
#define MLX90614_TOBJ2              (MLX90614_RAM_OFFSET+0x08) /* Read only access */


/* EEPROM register address */
#define MLX90614_TOMAX              (MLX90614_EEP_OFFSET+0x00) /* Read/Write access */
#define MLX90614_TOMIN              (MLX90614_EEP_OFFSET+0x01) /* Read/Write access */
#define MLX90614_PWMCTRL            (MLX90614_EEP_OFFSET+0x02) /* Read/Write access */

#define MLX90614_TA_RANGE           (MLX90614_EEP_OFFSET+0x03) /* Read/Write access */
#define MLX90614_EMI_CORR_COEF      (MLX90614_EEP_OFFSET+0x04) /* Read/Write access */
#define MLX90614_CONFIG_REGISTER1   (MLX90614_EEP_OFFSET+0x05) /* Read/Write access */

#define MLX90614_SMBUS_ADDR         (MLX90614_EEP_OFFSET+0x0E) /* Read/Write access */

#define MLX90614_ID_NUMBER1         (MLX90614_EEP_OFFSET+0x1C) /* Read only access */
#define MLX90614_ID_NUMBER2         (MLX90614_EEP_OFFSET+0x1D) /* Read only access */
#define MLX90614_ID_NUMBER3         (MLX90614_EEP_OFFSET+0x1E) /* Read only access */
#define MLX90614_ID_NUMBER4         (MLX90614_EEP_OFFSET+0x1F) /* Read only access */


/* PWMCTRL register bit meaning */
#define MLX90614_PWMCTRL_PERIOD(x)   ((x)<<4) 
#define MLX90614_PWMCTRL_TRPWMB(x)   ((x)<<3) 
#define MLX90614_PWMCTRL_PPODB(x)    ((x)<<2) 
#define MLX90614_PWMCTRL_EN_PWM(x)   ((x)<<1) 
#define MLX90614_PWMCTRL_SINGLE(x)   ((x)<<0) 

/* CONFIG_REGISTER1 register bit meaning */
#define MLX90614_CFG_EN_TEST(x)  ((x)<<15) 
#define MLX90614_CFG_KT2_NEG(x)  ((x)<<14)  /* Kt2 signe negtive    */

#define MLX90614_CFG_GAIN_1      (0<<11)    /* GAIN = 1             */
#define MLX90614_CFG_GAIN_3      (1<<11)    /* GAIN = 3             */
#define MLX90614_CFG_GAIN_6      (2<<11)    /* GAIN = 6             */
#define MLX90614_CFG_GAIN_12_5   (3<<11)    /* GAIN = 12,5          */
#define MLX90614_CFG_GAIN_25     (4<<11)    /* GAIN = 25            */
#define MLX90614_CFG_GAIN_50     (5<<11)    /* GAIN = 51            */
#define MLX90614_CFG_GAIN_100    (6<<11)    /* GAIN = 100           */
#define MLX90614_CFG_GAIN_100    (7<<11)    /* GAIN = 100           */

#define MLX90614_CFG_FIR_8       (0<< 8)    /* FIR = 8 not recommended  */
#define MLX90614_CFG_FIR_16      (1<< 8)    /* FIR = 16 not recommended */
#define MLX90614_CFG_FIR_32      (2<< 8)    /* FIR = 32 not recommended */
#define MLX90614_CFG_FIR_64      (3<< 8)    /* FIR = 64 not recommended */
#define MLX90614_CFG_FIR_128     (4<< 8)    /* FIR = 128                */
#define MLX90614_CFG_FIR_256     (5<< 8)    /* FIR = 256                */
#define MLX90614_CFG_FIR_512     (6<< 8)    /* FIR = 512                */
#define MLX90614_CFG_FIR_1024    (7<< 8)    /* FIR = 1024               */

#define MLX90614_CFG_KTS_NEG     (1<< 7)     /* Kts signe negtive   */
#define MLX90614_CFG_DUAL_IR     (1<< 6)     /* Dual IR sensors     */

#define MLX90614_CFG_TA_TO1      (0<< 4)     /* Ta, Tobj1       */
#define MLX90614_CFG_TA_TO2      (1<< 4)     /* Ta, Tobj2       */
#define MLX90614_CFG_TO2         (2<< 4)     /* Tobj2           */
#define MLX90614_CFG_TO1_TO2     (3<< 4)     /* Tobj1, Tobj2    */

#define MLX90614_CFG_REPEAT_TEST (1<< 3)     /* Repeat test On  */

#define MLX90614_CFG_IIR_100     (4<< 0)     /* IIR (100%) a1=1,        b1=0        */
#define MLX90614_CFG_IIR_80      (5<< 0)     /* IIR ( 80%) a1=0.8,      b1=0.2      */
#define MLX90614_CFG_IIR_67      (6<< 0)     /* IIR ( 67%) a1=0.666,    b1=0.333    */
#define MLX90614_CFG_IIR_57      (7<< 0)     /* IIR ( 57%) a1=0.571,    b1=0.428    */
#define MLX90614_CFG_IIR_50      (0<< 0)     /* IIR ( 50%) a1=0.5,      b1=0.5      */
#define MLX90614_CFG_IIR_25      (1<< 0)     /* IIR ( 25%) a1=0.25,     b1=0.75     */
#define MLX90614_CFG_IIR_17      (2<< 0)     /* IIR ( 17%) a1=0.166(6), b1=0.83(3)  */
#define MLX90614_CFG_IIR_13      (3<< 0)     /* IIR ( 13%) a1=0.125,    b1=0.875    */



#endif
