/****************************************************************************
 * sched/systemview/SEGGER_SYSVIEW_Nuttx.c
 *
 *   Copyright (C) 2016 Gregory Nutt. All rights reserved.
 *   Author: Pierre-Noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <nuttx/systemview.h>

/*
************************************************************************************************************************
*                                               SystemView Trace control Macros
************************************************************************************************************************
*/
#ifdef CONFIG_SEGGER_SYSTEMVIEW
extern unsigned g_idle_task_id;

#  define  systemview_trace_init(idle_tcb)    SEGGER_SYSVIEW_Conf((unsigned)idle_tcb)
#  define  systemview_trace_start()           SEGGER_SYSVIEW_Start()
#  define  systemview_trace_stop()            SEGGER_SYSVIEW_Stop()
#else
#  define  systemview_trace_init(idle_tcb)
#  define  systemview_trace_start()
#  define  systemview_trace_stop()
#endif


/*
************************************************************************************************************************
*                                      Nuttx Trace Kernel-Related Macros
************************************************************************************************************************
*/

#ifdef CONFIG_SEGGER_SYSTEMVIEW_TASK
# define  systemview_trace_task_create(p_tcb)           SEGGER_SYSVIEW_OnTaskCreate((unsigned)p_tcb)
# define  systemview_trace_task_ready(p_tcb)            SEGGER_SYSVIEW_OnTaskStartReady((unsigned)p_tcb)
# define  systemview_trace_task_resume(p_tcb)           \
{\
    if ( ((unsigned)p_tcb)==g_idle_task_id )\
    {\
        SEGGER_SYSVIEW_OnIdle((unsigned)p_tcb)\
    }\
    else\
    {\
        SEGGER_SYSVIEW_OnTaskStartExec((unsigned)p_tcb)\
    }\
}
# define  systemview_trace_task_suspend(p_tcb,cause)    SEGGER_SYSVIEW_OnTaskStopReady((unsigned)p_tcb,(unsigned)cause)
# define  systemview_trace_task_terminate()             SEGGER_SYSVIEW_OnTaskStopExec()
#else
# define  systemview_trace_task_create(p_tcb)       
# define  systemview_trace_task_ready(p_tcb)        
# define  systemview_trace_task_resume(p_tcb)       
# define  systemview_trace_task_suspend(p_tcb,cause)      
# define  systemview_trace_task_terminate(p_tcb)    
#endif

#ifdef CONFIG_SEGGER_SYSTEMVIEW_IRQ
# define  systemview_trace_isr_enter()              SEGGER_SYSVIEW_RecordEnterISR((unsigned)irq)
# define  systemview_trace_isr_exit()               SEGGER_SYSVIEW_RecordExitISR()
# define  systemview_trace_isr_exit_to_scheduler()  SEGGER_SYSVIEW_RecordExitISRToScheduler()
#else
# define  systemview_trace_isr_enter(irq)           
# define  systemview_trace_isr_exit()               
# define  systemview_trace_isr_exit_to_scheduler()  
#endif




