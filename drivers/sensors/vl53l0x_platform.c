/****************************************************************************
 * drivers/sensors/vl53l0x_platform.c
 *
 *   Copyright (C) 2017 Pierre-noel Bouteville . All rights reserved.
 *   adapted to nuttx by: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *   Copyright � 2016, STMicroelectronics International N.V.
 *   All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of STMicroelectronics nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 * NON-INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS ARE DISCLAIMED.
 * IN NO EVENT SHALL STMICROELECTRONICS INTERNATIONAL N.V. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/arch.h>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <nuttx/i2c/i2c_master.h>
#include "nuttx/sensors/vl53l0x_platform.h"
#include "nuttx/sensors/vl53l0x_i2c_platform.h"
#include "nuttx/sensors/vl53l0x_api.h"

/*******************************************************************************
 * Pre-processor Definitions
 ******************************************************************************/

#define LOG_FUNCTION_START(fmt, ... )           _LOG_FUNCTION_START(TRACE_MODULE_PLATFORM, fmt, ##__VA_ARGS__)
#define LOG_FUNCTION_END(status, ... )          _LOG_FUNCTION_END(TRACE_MODULE_PLATFORM, status, ##__VA_ARGS__)
#define LOG_FUNCTION_END_FMT(status, fmt, ... ) _LOG_FUNCTION_END_FMT(TRACE_MODULE_PLATFORM, status, fmt, ##__VA_ARGS__)

#ifndef CONFIG_I2C
#   error "VL53L0X needed I2C"
#endif

/* Configuration **************************************************************/


/* Debug **********************************************************************/

#ifdef CONFIG_VL53L0X_DEBUG
#  define vl53l0xerr  snerr
#  define vl53l0xinfo sninfo
#else
#  define vl53l0xerr(...)
#  define vl53l0xinfo(...)
#endif

/* Low Level access to vl53l0x IC */
/*
 * flags 
 *  -  I2C_M_READ       : for Read transaction  
 *  -  I2C_M_NORESTART  : for write transaction no restart
 */
static int VL53L0X_i2c(VL53L0X_DEV Dev,int flags, uint8_t reg_off, 
                       uint8_t *buf, int size)
{
  int ret;
  struct i2c_msg_s msg[2];

  /* Setup 8-bit MPU address write message */

  msg[0].frequency  = CONFIG_VL53L0X_I2C_FREQ;  /* I2C frequency             */
  msg[0].addr       = Dev->I2cDevAddr;          /* 7-bit address             */
  msg[0].flags      = 0;                        /* Write transaction         */
  msg[0].buffer     = &reg_off;                 /* Transfer from this address*/
  msg[0].length     = 1;                        /* Send one byte address     */

  /* Set up the data written message */

  msg[1].frequency  = CONFIG_VL53L0X_I2C_FREQ;  /* I2C frequency             */
  msg[1].addr       = Dev->I2cDevAddr;          /* 7-bit address             */
  msg[1].flags      = flags;                    /* flags transaction         */
  msg[1].buffer     = buf;                      /* Transfer to this address  */
  msg[1].length     = size;                     /* Receive/Send data buffer  */

  /* Perform the transfer */

  ret = I2C_TRANSFER(Dev->i2c, msg, 2);
  if (ret < 0)
    {
      vl53l0xerr("ERROR: I2C_TRANSFER failed: %d\n", ret);
      return VL53L0X_ERROR_CONTROL_INTERFACE;
    }

#ifdef CONFIG_VL53L0X_DEBUG_REG
  vl53l0xinfo("VL53L0X flags(0x%02X) %d bytes in 0x%02X. "
              "First is 0x%02X => return %d.\n",
          flags,size,reg_off,*buf,ret);
#endif

  return VL53L0X_ERROR_NONE;
}

VL53L0X_Error VL53L0X_WriteMulti(VL53L0X_DEV Dev, uint8_t index, uint8_t *pdata, 
                                 uint32_t count)
{
    return VL53L0X_i2c(Dev,I2C_M_NORESTART,index,pdata,count);
}

VL53L0X_Error VL53L0X_ReadMulti(VL53L0X_DEV Dev, uint8_t index, uint8_t *pdata, 
                                uint32_t count)
{
    return VL53L0X_i2c(Dev,I2C_M_READ,index,pdata,count);
}


VL53L0X_Error VL53L0X_WrByte(VL53L0X_DEV Dev, uint8_t index, uint8_t data)
{
    return VL53L0X_i2c(Dev,I2C_M_NORESTART,index,&data,1);
}

VL53L0X_Error VL53L0X_WrWord(VL53L0X_DEV Dev, uint8_t index, uint16_t data)
{
    return VL53L0X_i2c(Dev,I2C_M_NORESTART,index,(uint8_t*)&data,2);
}

VL53L0X_Error VL53L0X_WrDWord(VL53L0X_DEV Dev, uint8_t index, uint32_t data)
{
    return VL53L0X_i2c(Dev,I2C_M_NORESTART,index,(uint8_t*)&data,4);
}
VL53L0X_Error VL53L0X_RdByte(VL53L0X_DEV Dev, uint8_t index, uint8_t *data)
{
    return VL53L0X_i2c(Dev,I2C_M_READ,index,data,1);
}

VL53L0X_Error VL53L0X_RdWord(VL53L0X_DEV Dev, uint8_t index, uint16_t *data)
{
    return VL53L0X_i2c(Dev,I2C_M_READ,index,(uint8_t*)data,2);
}

VL53L0X_Error  VL53L0X_RdDWord(VL53L0X_DEV Dev, uint8_t index, uint32_t *data)
{
    return VL53L0X_i2c(Dev,I2C_M_READ,index,(uint8_t*)data,4);
}

VL53L0X_Error VL53L0X_UpdateByte(VL53L0X_DEV Dev, uint8_t index, uint8_t AndData, uint8_t OrData)
{
    uint8_t regval;
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;

    Status = VL53L0X_i2c(Dev,I2C_M_READ,index,&regval,1);

    if (Status == VL53L0X_ERROR_NONE) 
    {
        regval = (regval & AndData) | OrData;
        Status = VL53L0X_i2c(Dev,I2C_M_NORESTART,index,&regval,1);
    }

    return Status;
}


VL53L0X_Error VL53L0X_PollingDelay(VL53L0X_DEV Dev)
{
    LOG_FUNCTION_START("");

    up_mdelay(1);

    LOG_FUNCTION_END(VL53L0X_ERROR_NONE);
    return VL53L0X_ERROR_NONE;
}

VL53L0X_Error VL53L0X_Set2V8Mode(VL53L0X_DEV Dev)
{
    VL53L0X_Error Status = VL53L0X_ERROR_NONE;

    /* by default the I2C is running at 1V8 if you want to change it you
     * need to include this define at compilation level. */
    Status = VL53L0X_UpdateByte(Dev,
                                VL53L0X_REG_VHV_CONFIG_PAD_SCL_SDA__EXTSUP_HV,
                                0xFE,
                                0x01);
    return Status;

}

#if 0
	if (Status == VL53L0X_ERROR_NONE)
    {
        uint8_t  buf[3];
        uint16_t reg1;
        uint16_t reg2;
		Status = VL53L0X_ReadMulti( Dev, 0xC0, buf,3);
		Status = VL53L0X_RdWord(    Dev, 0x51, &reg1);
		Status = VL53L0X_RdWord(    Dev, 0x61, &reg1);
        sninfo(" 0xC0 : 0x%02X 0x%02X 0x%02X | 0x51: 0x%04X | 0x61: 0x%04X\n",
               buf[0],buf[0],buf[0],reg1,reg2);
    }
#endif


