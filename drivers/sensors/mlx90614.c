/****************************************************************************
 * drivers/sensors/mlx90614.c
 * Driver for the MLX90614 IR temperature sensor.
 *
 *   Copyright (C) 2016 Pierre-noel Bouteville . All rights reserved.
 *   Authors: Pierre-noel Bouteville <pnb990@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/arch.h>

#include <errno.h>
#include <debug.h>
#include <string.h>
#include <semaphore.h>

#include <nuttx/kmalloc.h>

//#include <nuttx/fs/fs.h>
#include <nuttx/i2c/i2c_master.h>
#include <nuttx/sensors/ioctl.h>
#include <nuttx/sensors/mlx90614_regs.h>
#include <nuttx/sensors/mlx90614.h>

#if defined(CONFIG_I2C) && defined(CONFIG_MLX90614)

/****************************************************************************
 * Private
 ****************************************************************************/

struct mlx90614_dev_s
{
  FAR struct i2c_master_s* i2c;         /* Pointer to the I2C instance */
  FAR const struct mlx90614_config_s *config; /* Pointer to the configuration of
                                         * the MLX90614 sensor */
  sem_t datasem;                        /* Manages exclusive access to
                                         * this structure */
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static int mlx90614_read_register(FAR struct mlx90614_dev_s *priv,
                                   uint8_t reg_addr, uint16_t *reg_data);
static int mlx90614_write_register(FAR struct mlx90614_dev_s *priv,
                                    uint8_t reg_addr, uint16_t reg_data);

static inline int mlx90614_read_temp(FAR struct mlx90614_dev_s *priv, 
                              uint16_t reg_addr,int *temp);

static int mlx90614_open(FAR struct file *filep);
static int mlx90614_close(FAR struct file *filep);
static ssize_t mlx90614_read(FAR struct file *, FAR char *, size_t);
static ssize_t mlx90614_write(FAR struct file *filep, FAR const char *buffer,
                              size_t buflen);
static int mlx90614_ioctl(FAR struct file *filep, int cmd, unsigned long arg);
static int mlx90614_raw2temp(uint16_t raw);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_mlx90614_fops =
{
  mlx90614_open,    /* open   */
  mlx90614_close,   /* close  */
  mlx90614_read,    /* read   */
  mlx90614_write,   /* write  */
  NULL,             /* seek   */
  mlx90614_ioctl    /* ioctl  */
#ifndef CONFIG_DISABLE_POLL
  , NULL            /* poll   */
#endif
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  ,NULL             /* unlink */
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/******************************************************************************
 * Name: mlx90614_takesem
 ******************************************************************************/

static int mlx90614_takesem(FAR sem_t *sem, bool errout)
{
  /* Loop, ignoring interrupts, until we have successfully acquired the semaphore */

  while (sem_wait(sem) != OK)
    {
      /* The only case that an error should occur here is if the wait was awakened
       * by a signal.
       */

      ASSERT(get_errno() == EINTR);

      /* When the signal is received, should we errout? Or should we just continue
       * waiting until we have the semaphore?
       */

      if (errout)
        {
          return -EINTR;
        }
    }

  return OK;
}

/******************************************************************************
 * Name: mlx90614_givesem
 ******************************************************************************/

#define mlx90614_givesem(sem) (void)sem_post(sem)


/****************************************************************************
 * Name: mlx90614_read_register
 ****************************************************************************/

static int mlx90614_read_register(FAR struct mlx90614_dev_s *priv,
                                   uint8_t reg_addr, uint16_t *reg_data)
{
  /* 16-bit data read sequence:
   *
   *  Start - I2C_Write_Address - mlx90614_Reg_Address -
   *    Repeated_Start - I2C_Read_Address  - mlx90614_Read_LSB -
   *      mlx90614_Read_MSB - mlx90614_Read_PEC - STOP
   */

  struct i2c_msg_s msg[2];
  uint8_t rxbuffer[3];
  int ret;

  /* Setup 8-bit ADXL345 address write message */

  msg[0].frequency = priv->config->i2c_freq;    /* I2C frequency */
  msg[0].addr      = priv->config->i2c_addr;    /* 7-bit address */
  msg[0].flags     = 0;                         /* Write transaction, beginning with START */
  msg[0].buffer    = &reg_addr;                 /* Transfer from this address */
  msg[0].length    = 1;                         /* Send one byte following the address
                                                 * (no STOP) */

  /* Set up the 8-bit ADXL345 data read message */

  msg[1].frequency = priv->config->i2c_freq;    /* I2C frequency */
  msg[1].addr      = priv->config->i2c_addr;    /* 7-bit address */
  msg[1].flags     = I2C_M_READ;                /* Read transaction, beginning with Re-START */
  msg[1].buffer    = rxbuffer;                  /* Transfer to this address */
  msg[1].length    = 3;                         /* Receive two bytes following the address
                                                 * (then STOP) */

  /* Perform the transfer */

  ret = I2C_TRANSFER(priv->i2c, msg, 2);
  if (ret < 0)
    {
      snerr("ERROR: I2C_TRANSFER failed: %d\n", ret);
      return -1;
    }

  /* TODO : Check PEC
     */
#ifdef CONFIG_MLX90614_DEBUG
  _err("reg 0x%02X read 0x%02X 0x%02X 0x%02X\n", reg_addr, rxbuffer[0], 
       rxbuffer[1], rxbuffer[2]);
#endif

  *reg_data = (uint16_t)rxbuffer[1] << 8 | (uint16_t)rxbuffer[0];

  return 0;
}

/****************************************************************************
 * Name: mlx90614_write_register
 ****************************************************************************/

static int mlx90614_write_register(FAR struct mlx90614_dev_s *priv,
                                    uint8_t reg_addr, uint16_t reg_data)
{
  /* 16-bit data read sequence:
   *
   *  Start - I2C_Write_Address - mlx90614_Reg_Address -
   *    I2C_Write_Address  - mlx90614_Write_LSB -
   *      mlx90614_Write_MSB - mlx90614_Write_PEC - STOP
   */

  struct i2c_msg_s msg[1];
  uint8_t txbuffer[4];
  int ret;

  txbuffer[0] = reg_addr;
  txbuffer[1] = ((reg_data)&0xFF);
  txbuffer[2] = ((reg_data)>>8);
  txbuffer[3] = 0; /* TODO: Compute PEC 
                    */

  /* Setup 8-bit ADXL345 address write message */

  msg[0].frequency = priv->config->i2c_freq;
  msg[0].addr      = priv->config->i2c_addr;
  msg[0].flags     = 0;
  msg[0].buffer    = txbuffer;
  msg[0].length    = 4;
                                          

#ifdef CONFIG_MLX90614_DEBUG
  _err("reg 0x%02X write 0x%02X 0x%02X 0x%02X\n", reg_addr, txbuffer[1], txbuffer[2], txbuffer[3]);
#endif

  /* Perform the transfer */

  ret = I2C_TRANSFER(priv->i2c, msg, 1);
  if (ret < 0)
    {
      snerr("ERROR: I2C_TRANSFER failed: %d\n", ret);
      return -1;
    }


  return 0;
}

/****************************************************************************
 * Name: mlx90614_raw2temp
 ****************************************************************************/
static int mlx90614_raw2temp(uint16_t raw)
{
    /* TODO : remove 
       reg     07 read  273a65 => 
       raw 10042  ->    473990 mC
     */
    
    int val = raw;
    /* value in milli degree Celsius 
     * T(mC) = ( val * 0.02 + 273.15 ) * 1000 
     */
    return ((val*20) - 273150); 
}

static inline int mlx90614_read_temp(FAR struct mlx90614_dev_s *priv, 
                              uint16_t reg_addr,int *temp)
{
  uint16_t raw;
  if ( mlx90614_read_register(priv,reg_addr,&raw) < 0)
      return -1;
  *temp = mlx90614_raw2temp(raw);
#ifdef CONFIG_MLX90614_DEBUG
  _err("%10d->%3d.%03d mC\n", raw, (*temp)/1000, (*temp)%1000 );
#endif
  return sizeof(int);
}

/****************************************************************************
 * Name: mlx90614_open
 ****************************************************************************/

static int mlx90614_open(FAR struct file *filep)
{
  FAR struct inode *inode = filep->f_inode;
  FAR struct mlx90614_dev_s *priv = inode->i_private;

  if (priv == NULL)
    return -1;

  return OK;
}

/****************************************************************************
 * Name: mlx90614_close
 ****************************************************************************/

static int mlx90614_close(FAR struct file *filep)
{
  FAR struct inode *inode = filep->f_inode;
  FAR struct mlx90614_dev_s *priv = inode->i_private;

  if (priv == NULL)
    return -1;

  return OK;
}

/****************************************************************************
 * Name: mlx90614_read
 ****************************************************************************/

static ssize_t mlx90614_read(FAR struct file *filep, FAR char *buffer,
                             size_t buflen)
{
  FAR struct inode *inode = filep->f_inode;
  FAR struct mlx90614_dev_s *priv = inode->i_private;
  int *ptr = (int*)buffer;
  int ret;

  DEBUGASSERT(priv != NULL);

  /* Check if enough memory was provided for the read call */

  if ( (buflen % sizeof(int)) != 0 )
    {
      snerr("ERROR: memory multiple of int size\n");
      return -ENOSYS;
    }

  /* Copy the sensor data into the buffer */

  /* Aquire the semaphore before the data is copied */

  ret = mlx90614_takesem(&priv->datasem,true);
  if (ret < 0)
    {
      return ret;
    }

  do
    {
      int flags = priv->config->flags;
      if ( ( buflen > 0 ) && ( flags & MLX90614_FLAGS_TA ) )
        {
          buflen -= sizeof(int);
          if ( mlx90614_read_temp(priv, MLX90614_TA,ptr++) < 0)
            {
              ret = -1;
              break;
            }
        }
      if ( ( buflen > 0 ) && ( flags & MLX90614_FLAGS_TOBJ1 ) )
        {
          buflen -= sizeof(int);
          if ( mlx90614_read_temp(priv, MLX90614_TOBJ1,ptr++) < 0)
            {
              ret = -1;
              break;
            }
        }
      if ( ( buflen > 0 ) && ( flags & MLX90614_FLAGS_TOBJ2 ) )
        {
          buflen -= sizeof(int);
          if ( mlx90614_read_temp(priv, MLX90614_TOBJ2,ptr++) < 0)
            {
              ret = -1;
              break;
            }
        }
    }while(0);

  /* Give back the semaphore */

  mlx90614_givesem(&priv->datasem);

  if ( ret < 0 )
      return ret;

  return ((char*)ptr)-buffer;
}

/****************************************************************************
 * Name: mlx90614_write
 ****************************************************************************/

static ssize_t mlx90614_write(FAR struct file *filep, FAR const char *buffer,
                              size_t buflen)
{
  return -ENOSYS;
}

/****************************************************************************
 * Name: mlx90614_ioctl
 ****************************************************************************/

static int mlx90614_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  int ret = OK;
  FAR struct inode *inode = filep->f_inode;
  FAR struct mlx90614_dev_s *priv = inode->i_private;

  switch (cmd)
    {
      /* Command was not recognized */
    case SNIOC_SET_EMISSION:
        /* erase */
        {
          ret = mlx90614_write_register(priv,MLX90614_EMI_CORR_COEF,0);
          if ( ret < 0 )
              break;
          ret = mlx90614_write_register(priv,MLX90614_EMI_CORR_COEF,arg); 
          if ( ret < 0 )
              break;
        }

    default:
      snerr("ERROR: Unrecognized cmd: %d\n", cmd);
      ret = -ENOTTY;
      break;
    }

  return ret;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: mlx90614_register
 *
 * Description:
 *   Register the MLX90614 character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/mag0"
 *   i2c     - An instance of the SPI interface to use to communicate with
 *             MLX90614
 *   config  - Describes the configuration of the MLX90614 part.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int mlx90614_register(FAR const char *devpath, FAR struct i2c_master_s *i2c,
                      FAR const struct mlx90614_config_s *config)
{
  FAR struct mlx90614_dev_s *priv;
  int ret;

  /* Sanity check */

  DEBUGASSERT(i2c != NULL);
  DEBUGASSERT(config != NULL);
  DEBUGASSERT(config->i2c_freq <= 100000); /* 100KHz is maximum frequency */

  /* Initialize the MLX90614 device structure */

  priv = (FAR struct mlx90614_dev_s *)kmm_malloc(sizeof(struct mlx90614_dev_s));
  if (priv == NULL)
    {
      snerr("ERROR: Failed to allocate instance\n");
      return -ENOMEM;
    }

  priv->i2c = i2c;
  priv->config = config;

  sem_init(&priv->datasem, 0, 1);       /* Initialize sensor data access
                                         * semaphore */

  /* Register the character driver */

  ret = register_driver(devpath, &g_mlx90614_fops, 0666, priv);
  if (ret < 0)
    {
      snerr("ERROR: Failed to register driver: %d\n", ret);
      kmm_free(priv);
      sem_destroy(&priv->datasem);
      return -ENODEV;
    }


  return OK;
}

#endif /* CONFIG_SPI && CONFIG_MLX90614 */
